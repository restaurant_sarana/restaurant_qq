<?php
include('dashboard/action/connect.php');

$order=$_POST['order_id'];

$query="SELECT order_food.id,order_food_details.type,
CASE WHEN order_food_details.type='F' THEN (SELECT food FROM food_items WHERE id=order_food_details.items_id)
  ELSE (SELECT name FROM soft_drink WHERE id=order_food_details.items_id) END items,
order_food_details.food_details,order_food_details.qty,order_food_details.price,order_food_details.discount,order_food.date,
order_food.defacno,payment.payment_date,payment.amount_total,payment.discount,payment.payment_receive,payment.change_payment
FROM order_food
INNER JOIN order_food_details ON order_food.defacno = order_food_details.defacno
LEFT JOIN payment ON order_food.defacno=payment.defacno
WHERE order_food.status='1' AND order_food_details.status='1' AND order_food.table_id<>'0'
and order_food.defacno='$order' ORDER BY id DESC";
$result_order = $connect->prepare($query);
$result_order->execute();
$i=0;
while($row = $result_order->fetch()){
  $itemtype=$row[1];
  $itemid=$row[2];
  $itemsize=$row[3];
  $itemqty=$row[4];
  $itemprice=$row[5];
  $order=$row[8];
  $amount_total=$row[10];
  $discount=$row[11];
  $payment_receive=$row[12];
  $change_payment=$row[13];
  $payment_date=$row[9];

  $jdata['itemtype'][$i]=$itemtype;
  $jdata['items'][$i]=$itemid;
  $jdata['itemssize'][$i]=$itemsize;
  $jdata['itemsqty'][$i]=$itemqty;
  $jdata['itemsprice'][$i]=$itemprice;
  $jdata['order'][$i]=$order;
  $jdata['amount_total'][$i]=$amount_total;
  $jdata['discount'][$i]=$discount;
  $jdata['payment_receive'][$i]=$payment_receive;
  $jdata['payment_change'][$i]=$change_payment;
  $jdata['payment_date'][$i]=$payment_date;
  $i=$i+1;
}
echo json_encode($jdata);
 ?>
