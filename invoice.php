<?php
include('dashboard/action/connect.php');

$order=$_POST['itemsorder'];
$itemsdate=$_POST['itemsdate'];
$itemstype=$_POST['itemstype'];
$itemsid=$_POST['itemsid'];
$itemsname=$_POST['itemsname'];
$itemssize=$_POST['itemssize'];
$itemsqty=$_POST['itemsqty'];
$itemsprice=$_POST['itemsprice'];
$itemsdiscount=$_POST['discount'][0];

$grandtotal=0;
// echo $itemsid;
?>
<div class="modal" id="invoiceModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <h5 class="modal-title">Invoice</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body" id="invoice_paper">
      <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title">
    			<h2>Invoice</h2><h3 class="pull-right">Order # <?php echo $order; ?></h3>
    		</div>
    		<hr>
    		<!--<div class="row">
    			 <div class="col-xs-6">
    				<address>
    				<strong>Billed To:</strong><br>
    					John Smith<br>
    					1234 Main<br>
    					Apt. 4B<br>
    					Springfield, ST 54321
    				</address>
    			</div> -->
    			<!-- <div class="col-xs-6 text-right">
    				<address>
        			<strong>Shipped To:</strong><br>
    					Jane Smith<br>
    					1234 Main<br>
    					Apt. 4B<br>
    					Springfield, ST 54321
    				</address>
    			</div>
    		</div> -->
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    					<strong>Payment Method:</strong><br>
    					Visa ending **** 4242<br>
    					jsmith@email.com
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
    					<strong>Order Date:</strong><br>
    					<?php echo $itemsdate; ?><br><br>
    				</address>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Order summary</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Item</strong></td>
        							<td class="text-center"><strong>Price</strong></td>
        							<td class="text-center"><strong>Quantity</strong></td>
        							<td class="text-right"><strong>Totals</strong></td>
                                </tr>
    						</thead>
    						<tbody>
<?php

for($count = 0; $count<count($itemstype); $count++){

    $items_type_clean = mysqli_real_escape_string($conn, $itemstype[$count]);
    $items_id_clean = mysqli_real_escape_string($conn, $itemsid[$count]);
    $items_name_clean = mysqli_real_escape_string($conn, $itemsname[$count]);
    $items_size_clean = mysqli_real_escape_string($conn, $itemssize[$count]);
    $items_qty_clean = mysqli_real_escape_string($conn, $itemsqty[$count]);
		$items_price_clean = mysqli_real_escape_string($conn, $itemsprice[$count]);

    $total=$items_qty_clean*$items_price_clean;
    $grandtotal+=$total;
    //echo $items_type_clean.$items_id_clean.$items_name_clean.$items_size_clean.$items_qty_clean.$items_price_clean;
    echo '
    <tr>
        <td>'.$items_name_clean.'</td>
        <td class="text-center">$ '.$items_price_clean.'</td>
        <td class="text-center">'.$items_qty_clean.'</td>
        <td class="text-right">$ '.$total.'</td>
    </tr>
    ';
}

?>

    							<tr>
    								<td class="thick-line"></td>
    								<td class="thick-line"></td>
    								<td class="thick-line text-center"><strong>Subtotal ៛</strong></td>
    								<td class="thick-line text-right"><?php echo $grandtotal; ?></td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Discount %</strong></td>
    								<td class="no-line text-right" id="grand_discount"><?php echo $itemsdiscount; ?></td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Total ៛</strong></td>
    								<td class="no-line text-right" id="grand_total"><?php echo $grandtotal-($grandtotal*$itemsdiscount/100); ?></td>
									</tr>


    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="invoice_prt">Print</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


