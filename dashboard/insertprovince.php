<?php
//insert.php
include('action/connect.php');
if(isset($_POST["item_name"]))
{
   $item_name = $_POST["item_name"];
   $item_name_eng = $_POST["item_name_eng"];
   $query = '';
   for($count = 0; $count<count($item_name); $count++)
   {
    $item_name_clean = mysqli_real_escape_string($conn, $item_name[$count]);
    $item_name_eng_clean= mysqli_real_escape_string($conn, $item_name_eng[$count]);
    if($item_name_clean != '' && $item_name_eng_clean != '')
    {
     $query .= '
     INSERT INTO tbl_province(province, province_eng) 
     VALUES("'.$item_name_clean.'", "'.$item_name_eng_clean.'");
     ';
    }
   }
   if($query != '')
   {
    if(mysqli_multi_query($conn, $query))
    {
     echo 'Item Data Inserted';
    }
    else
    {
     echo 'Error'.$item_name_clean;
    }
   }
   else
   {
    echo 'All Fields are Required';
   }
  }
?>
