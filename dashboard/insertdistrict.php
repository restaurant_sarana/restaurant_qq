<?php
//insert.php
include('action/connect.php');
if(isset($_POST["item_name"]))
{
   $province = $_POST["province"];
   $item_name = $_POST["item_name"];
   $item_name_eng = $_POST["item_name_eng"];
   $query = '';
   for($count = 0; $count<count($item_name); $count++)
   {
    $province_clean = mysqli_real_escape_string($conn, $province[$count]);
    $item_name_clean = mysqli_real_escape_string($conn, $item_name[$count]);
    $item_name_eng_clean= mysqli_real_escape_string($conn, $item_name_eng[$count]);
    if($province_clean !='' && $item_name_clean != '' && $item_name_eng_clean != '')
    {
     $query .= '
     INSERT INTO tbl_district(province_id, district, district_eng) 
     VALUES("'.$province_clean.'","'.$item_name_clean.'", "'.$item_name_eng_clean.'");
     ';
    }
   }
   if($query != '')
   {
    if(mysqli_multi_query($conn, $query))
    {
     echo 'Item Data Inserted';
    }
    else
    {
     echo 'Error';
    }
   }
   else
   {
    echo 'All Fields are Required';
   }
  }
?>
