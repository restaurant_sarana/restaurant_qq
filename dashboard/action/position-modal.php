<?php
include('connect.php');
$output='';

$output='
<div class="modal fade" id="positionModal" tabindex="-1" role="dialog" aria-labelledby="addPositionModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
	
		<div class="modal-content">
	
			<div class="modal-header">
				<h5 class="modal-title" id="addPositionModal">Add Position</h5>
     			
			</div>
			<div class="modal-body">
				<form method="post" id="frm_position" enctype="multipart/form-data">
					<input type="hidden" name="add_edit_prod" id="add_edit_prod" value="0">
					<input type="hidden" name="txt_id" id="txt_id">
					
					<div class="form-group label-floating">
					<label class="control-label">តួនាទី</label>
					<input type="text" class="form-control" name="txt_position" id="txt_position">
					</div>
					
					<div class="form-group label-floating">
					<label class="control-label">Position</label>
					<input type="text" class="form-control" name="txt_position_eng" id="txt_position_eng">
					</div><br>

					<input type="submit" class="btn btn-primary" style="float: right;" name="submit" id="save-position" value="Add">
				</form>
			</div>
			<div class="modal-footer">
				

			</div>
	
		</div>
	
	</div>
</div>
<script>
$(document).ready(function(){
$(".selectpicker").selectpicker("refresh");
});
//Save Customer
		$("#frm_position").on("submit",function(e){
			e.preventDefault();
			var frm_data=new FormData(this.closest("form"));
			save_position(frm_data);
	
		});
</script>
';
echo $output;
?>
