<?php
include('connect.php');
$output='';
$sql="SELECT * FROM food_category WHERE status='1' AND type='F'";
$result=$conn->query($sql);
$output='
<div class="modal fade" id="foodModal" tabindex="-1" role="dialog" aria-labelledby="addFoodModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
	
		<div class="modal-content">
	
			<div class="modal-header">
				<h5 class="modal-title" id="addFoodModal">Add Food</h5>
     			
			</div>
			<div class="modal-body">
				<form method="post" id="frm_product" enctype="multipart/form-data">
					<input type="hidden" name="add_edit_prod" id="add_edit_prod" value="0">
					<input type="hidden" name="txt_id" id="txt_id">
					<div class="form-group">
						<label for="category">Category</label>
						<select class="form-control selectpicker" data-style="btn btn-link" id="category" name="category">
						<option value="">--Select One--</option>
';
while($row=$result->fetch_array()){
	$output.= '

							
							<option value="'.$row[0].'">'.$row[2].'</option>
						
';
}
$output.='
						</select>
					</div>
					<div class="form-group label-floating">
					<label class="control-label">Food</label>
					<input type="text" class="form-control" name="txt_food" id="txt_food">
					</div><br>
					<div class="form-group label-floating">
						<label class="control-label">Price ($)</label>
						<input type="text" class="form-control" name="txt_price" id="txt_price">
					</div>
					
					<label>Image</label><br>
					<div class="fileinput fileinput-new text-center" data-provides="fileinput">
					<div class="fileinput-new thumbnail">
					<img src="assets/img/image_placeholder.jpg" alt="...">
					</div>
					<div class="fileinput-preview fileinput-exists thumbnail"></div>
					<input type="hidden" class="fileinput-filename" name="imgname" id="imgname">
					<div>
					<span class="btn btn-rose btn-round btn-file">
					<span class="fileinput-new">Select image</span>
					<span class="fileinput-exists">Change</span>
					<input type="file" name="myimage" id="myimage">
					</span>
					<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
					</div>
					</div>
					<div class="form-group">
						<label for="status1">Status</label>
						<select class="form-control selectpicker" data-style="btn btn-link" id="status1" name="txt_status">
							<option value="1">Active</option>
							<option value="2">Inactive</option>
						</select>
					</div>
					<br>
					<input type="submit" class="btn btn-primary" style="float: right;" name="submit" id="save-food" value="Add">
				</form>
			</div>
			<div class="modal-footer">
				

			</div>
	
		</div>
	
	</div>
</div>
<script>
$(document).ready(function(){
$(".selectpicker").selectpicker("refresh");
});
//Save Product
		$("#frm_product").on("submit",function(e){
			e.preventDefault();
			var frm_data=new FormData(this.closest("form"));
			save_food(frm_data);
	
		});
</script>
';
echo $output;
?>
