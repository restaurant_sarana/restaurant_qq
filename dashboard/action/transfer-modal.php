<?php
include('connect.php');
$output='';
$sql="SELECT * FROM tbl_category WHERE status='1'";
$sql_branch="SELECT * FROM tbl_carbranch";
$result_branch=$conn->query($sql_branch);
$result=$conn->query($sql);
$output='
<div class="modal fade" id="transferModal" tabindex="-1" role="dialog" aria-labelledby="addTransferModal" aria-hidden="true">
	<div class="modal-dialog" role="document" style="width:960px;">
	
		<div class="modal-content">
	
			<div class="modal-header">
				<h5 class="modal-title" id="addTransferModal">Add Transfer Product</h5>
     			
			</div>
			<div class="modal-body">
				<form method="post" id="frm_transfer" enctype="multipart/form-data">
					<input type="hidden" name="add_edit_prod" id="add_edit_prod" value="0">
					<input type="hidden" name="txt_id" id="txt_id">
					<div class="form-group">
						<label for="txt_transfer_to_branch">Transfer to Branch</label>
						<select class="form-control selectpicker" data-style="btn btn-link" id="txt_transfer_to_branch" name="txt_transfer_to_branch">
							<option value="">--Select Branch--</option>
							';
							while($row=$result_branch->fetch_array()){
							$output.= '
							<option value="'.$row[0].'">'.$row[1]."(".$row[2].")".'</option>
							';
							}
							$output.='
						</select>
					</div>
					
					<a href="#" class="btn btn-success add-row"><i class="material-icons">add</i></a>
					<table class="table table-striped table-no-bordered table-hover">
						<thead>
							<tr>
								<th width="20%">Category</th>
								<th width="20%">Product</th>
								<th width="10%">Cost</th>
								<th width="10%" colspan="2">Qty</th>
								<th width="10%">Total</th>
								<th width="5%">Action</th>
							</tr>
						<thead>
						<tbody id="table_body">
							<tr id="row0">
								<td>
									<select class="form-control selectpicker" data-style="btn btn-link" id="category" name="category[]">
									<option value="">--Select One--</option>
										';
										while($row=$result->fetch_array()){
										$output.= '
										<option value="'.$row[0].'">'.$row[1].'</option>
										';
										}
										$output.='
									</select>
								</td>
								<td>
									<select class="form-control selectpicker" data-style="btn btn-link" id="product" name="product[]">
									</select>
								</td>
								<td>
									<div class="form-group">
										<input type="text"  name="txt_cost[]" class="form-control txt_cost" readonly>
									</div>
								</td>
								<td width="5%">
									<div class="form-group">
										<input type="text"  name="txt_qty[]" class="form-control txt_qty" value="1">
									</div>
								</td>
								<td width="15%">
								<div class="btn-group">
									<a class="btn btn-round btn-info btn-xs remove-amount"> <i class="material-icons">remove</i> </a>
									<a class="btn btn-round btn-info btn-xs add-amount"> <i class="material-icons">add</i> </a>
								</div>
								</td>
								<td>
									<div class="form-group">
										<input type="text"  name="txt_total[]" class="form-control txt_total" readonly>
									</div>
								</td>
								<td></td>
							</tr>
						<tbody>
						<tfoot>
							<th></th>
							<th></th>
							<th>Total : </th>
							<th colspan="2">
								<div class="form-group">
									<input type="text"  name="txt_qty_total" class="form-control txt_qty_total" readonly>
								</div>
							</th>
							<th colspan="2">
								<div class="form-group">
									<input type="text"  name="txt_cost_total" class="form-control txt_cost_total" readonly>
								</div>
							</th>
						</tfoot>
					</table>
					
					<input type="submit" class="btn btn-primary" style="float: right;" name="submit" id="save-stock" value="Add">
				</form>
			</div>
			<div class="modal-footer">
				

			</div>
	
		</div>
	
	</div>
</div>
<script>
$(document).ready(function(){
$(".selectpicker").selectpicker("refresh");
});
//Save Stock Product
		$("#frm_transfer").on("submit",function(e){
		
			e.preventDefault();
			var frm_data=new FormData(this.closest("form"));
			save_transfer(frm_data);
	
		});
</script>
';
echo $output;
?>
