<?php
include('connect.php');
$output='';

$output='
	<div class="modal fade" id="foodCategoryModal" tabindex="-1" role="dialog" aria-labelledby="addFoodCategoryModal" aria-hidden="true">
		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">
					<h5 class="modal-title" id="addFoodCategoryModal">Add Food Category</h5>

				</div>
				<div class="modal-body">
					<form method="post" id="frm_category" enctype="multipart/form-data">
						<input type="hidden" name="add_edit_cate" id="add_edit_cate" value="0">
						<input type="hidden" name="txt_id" id="txt_id">
						<div class="form-group">
							<label for="txt_type">Type</label>
							<select class="form-control selectpicker" data-style="btn btn-link" id="txt_type" name="txt_type">
								<option value="F">Food</option>
								<option value="D">Drink</option>
							</select>
						</div>
						<div class="form-group label-floating">
						<label class="control-label">Category Name</label>
						<input type="text" class="form-control" name="txt_category" id="txt_category">
						</div>

						<div class="fileinput fileinput-new text-center" data-provides="fileinput">
						<div class="fileinput-new thumbnail">
						<img src="assets/img/image_placeholder.jpg" alt="...">
						</div>
						<div class="fileinput-preview fileinput-exists thumbnail"></div>
						<input type="hidden" class="fileinput-filename" name="imgname" id="imgname">
						<div>
						<span class="btn btn-rose btn-round btn-file">
						<span class="fileinput-new">Select image</span>
						<span class="fileinput-exists">Change</span>
						<input type="file" name="myimage" id="myimage">
						</span>
						<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
						</div>
						</div>
						<div class="form-group">
							<label for="status">Status</label>
							<select class="form-control selectpicker" data-style="btn btn-link" id="status" name="txt_status">
								<option value="1">Active</option>
								<option value="2">Inactive</option>
							</select>
						</div>
						<br>
						<input type="submit" class="btn btn-primary" style="float: right;" id="save-foodCategory" value="Add">
					</form>
				</div>
				<div class="modal-footer">


				</div>

			</div>

		</div>
	</div>
<script>
$(document).ready(function(){
$(".selectpicker").selectpicker("refresh");
});
//Save category
		$("#frm_category").on("submit",function(e){
			e.preventDefault();
			var frm_data=new FormData(this.closest("form"));
			save_foodCategory(frm_data);
		});
</script>
';
echo $output;
?>
