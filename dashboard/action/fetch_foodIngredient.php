<?php
include('connect.php');
$output='';
$sql='SELECT ingredients.id, ingredients.itemsName, ingredients.unit_cost, ingredients.scale_id, ingredients.img, ingredients.status, scale.id, scale.scale  FROM ingredients INNER JOIN scale ON ingredients.scale_id=scale.id';

$result=$conn->query($sql);
$output.='
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-icon" data-background-color="purple">
					<i class="material-icons">assignment</i>
				</div>
				<div class="card-content">
					<h4 class="card-title">Ingredient</h4>
					<div class="toolbar">
						<button class="btn btn-info" id="add-ingredient">
							<span class="btn-label">
							<i class="material-icons">shopping_cart</i>
							</span>
						Add Ingredient
						</button> 
					</div>
					<div class="responsive material-datatables">
						<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
							<thead>
								<tr>
									<th width="5%">ID</th>
									<th width="25%">Product</th>
									<th width="5%">Cost</th>
									<th width="5%">Scale</th>
									<th width="10%">Image</th>
									<th width="10%">Status</th>
									<th width="20%" class="disabled-sorting text-right">Actions</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>ID</th>
									<th>Product</th>
									<th>Cost</th>
									<th>Scale</th>
									<th>Image</th>
									<th>Statu</th>
									<th>Actions</th>
								</tr>
							</tfoot>
							<tbody>
								
';
while($row=$result->fetch_array()){
	$output .= '
	
		<tr>
			<td>'.$row[0].'</td>
			<td>'.$row[1].'</td>
			<td>'.$row[2].'</td>
			<td>'.'/'.$row['scale'].'<input type="hidden" value='.$row['scale_id'].'></td>
			<td align="center"><img src="img/'.$row[4].'" class="img-rounded" style="height: 100px; width: auto;"></td>
			<td>'.$row[5].'</td>
			<td class="text-right">
			<a href="#" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>
			<a href="#" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons">close</i></a>
			</td>
		</tr>
							
	';
}
$output.='
</tbody>
						</table>
					</div>
				</div>
                           <!-- end content-->
			</div>
                       <!--  end card  -->
		</div>
                        <!-- end col-md-12 -->
	</div>
                    <!-- end row -->
</div>
<script src="assets/js/datatable.js"></script>
';

echo $output;
?>
