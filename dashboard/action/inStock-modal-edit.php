<?php
include('connect.php');
$output='';
$sql="SELECT * FROM food_category WHERE status='1' AND type='D'";
$result=$conn->query($sql);
$id=$_POST['id'];
$category=$_POST['category'];
$product=$_POST['product'];
$qty=$_POST['qty'];
$cost=$_POST['cost'];
$subtotal=$_POST['subtotal'];

$output='
<div class="modal fade" id="inStockModal" tabindex="-1" role="dialog" aria-labelledby="addStockModal" aria-hidden="true">
	<div class="modal-dialog" role="document" style="width:960px;">
	
		<div class="modal-content">
	
			<div class="modal-header">
				<h5 class="modal-title" id="addStockModal">Add Stock Product</h5>
     			
			</div>
			<div class="modal-body">
				<form method="post" id="frm_stock" enctype="multipart/form-data">
					<input type="hidden" name="add_edit_prod" id="add_edit_prod" value="0">
					<input type="hidden" name="txt_id" id="txt_id" value="'.mysqli_real_escape_string($conn, $id[1]).'">
					<table class="table table-striped table-no-bordered table-hover">
						<thead>
							<tr>
								<th width="20%">Category</th>
								<th width="20%">Product</th>
								<th width="10%">Cost</th>
								<th width="10%">Qty</th>
								<th width="10%">Total</th>
								
							</tr>
						<thead>
						<tbody id="table_body">
';

for($count = 1; $count<count($category); $count++)
{
	
	$id_clean = mysqli_real_escape_string($conn, $id[$count]);
	$category_clean = mysqli_real_escape_string($conn, $category[$count]);
	$product_clean = mysqli_real_escape_string($conn, $product[$count]);
	$qty_clean = mysqli_real_escape_string($conn, $qty[$count]);
	$cost_clean = mysqli_real_escape_string($conn, $cost[$count]);
	$subtotal_clean = mysqli_real_escape_string($conn, $subtotal[$count]);
	
	$product_name="SELECT soft_drink.id,soft_drink.category_softdrink_id,soft_drink.name FROM soft_drink WHERE id='$product_clean'";
	$category_name="SELECT * FROM food_category WHERE type='D' AND id='$category_clean'";
	$response_product=$conn->query($product_name);
	$response_category=$conn->query($category_name);
	$row_product=$response_product->fetch_array();
	$row_category=$response_category->fetch_array();
	$output.='
	<tr id="row'.$count.'">
		<td>
			<div class="form-group">
				<input type="text" class="form-control" value="'.$row_category[2].'" readonly>
			</div>
			<input type="hidden" id="category" name="category[]" value="'.$category_clean.'">
		</td>
		<td>
			<div class="form-group">
				<input type="text" class="form-control" value="'.$row_product[2].'" readonly>
			</div>
			<input type="hidden"  id="product" name="product[]" value="'.$product_clean.'">
		</td>
		<td>
			<div class="form-group">
				<input type="text"  name="txt_cost[]" class="form-control txt_cost" value="'.$cost_clean.'" readonly>
			</div>
		</td>
		<td>
			<div class="form-group">
				<input type="text"  name="txt_qty[]" class="form-control txt_qty"  value="'.$qty_clean.'" readonly>
			</div>
		</td>
		
		<td>
			<div class="form-group">
				<input type="text"  name="txt_total[]" class="form-control txt_total"  value="'.$subtotal_clean.'" readonly>
			</div>
		</td>
	</tr>
	';
}
$output.='

						<tbody>
						<tfoot>
							<th></th>
							<th></th>
							<th>Total : </th>
							<th>
								<div class="form-group">
									<input type="text"  name="txt_qty_total" class="form-control txt_qty_total" readonly>
								</div>
							</th>
							<th colspan="2">
								<div class="form-group">
									<input type="text"  name="txt_cost_total" class="form-control txt_cost_total" readonly>
								</div>
							</th>
						</tfoot>
					</table>
					
					<input type="submit" class="btn btn-primary" style="float: right;" name="submit" id="save-stock" value="Delete">
				</form>
			</div>
			<div class="modal-footer">
				

			</div>
	
		</div>
	
	</div>
</div>
<script>
$(document).ready(function(){
$(".selectpicker").selectpicker("refresh");
});
//Save Stock Product
		$("#frm_stock").on("submit",function(e){
			
			e.preventDefault();
			var frm_data=new FormData(this.closest("form"));
			save_stock(frm_data);
	
		});
</script>
';
echo $output;
?>
