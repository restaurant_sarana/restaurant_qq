<?php
include('connect.php');
$output='';
$branch=$_POST['branch'];
$sql='SELECT tbl_product.id,tbl_product.category_id,tbl_category.id,tbl_category.category,tbl_product.product,tbl_product.cost,tbl_product.price,tbl_product.qty,tbl_product.img,tbl_product.status FROM tbl_product INNER JOIN tbl_category ON tbl_product.category_id=tbl_category.id';
$sql_count="SELECT * FROM transfer_details";
$sql_branch="SELECT * FROM tbl_carbranch";
$result_branch=$conn->query($sql_branch);
$result=$conn->query($sql);
$output.='
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-icon" data-background-color="purple">
					<i class="material-icons">assignment</i>
				</div>
				<div class="card-content">
					<h4 class="card-title">Product</h4>
					<div class="form-group">
						<label for="select_branch">Select Branch</label>
						<select data-style="btn btn-link" id="select_branch" name="select_branch">
							<option value="0">All</option>
							';
							while($row=$result_branch->fetch_array()){
							$output.= '
							<option value="'.$row[0].'">'.$row[1]."(".$row[2].")".'</option>
							';
							}
							$output.='
						</select>
					</div>
					
					<div class="responsive material-datatables">
						<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
							<thead>
								<tr>
									<th width="5%">ID</th>
									<th width="20%">Catagory Name</th>
									<th width="25%">Product</th>
									<th width="5%">Cost</th>
									<th width="5%">Price</th>
									<th width="10%">Qty</th>
									<th width="10%">Image</th>
									<th width="10%">Status</th>
									<th width="20%" class="disabled-sorting text-right">Actions</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>ID</th>
									<th>CatagoryName</th>
									<th>Product</th>
									<th>Cost</th>
									<th>Price</th>
									<th>Qty</th>
									<th>Image</th>
									<th>Statu</th>
									<th>Actions</th>
								</tr>
							</tfoot>
							<tbody>
								
';
while($row=$result->fetch_array()){
	$output .= '
	
		<tr>
			<td>'.$row[0].'</td>
			<td>'.$row['category'].'<input type="hidden" value='.$row['category_id'].'>
			</td>
			<td>'.$row['product'].'</td>
			<td>'.$row['cost'].'</td>
			<td>'.$row['price'].'</td>
			';
	
			$sql_count='SELECT sum(qty) FROM transferproduct_details WHERE product_id="'.$row[0].'" AND branch_id="'.$branch.'" AND status=1';
			$result_count=$conn->query($sql_count);
			$row_count=$result_count->fetch_array();
			$output.='
			<td>'.$row_count[0].'</th>
			<td align="center"><img src="img/'.$row['img'].'" class="img-rounded" style="height: 100px; width: auto;"></td>
			<td>'.$row['status'].'</td>
			<td class="text-right">
			<a href="#" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>
			<a href="#" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons">close</i></a>
			</td>
		</tr>
							
	';
}
$output.='
</tbody>
						</table>
					</div>
				</div>
                           <!-- end content-->
			</div>
                       <!--  end card  -->
		</div>
                        <!-- end col-md-12 -->
	</div>
                    <!-- end row -->
</div>
<script src="assets/js/datatable.js"></script>

';

echo $output;
?>
