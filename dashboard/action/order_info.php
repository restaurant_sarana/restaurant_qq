<?php
include('connect.php');

$order_id=$_POST['order_id'];
$sql="select order_food.defacno,order_food.date,payment.payment_date,
case when order_food_details.type='F' then 'Food' else 'Drink' end type,
case when order_food_details.type='F' then (select food from food_items where id=order_food_details.items_id)
  else (select name from soft_drink where id=order_food_details.items_id) end as items,
order_food_details.qty,order_food_details.price,payment.amount_total,payment.discount,payment.payment_receive,payment.change_payment from order_food_details
left join order_food on order_food_details.defacno=order_food.defacno
left join payment on order_food.defacno=payment.defacno
where order_food_details.defacno='$order_id' order by type;";

$result_order = $connect->prepare($sql);
$result_order->execute(); 

$count=1;
?>
<div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="addStockModal" aria-hidden="true">
	<div class="modal-dialog" role="document" style="width:960px;">
	
		<div class="modal-content">
	
			<div class="modal-header">
				<h5 class="modal-title" id="addStockModal">View Order</h5>
     			
			</div>
			<div class="modal-body">
				<form method="post" id="frm_stock" enctype="multipart/form-data">
					<input type="hidden" name="add_edit_prod" id="add_edit_prod" value="0">
					<input type="hidden" name="txt_id" id="txt_id" value="<?php echo $order_id ?>">
					<table class="table table-striped table-no-bordered table-hover">
						<thead>
							<tr>
								<th width="20%">Type</th>
								<th width="20%">Items</th>
								<th width="10%">Qty</th>
								<th width="10%">price</th>
								<th width="10%">Total</th>
								
							</tr>
						<thead>
						<tbody id="table_body">
<?php
$sumtotal=0;
while($row = $result_order->fetch()){
$total=$row[5]*$row[6];
$sumtotal+=$total;
?>
	<tr id="row<?php echo $count; ?>">
		<td>
			<?php echo $row[3];  ?>
		</td>
		<td>
			<?php echo $row[4];  ?>
		</td>
		<td>
			<?php echo $row[5];  ?>
		</td>
		<td>
			<?php echo $row[6];  ?>
		</td>
		<td>
			<?php echo $total;  ?>
		</td>
	</tr>
<?php
$count++;
}
?>
						<tbody>
						<tfoot>
							<th></th>
							<th></th>
							<th>Total : </th>
							<th>
								
							</th>
							<th colspan="2">
								<?php echo $sumtotal; ?>
							</th>
						</tfoot>
					</table>
					<!-- <?php
					// if($row[7][0]=='Paid'){
					// 	echo '<input type="submit" class="btn btn-primary" style="float: right;" name="submit" id="reverse-order" value="Unpaid">';
					// }
					?> -->
					
					<input type="submit" class="btn btn-primary" style="float: right;" name="submit" id="delete-order" value="Delete">
				</form>
			</div>
			<div class="modal-footer">
				

			</div>
	
		</div>
	
	</div>
</div>
<script>
$(document).ready(function(){
$(".selectpicker").selectpicker("refresh");
});
//Save Stock Product
		// $("#frm_stock").on("submit",function(e){
			
		// 	e.preventDefault();
		// 	var frm_data=new FormData(this.closest("form"));
		// 	save_stock(frm_data);
	
		// });
</script>
