<?php
include('connect.php');
$fetch_order="SELECT COUNT(*) FROM Order_Food where type<>'C' and DATE_FORMAT(date,'%Y-%m-%d')=DATE_FORMAT(sysdate(),'%Y-%m-%d');";
$result = $connect->prepare($fetch_order);
$result->execute(); 
$number_of_rows = $result->fetchColumn();

$fetch_income="select SUM(amount_total) from payment where DATE_FORMAT(payment_date,'%Y-%m-%d')=DATE_FORMAT(sysdate(),'%Y-%m-%d');";
$result_income = $connect->prepare($fetch_income);
$result_income->execute(); 
$income = $result_income->fetchColumn();

$order="select defacno,date,paid_date,(select name from desk where id=order_food.table_id) as desk,
case when type='P' then 'Paid' when type='C' then 'Cancel' else 'Not Paid' end status
from order_food where DATE_FORMAT(order_food.date,'%Y-%m-%d')=DATE_FORMAT(sysdate(),'%Y-%m-%d') order by id desc;";
$result_order = $connect->prepare($order);
$result_order->execute(); 

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="card card-stats">
                <div class="card-header" data-background-color="orange">
                    <i class="material-icons">store</i>
                </div>
                <div class="card-content">
                    <p class="category">Order</p>
                    <h3 class="card-title" id="count_order"><?php echo $number_of_rows; ?></h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">update</i> Just Updated
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="material-icons">account_balance</i>
                </div>
                <div class="card-content">
                    <p class="category">Revenue</p>
                    <h3 class="card-title" id="sum_income"><?php echo $income; ?></h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">update</i> Just Updated
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">swap_horiz</i>
                </div>
                <div class="card-content">
                    <p class="category">Expense</p>
                    <h3 class="card-title" id="count_order"><?php echo "0"; ?></h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">update</i> Just Updated
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <a href="http://localhost:8080/QQ/dashboard/report.php">
                    <!--                                    <a href="http://localhost/dashboard/report.php">-->
                    <button class="btn btn-social btn-fill btn-reddit">
                        <i class="fa fa-tasks"></i> Report
                    </button>
                </a>
            </div>
        </div>
    </div>
			<div class="card">
				<div class="card-header card-header-icon" data-background-color="purple">
					<i class="material-icons">assignment</i>
				</div>
				<div class="card-content">
					<h4 class="card-title">Order List</h4>
					<!-- <div class="toolbar">
						<button class="btn btn-info" id="add-desk">
							<span class="btn-label">
							<i class="material-icons">shopping_cart</i>
							</span>
						Add Position
						</button> 
					</div> -->
					<div class="responsive material-datatables">
						<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
							<thead>
								<tr>
									<th width="5%">Order ID</th>
									<th width="10%">Order Date</th>
                                    <th width="10%">Payment Date</th>
									<th width="10%">Table</th>
                                    <th width="10%">Status</th>
									<th width="10%" class="disabled-sorting text-right">Actions</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
                                    <th>Order ID</th>
									<th>Order Date</th>
                                    <th>Payment Date</th>
									<th>Table</th>
                                    <th>Status</th>
									<th class="text-right">Actions</th>
								</tr>
							</tfoot>
							<tbody>
<?php
while($row = $result_order->fetch()){
    echo '
        <tr>
            <td>'.$row[0].'</td>
            <td>'.$row[1].'</td>
            <td>'.$row[2].'</td>
            <td>'.$row[3].'</td>
            <td>'.$row[4].'</td>
            <td class="text-right">
			<a href="#" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>
            </td>
        </tr>
    ';
}
?>

							
                            </tbody>
						</table>
					</div>
				</div>
                           <!-- end content-->
			</div>
                       <!--  end card  -->

</div>

<script src="assets/js/datatable.js"></script>