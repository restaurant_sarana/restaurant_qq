<?php
include('connect.php');
$output='';
$sql='SELECT * FROM transferproduct WHERE status=1';

$result=$conn->query($sql);
$output.='
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-icon" data-background-color="purple">
					<i class="material-icons">assignment</i>
				</div>
				<div class="card-content">
					<h4 class="card-title">Transfer to Branch</h4>
					<div class="toolbar">
						<button class="btn btn-info" id="transfer-stock">
							<span class="btn-label">
							<i class="material-icons">shopping_cart</i>
							</span>
						Add Stock to Branch
						</button> 
					</div>
					<div class="responsive material-datatables">
						<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
							<thead>
								<tr>
									<th width="5%">Inoice Id</th>
									<th width="20%">Date</th>
									<th width="25%">Total Product</th>
									<th width="5%">Branch</th>
									<th width="20%" class="disabled-sorting text-right">Actions</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Inoice Id</th>
									<th>Date</th>
									<th>Total Product</th>
									<th>Branch</th>
									<th>Actions</th>
								</tr>
							</tfoot>
							<tbody>
								
';
while($row=$result->fetch_array()){
	$output .= '
	
		<tr>
			<td>'.$row[0].'</td>
			<td>'.$row['date'].'</td>
			<td>'.$row['total_product'].'</td>
			<td>'.$row['branch_id'].'</td>
			<td class="text-right">
			<a href="#" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>
			<a href="#" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons">close</i></a>
			</td>
		</tr>
							
	';
}
$output.='
</tbody>
						</table>
					</div>
				</div>
                           <!-- end content-->
			</div>
                       <!--  end card  -->
		</div>
                        <!-- end col-md-12 -->
	</div>
                    <!-- end row -->
</div>
<script src="assets/js/datatable.js"></script>
';

echo $output;
?>
