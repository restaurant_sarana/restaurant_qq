<?php
include('connect.php');
$output='';
$sql="SELECT * FROM tbl_province";
$result=$conn->query($sql);

$output='
<div class="modal fade" id="CompanyModal" tabindex="-1" role="dialog" aria-labelledby="addCompanyModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
	
		<div class="modal-content">
	
			<div class="modal-header">
				<h5 class="modal-title" id="addCompanyModal">Add Company</h5>
     			
			</div>
			<div class="modal-body">
				<form method="post" id="frm_customer" enctype="multipart/form-data">
					<input type="hidden" name="add_edit_prod" id="add_edit_prod" value="0">
					<input type="hidden" name="txt_id" id="txt_id">
					<div class="form-group">
						<label for="province">Provice</label>
						<select class="form-control selectpicker" data-style="btn btn-link" id="province" name="province">
							<option value="">--Select One--</option>
							';
							while($row=$result->fetch_array()){
							$output.= '
							<option value="'.$row[0].'">'.$row[1].'</option>
							';
							}
							$output.='
						</select>
					</div>
					<div class="form-group">
						<label for="district">District</label>
						<select class="form-control selectpicker" data-style="btn btn-link" id="district" name="district">
							
						</select>
					</div>
					<div class="form-group label-floating">
					<label class="control-label">Company Name</label>
					<input type="text" class="form-control" name="txt_company" id="txt_company">
					</div><br>

                    <div class="form-group">
                        <div class="form-group label-floating">
                            <label class="control-label">Address</label>
                            <textarea class="form-control" rows="2" id="txt_address" name="txt_address"></textarea>
                        </div>
                    </div>
					<div class="form-group label-floating">
						<label class="control-label">Phone Number</label>
						<input type="text" class="form-control" name="txt_phoneNumber" id="txt_phoneNumber">
					</div>
					<div class="form-group label-floating">
						<label class="control-label">Email</label>
						<input type="text" class="form-control" name="txt_email" id="txt_email">
					</div>
					<label>Image</label><br>
					<div class="fileinput fileinput-new text-center" data-provides="fileinput">
					<div class="fileinput-new thumbnail">
					<img src="assets/img/image_placeholder.jpg" alt="...">
					</div>
					<div class="fileinput-preview fileinput-exists thumbnail"></div>
					<input type="hidden" class="fileinput-filename" name="imgname" id="imgname">
					<div>
					<span class="btn btn-rose btn-round btn-file">
					<span class="fileinput-new">Select image</span>
					<span class="fileinput-exists">Change</span>
					<input type="file" name="myimage" id="myimage">
					</span>
					<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
					</div>
					</div>
					
					<br>
					<input type="submit" class="btn btn-primary" style="float: right;" name="submit" id="save-company" value="Add">
				</form>
			</div>
			<div class="modal-footer">
				

			</div>
	
		</div>
	
	</div>
</div>
<script>
$(document).ready(function(){
$(".selectpicker").selectpicker("refresh");
});
//Save Customer
		$("#frm_customer").on("submit",function(e){
			e.preventDefault();
			var frm_data=new FormData(this.closest("form"));
			save_supplierCompany(frm_data);
			
		});
</script>
';
echo $output;
?>
