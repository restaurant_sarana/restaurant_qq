<?php
include('connect.php');
$output='';
$sql='SELECT user1.id,user1.user_name,user1.user_create,user2.user_name FROM tbl_user AS user1 INNER JOIN tbl_user AS user2 WHERE user1.user_create=user2.id';

$result=$conn->query($sql);
$output.='
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-icon" data-background-color="purple">
					<i class="material-icons">assignment</i>
				</div>
				<div class="card-content">
					<h4 class="card-title">User</h4>
					<div class="toolbar">
						<button class="btn btn-info" id="add-user">
							<span class="btn-label">
							<i class="material-icons">supervised_user_circle</i>
							</span>
						Add User
						</button> 
					</div>
					<div class="responsive material-datatables">
						<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
							<thead>
								<tr>
									<th width="5%">ID</th>
									<th>Username</th>
									<th width="20%">User Create</th>
									<th width="20%" class="disabled-sorting text-right">Actions</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>ID</th>
									<th>Username</th>
									<th>User Create</th>
									<th>Actions</th>
								</tr>
							</tfoot>
							<tbody>
								
';
while($row=$result->fetch_array()){
	$output .= '
	
		<tr>
			<td>'.$row[0].'</td>
            <td>'.$row[1].'</td>
            <td>'.$row[3].'</td>
			<td class="text-right">
			<a href="#" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>
			<a href="#" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons">close</i></a>
			</td>
		</tr>
							
	';
}
$output.='
</tbody>
						</table>
					</div>
				</div>
                           <!-- end content-->
			</div>
                       <!--  end card  -->
		</div>
                        <!-- end col-md-12 -->
	</div>
                    <!-- end row -->
</div>
<script src="assets/js/datatable.js"></script>
';

echo $output;
?>
