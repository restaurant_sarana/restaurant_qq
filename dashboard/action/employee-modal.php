<?php
include('connect.php');
$output='';
$dateofbirth=$_POST['dob'];
$sql="SELECT * FROM tbl_province";
$sql_pos="SELECT * FROM tbl_positions";
$result=$conn->query($sql);
$result_car=$conn->query($sql_car);
$result_pos=$conn->query($sql_pos);
$output='
<div class="modal fade" id="employeeModal" tabindex="-1" role="dialog" aria-labelledby="addEmployeeModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
	
		<div class="modal-content">
	
			<div class="modal-header">
				<h5 class="modal-title" id="addEmployeeModal">Add Employee</h5>
     			
			</div>
			<div class="modal-body">
				<form method="post" id="frm_employee" enctype="multipart/form-data">
					<input type="hidden" name="add_edit_prod" id="add_edit_prod" value="0">
					<input type="hidden" name="txt_id" id="txt_id">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label">First Name</label>
								<input type="text" class="form-control" name="txt_firstName" id="txt_firstName">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group label-floating">
								<label class="control-label">Last Name</label>
								<input type="text" class="form-control" name="txt_lastName" id="txt_lastName">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="txt_sex">Sex</label>
						<select class="form-control selectpicker" data-style="btn btn-link" id="txt_sex" name="txt_sex">
							<option value="">--Select One--</option>
							<option value="M">Male</option>
							<option value="F">Female</option>
						</select>
					</div>
					<div class="form-group">
                        <label class="label-control">Date Of Birth</label>
                        <input type="text" name="txt_dateOfBirth" id="txt_dateOfBirth" value="'.$dateofbirth.'" class="form-control datepicker"/>
                    </div>
					<div class="form-group">
                        <div class="form-group label-floating">
                            <label class="control-label">Address</label>
                            <textarea class="form-control" rows="2" id="txt_address" name="txt_address"></textarea>
                        </div>
                    </div>
					<div class="form-group">
						<label for="province">Provice</label>
						<select class="form-control selectpicker" data-style="btn btn-link" id="province" name="province">
							<option value="">--Select One--</option>
							';
							while($row=$result->fetch_array()){
							$output.= '
							<option value="'.$row[0].'">'.$row[1].'</option>
							';
							}
							$output.='
						</select>
					</div>

					<div class="form-group label-floating">
						<label class="control-label">Phone Number</label>
						<input type="text" class="form-control" name="txt_phoneNumber" id="txt_phoneNumber">
					</div>
					<div class="form-group">
						<label for="province">Position</label>
						<select class="form-control selectpicker" data-style="btn btn-link" id="txt_position" name="txt_position">
							<option value="">--Select One--</option>
							';
							while($row=$result_pos->fetch_array()){
							$output.= '
							<option value="'.$row[0].'">'.$row[1].'</option>
							';
							}
							$output.='
						</select>
					</div>
					<div class="form-group label-floating">
					<label class="control-label">Salary($)</label>
					<input type="text" class="form-control" name="txt_salary" id="txt_salary">
					</div>
					
					<label>Employee Photo</label><br>
					<div class="fileinput fileinput-new text-center" data-provides="fileinput">
					<div class="fileinput-new thumbnail">
					<img src="assets/img/image_placeholder.jpg" alt="...">
					</div>
					<div class="fileinput-preview fileinput-exists thumbnail"></div>
					<input type="hidden" class="fileinput-filename" name="imgname" id="imgname">
					<div>
					<span class="btn btn-rose btn-round btn-file">
					<span class="fileinput-new">Select image</span>
					<span class="fileinput-exists">Change</span>
					<input type="file" name="myimage" id="myimage">
					</span>
					<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
					</div>
					</div>
					
					<br>
					<input type="submit" class="btn btn-primary" style="float: right;" name="submit" id="save-employee" value="Add">
				</form>
			</div>
			<div class="modal-footer">
				

			</div>
	
		</div>
	
	</div>
</div>
<script>
$(document).ready(function(){
$(".selectpicker").selectpicker("refresh");
$(".datepicker").datetimepicker({
            format: "MM/DD/YYYY",
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: "fa fa-chevron-left",
                next: "fa fa-chevron-right",
                today: "fa fa-screenshot",
                clear: "fa fa-trash",
                close: "fa fa-remove",
                inline: true
            }
         });
});
//Save Customer
		$("#frm_employee").on("submit",function(e){
			e.preventDefault();
			var frm_data=new FormData(this.closest("form"));
			save_employee(frm_data);
	
		});
</script>
';
echo $output;
?>
