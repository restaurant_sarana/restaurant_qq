<?php
include ('connect.php');
$date=date('Y-m-d');
$userid="001";
$last_id="";
$supplier=$_POST['supplier'][0];

$insert="INSERT INTO ingredients_buy (items_id,date,total_unit,total_cost,supplier_id,user_id,status) VALUES (:items_id,'$date',:total_unit,:total_cost,:supplier_id,:user_id,'1');";
for($count=0;$count<count($_POST['ingredients']);$count++){
    $data = array(
        ':items_id' => $_POST['ingredients'][$count],
        ':total_unit' => $_POST['txt_qty'][$count],
        ':total_cost' => $_POST['txt_cost'][$count]*$_POST['txt_qty'][$count],
        ':supplier_id' => $supplier,
        ':user_id' => $userid
    );
    try{
        $connect->beginTransaction();

        $statement=$connect->prepare($insert);
        $statement->execute($data);
        $connect->commit();

        echo "Success";
    }catch(PDOException $exception){
        $connect->rollBack();
        echo $exception->getMessage();
    }
}