<?php
include('connect.php');
$output='';
$sql="SELECT ingredients.itemsName,ingredients_buy.date,ingredients_buy.total_cost,ingredients_buy.total_unit FROM ingredients_buy INNER join ingredients on ingredients_buy.items_id=ingredients.id where ingredients_buy.status='1'";

$result=$conn->query($sql);
$output.='
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-icon" data-background-color="purple">
					<i class="material-icons">assignment</i>
				</div>
				<div class="card-content">
					<h4 class="card-title">Buy Ingredient</h4>
					<div class="toolbar">
						<button class="btn btn-info" id="add-ingredientBuy">
							<span class="btn-label">
							<i class="material-icons">shopping_cart</i>
							</span>
						Buy Ingredient
						</button> 
					</div>
					<div class="responsive material-datatables">
						<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
							<thead>
								<tr>
									<th width="5%">Item Id</th>
									<th width="20%">Date</th>
									<th width="25%">Total Product</th>
									<th width="5%">Total Cost</th>
									<th width="20%" class="disabled-sorting text-right">Actions</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Items</th>
									<th>Date</th>
									<th>Total Qty</th>
									<th>Total Cost</th>
									<th>Actions</th>
								</tr>
							</tfoot>
							<tbody>
								
';
while($row=$result->fetch_array()){
	$output .= '
	
		<tr>
			<td>'.$row[0].'</td>
			<td>'.$row[1].'</td>
			<td>'.$row[2].'</td>
			<td>'.$row[3].'</td>
			<td class="text-right">
			<a href="#" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>
			<a href="#" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons">close</i></a>
			</td>
		</tr>
							
	';
}
$output.='
</tbody>
						</table>
					</div>
				</div>
                           <!-- end content-->
			</div>
                       <!--  end card  -->
		</div>
                        <!-- end col-md-12 -->
	</div>
                    <!-- end row -->
</div>
<script src="assets/js/datatable.js"></script>
';

echo $output;
?>
