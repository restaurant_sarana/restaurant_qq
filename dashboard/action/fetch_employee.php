<?php
include('connect.php');
$output='';
$sql='SELECT tbl_employee.id, tbl_employee.first_name,tbl_employee.last_name,tbl_employee.sex,tbl_employee.phone,tbl_employee.positions,tbl_employee.salary,tbl_employee.photo, tbl_positions.id, tbl_positions.positions FROM tbl_employee INNER JOIN tbl_positions ON tbl_employee.positions=tbl_positions.id';
//$sql='SELECT * FROM tbl_employee ORDER BY id DESC';
$result=$conn->query($sql);
$output.='
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-icon" data-background-color="purple">
					<i class="material-icons">assignment</i>
				</div>
				<div class="card-content">
					<h4 class="card-title">Employee</h4>
					<div class="toolbar">
						<button class="btn btn-info" id="add-employee">
							<span class="btn-label">
							<i class="material-icons">shopping_cart</i>
							</span>
						Add Employee
						</button> 
					</div>
					<div class="responsive material-datatables">
						<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
							<thead>
								<tr>
									<th width="5%">ID</th>
									
									<th width="10%">First Name</th>
									<th width="10%">Last Name</th>
									<th width="10%">Sex</th>
									<th width="10%">Phone</th>
									<th width="10%">Position</th>
									<th width="10%">Salary</th>
									<th width="10%">Photo</th>
									<th width="10%" class="disabled-sorting text-right">Actions</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>ID</th>
									
									<th>First Name</th>
									<th>Last Name</th>
									<th>Sex</th>
									<th>Phone</th>
									<th>Position</th>
									<th>Salary</th>
									<th>Photo</th>
									<th class="text-right">Actions</th>
								</tr>
							</tfoot>
							<tbody>
								
';
while($row=$result->fetch_array()){
	$output .= '
	
		<tr>
			<td>'.$row[0].'</td>
			<td>'.$row['first_name'].'</td>
			<td>'.$row['last_name'].'</td>
			<td>'.$row['sex'].'</td>
			<td>'.$row['phone'].'</td>
			<td>'.$row['positions'].'</td>
			<td>'.$row['salary']." $".'</td>
			<td align="center"><img src="img/'.$row['photo'].'" class="img-rounded" style="height: 100px; width: auto;"></td>
			<td class="text-right">
			<a href="#" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>
			<a href="#" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons">close</i></a>
			</td>
		</tr>
							
	';
}
$output.='
</tbody>
						</table>
					</div>
				</div>
                           <!-- end content-->
			</div>
                       <!--  end card  -->
		</div>
                        <!-- end col-md-12 -->
	</div>
                    <!-- end row -->
</div>

<script src="assets/js/datatable.js"></script>
';

echo $output;
?>
