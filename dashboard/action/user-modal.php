<?php
include('connect.php');
$output='';
$sql="SELECT * FROM tbl_employee WHERE status='1'";
$user_create="SELECT * FROM tbl_user";
$role="SELECT * FROM role WHERE level > 1 ORDER BY level";
$result=$conn->query($sql);
$result_user=$conn->query($user_create);
$result_role=$conn->query($role);
$output='
<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="addUserModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
	
		<div class="modal-content">
	
			<div class="modal-header">
				<h5 class="modal-title" id="addUserModal">Add Food</h5>
     			
			</div>
			<div class="modal-body">
				<form method="post" id="frm_user" enctype="multipart/form-data">
					<input type="hidden" name="add_edit_prod" id="add_edit_prod" value="0">
					<input type="hidden" name="txt_id" id="txt_id">
					<div class="form-group">
						<label for="employee">Employee</label>
						<select class="form-control selectpicker" data-style="btn btn-link" id="employee" name="employee">
						<option value="">--Select One--</option>
';
while($row=$result->fetch_array()){
	$output.= '

							
							<option value="'.$row[0].'">'.$row[1].$row[2].'</option>
						
';
}
$output.='
						</select>
					</div>
					<div class="form-group label-floating">
					<label class="control-label">Username</label>
					<input type="text" class="form-control" name="txt_username" id="txt_username">
					</div><br>
					<div class="form-group label-floating">
						<label class="control-label">Password</label>
						<input type="password" class="form-control" name="txt_password" id="txt_password">
                    </div>
                    <div class="form-group label-floating">
						<label class="control-label">Confirm Password</label>
						<input type="password" class="form-control" name="txt_con_password" id="txt_con_password">
					</div>
					<div class="form-group">
						<label for="user_create">User Create</label>
						<select class="form-control selectpicker" data-style="btn btn-link" id="user_create" name="user_create">
';
while($row=$result_user->fetch_array()){
	$output.= '

							
							<option value="'.$row[0].'">'.$row[5].'</option>
						
';
}
$output.='
						</select>
					</div>
					<div class="form-group">
						<label for="role">Role</label>
						<select class="form-control selectpicker" data-style="btn btn-link" id="role" name="role">
';
while($row=$result_role->fetch_array()){
	$output.= '

							
							<option value="'.$row[0].'">'.$row[1].'</option>
						
';
}
$output.='
						</select>
					</div>
					<div class="form-group">
						<label for="status1">Status</label>
						<select class="form-control selectpicker" data-style="btn btn-link" id="status1" name="txt_status">
							<option value="1">Active</option>
							<option value="2">Inactive</option>
						</select>
					</div>
					<br>
					<input type="submit" class="btn btn-primary" style="float: right;" name="submit" id="save-user" value="Add">
				</form>
			</div>
			<div class="modal-footer">
				

			</div>
	
		</div>
	
	</div>
</div>
<script>
$(document).ready(function(){
$(".selectpicker").selectpicker("refresh");
});
//Save Product
		$("#frm_user").on("submit",function(e){
			e.preventDefault();
            var frm_data=new FormData(this.closest("form"));
            var password=$("#txt_password").val();
            var con_password=$("#txt_con_password").val();
            if(password==con_password && password!=""){
                save_user(frm_data);
            }
			else{
                alert("Please Check Password Again!");
            }
	
		});
</script>
';
echo $output;
?>
