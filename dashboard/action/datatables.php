﻿<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-icon" data-background-color="purple">
					<i class="material-icons">assignment</i>
				</div>
				<div class="card-content">
					<h4 class="card-title">DataTables.net</h4>
					<div class="toolbar">
						<button class="btn btn-info" id="add-category">
							<span class="btn-label">
							<i class="material-icons">shopping_cart</i>
							</span>
						Add Category
						</button> 
					</div>
					<div class="responsive material-datatables">
						<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
							<thead>
								<tr>
									<th width="60%">CatagoryName</th>
									<th width="10%">Image</th>
									<th width="10%">Status</th>
									<th width="20%" class="disabled-sorting text-right">Actions</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>CatagoryName</th>
									<th>Image</th>
									<th>Status</th>
									<th class="text-right">Actions</th>
								</tr>
							</tfoot>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
                           <!-- end content-->
			</div>
                       <!--  end card  -->
		</div>
                        <!-- end col-md-12 -->
	</div>
                    <!-- end row -->
</div>

<script>
	$(document).ready(function() {
	
        $("#datatables").DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });
		
		var table = $("#datatables").DataTable();
        // Edit record
        
        

        $(".card .material-datatables label").addClass("form-group");
	});
</script>


