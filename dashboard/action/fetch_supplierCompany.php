<?php
include('connect.php');
$output='';
$sql='SELECT company.id,company.name,company.province_id,company.district_id,company.address,company.contact_phone,company.email,company.img,tbl_province.id,tbl_province.province,tbl_district.id,tbl_district.district FROM company INNER JOIN tbl_province ON company.province_id=tbl_province.id INNER JOIN tbl_district ON company.district_id=tbl_district.id';
//$sql='SELECT * FROM tbl_customer ORDER BY id DESC';
$result=$conn->query($sql);
$output.='
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-icon" data-background-color="purple">
					<i class="material-icons">assignment</i>
				</div>
				<div class="card-content">
					<h4 class="card-title">Company</h4>
					<div class="toolbar">
						<button class="btn btn-info" id="add-company">
							<span class="btn-label">
							<i class="material-icons">shopping_cart</i>
							</span>
						Add Company
						</button> 
					</div>
					<div class="responsive material-datatables">
						<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
							<thead>
								<tr>
									<th width="5%">ID</th>
									<th width="10%">Company</th>
									<th width="10%">Province</th>
									<th width="10%">District</th>
									<th width="10%">Address</th>
									<th width="15%">Phone</th>
									<th width="15%">Email</th>
									<th width="10%">Photo</th>
									<th width="10%" class="disabled-sorting text-right">Actions</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>ID</th>
									<th>Company Name</th>
									<th>Province</th>
									<th>District</th>
									<th>Address</th>
									<th>Phone</th>
									<th>Email</th>
									<th>Photo</th>
									<th class="text-right">Actions</th>
								</tr>
							</tfoot>
							<tbody>
								
';
while($row=$result->fetch_array()){
	$output .= '
	
		<tr>
			<td>'.$row[0].'</td>
			<td>'.$row[1].'</td>
			<td>'.$row['province'].'<input type="hidden" name="t_province" value='.$row[2].'></td>
			<td>'.$row['district'].'<input type="hidden" name="t_district" value='.$row[3].'></td>
			<td>'.$row[4].'</td>
			<td>'.$row[5].'</td>
			<td>'.$row[6].'</td>
			<td align="center"><img src="img/'.$row[7].'" class="img-rounded" style="height: 100px; width: auto;"></td>
			<td class="text-right">
			<a href="#" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>
			<a href="#" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons">close</i></a>
			</td>
		</tr>
							
	';
}
$output.='
</tbody>
						</table>
					</div>
				</div>
                           <!-- end content-->
			</div>
                       <!--  end card  -->
		</div>
                        <!-- end col-md-12 -->
	</div>
                    <!-- end row -->
</div>

<script src="assets/js/datatable.js"></script>
';

echo $output;
?>
