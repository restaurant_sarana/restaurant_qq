<?php
$servername = "localhost:3308";
$username = "root";
$password = "";
$dbname = "restaurants_db";

// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);
$connect = new PDO("mysql:host=$servername;dbname=$dbname", "$username", "$password");
$connect->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
header('Content-type: text/html; charset=utf-8');
// Change character set to utf8
mysqli_set_charset($conn,"utf8");

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>