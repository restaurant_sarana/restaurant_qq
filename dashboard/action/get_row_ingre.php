<?php
include('connect.php');
$tr=$_POST['tr'];
$output='';
$sql="SELECT * FROM ingredients WHERE status='1'";
$result=$conn->query($sql);
$output='
<tr id="'.$tr.'">
	<td>
									<select class="form-control selectpicker" data-style="btn btn-link" id="ingredients" name="ingredients[]">
									<option value="">--Select One--</option>
										';
										while($row=$result->fetch_array()){
										$output.= '
										<option value="'.$row[0].'">'.$row[1].'</option>
										';
										}
										$output.='
									</select>
								</td>
								<td>
									<div class="form-group">
										<input type="text"  name="txt_cost[]" id="ingredientsCost" class="form-control txt_cost" readonly>
									</div>
								</td>
								<td width="5%">
									<div class="form-group">
										<input type="text"  name="txt_qty[]" class="form-control txt_qty" value="1">
									</div>
								</td>
								<td width="15%">
								<div class="btn-group">
									<a class="btn btn-round btn-info btn-xs remove-amount"> <i class="material-icons">remove</i> </a>
									<a class="btn btn-round btn-info btn-xs add-amount"> <i class="material-icons">add</i> </a>
								</div>
								</td>
								<td>
									<div class="form-group">
										<input type="text"  name="txt_total[]" class="form-control txt_total" readonly>
									</div>
								</td>
	<td><a href="#" class="btn btn-danger btn-xs btn-simple delete"><i class="material-icons">delete</i></a></td>
</tr>
';
echo $output;
?>