<?php
?>
<DOCTYPE
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Report</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
<!--    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />-->
    <!--  Material Dashboard CSS    -->
    <link href="assets/css/material-dashboard.css" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!--     Fonts and icons     -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/google-roboto-300-700.css" rel="stylesheet" />
</head>
<body>
    <div class="container-fluid" style="padding: 0px;">

                <nav class="navbar navbar-dark bg-primary">
                    <form class="form-inline">
                        <div class="btn-group" role="group" aria-label="Basic Report">
                            <button class="btn btn-success" data-report="salesreport" id="sales-report" type="button">Sale Report</button>
                            <button class="btn btn-secondary" data-report="orderreport" id="order-report" type="button">Order Report</button>
                            <button class="btn btn-success" data-report="expensereport" id="expense-report" type="button">Expense Report</button>
                            <button class="btn btn-secondary" data-report="stockreport" id="stock-report" type="button">Stock Report</button>
                        </div>
                    </form>
                </nav>

    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Restuarant Management Report</h2>
                <br>
                <h4><i><u id="Show-Report"></u></i></h4>
            </div>
            <div class="col-md-4">
                <input type="button" class="btn btn-facebook" value="Export To Excel" id="export">
                <input type="button" class="justify-content-end btn btn-default" value="Print" id="print">
            </div>
        </div>

        <br><br><br>
    </div>
    <div class="container">
        <form method="post" name="report" >
            <label>From Date:</label>
            <input type="date" id="frdt" required>
            <label>To Date:</label>
            <input type="date" id="todt" required>
            <input type="submit" class="btn btn-primary" value="Seacher" name="submit" id="seacher">
            <div class="content">

            </div>

        </form>
    </div>
</body>
<script src="assets/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/material.min.js" type="text/javascript"></script>
<script src="assets/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="assets/dist/jquery.table2excel.js" type="text/javascript"></script>
<script src="../assets/js/printThis.js"></script>
<script>
$(document).ready(function () {
    var report="";
    var title="";
    var frdt="";
    var todt="";
    $('#print').click(function () {
        if((frdt!=""&&todt!="")&&report!="") {
            $('.content').printThis({
                header: "<h1>" + title + "</h1><br><h4>From Date: " + frdt + " To Date: " + todt + "<br>"
            });
        }
    });
    $('#export').click(function () {
        if((frdt!=""&&todt!="")&&report!="") {
            $('.content').table2excel({
                // exclude CSS class
                exclude: ".noExl",
                name: "Worksheet Name",
                filename: report, //do not include extension
                fileext: ".xls" // file extension

            });
        }

    });
    $('#seacher').click(function (e) {
        e.preventDefault();
        frdt=$('#frdt').val();
        todt=$('#todt').val();
        if((frdt!=""&&todt!="")&&report!=""){
            $.ajax({
                url: report+".php",
                method: "post",
                data: {
                    frdt: frdt,
                    todt: todt,
                    report:report
                },
                success: function (data) {
                    $(".content").html(data);
                }
            });
        }

    });
    $('#sales-report').click(function () {
        report=$(this).data('report');
        title=$(this).html();
        $('#Show-Report').html($(this).html());
    });
    $('#order-report').click(function () {
        report=$(this).data('report');
        title=$(this).html();
        $('#Show-Report').html($(this).html());
    });
    $('#expense-report').click(function () {
        report=$(this).data('report');
        title=$(this).html();
        $('#Show-Report').html($(this).html());
    });
    $('#stock-report').click(function () {
        report=$(this).data('report');
        title=$(this).html();
        $('#Show-Report').html($(this).html());
    });
})
</script>
</html>