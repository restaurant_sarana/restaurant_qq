<?php

include ("action/connect.php");

$frdt=date("Y/m/d");
$todt=date("Y/m/d");
$todt=date('Y/m/d',strtotime($todt . "+1 days"));
if(isset($_POST['frdt'])){
    $frdt = date('Y/m/d',strtotime($_POST['frdt'] . "-1 days"));
}
if(isset($_POST['todt'])){
//    $todt = date('Y/m/d',strtotime($_POST['todt'] . "+1 days"));
}

$query="select b.*,b.sold+b.curqty as bfqty from (select 'Drink' as type,soft_drink.name,
       case when a.qty is null then 0 else a.qty end as sold,
       soft_drink.qty as curqty from soft_drink left join (select order_food_details.items_id,sum(qty) qty from order_food_details right join order_food on order_food_details.defacno=order_food.defacno where order_food_details.type='D' and order_food.paid_date between '$frdt' and '$todt') as a
on soft_drink.id=a.items_id) b";
$result=$connect->prepare($query);
$result->execute();
//$row=$result->fetch();
$output='<table class="table">
  <thead>
    <tr>
      <th scope="col">Type</th>
      <th scope="col">Items</th>
      <th scope="col">Before Qty</th>
      <th scope="col">On hand Qty</th>

    </tr>
  </thead>
  <tbody>';
while ($row=$result->fetch()){

    $output.='<tr>
      <th scope="row">'.$row['type'].'</th>
      <td>'.$row['name'].'</td>
      <td>'.$row['bfqty'].'</td>
      <td>'.$row['curqty'].'</td>

    </tr>';
}
$output.='    
 
  </tbody>
</table>';

echo $output;