<?php
//fetch.php
include('action/connect.php');
$output = '';
$count='';
$sql='SELECT * FROM tbl_district';
$query = "SELECT tbl_province.province, tbl_province.id, tbl_district.province_id, tbl_district.district, tbl_district.district_eng FROM tbl_district INNER JOIN tbl_province ON tbl_district.province_id=tbl_province.id";
$result = mysqli_query($conn, $query);
$output = '
<br />
<h3 align="center">District Data</h3>
<table class="table table-bordered table-striped">
 <tr>
  <th width="35%">Province</th>
  <th width="30%">ស្រុក</th>
  <th width="30%">District</th>
  <th width="5%"></th>
 </tr>
';
while($row = mysqli_fetch_array($result))
{
$count=$count+1;
 $output .= '
 <tr>
 <td>'.$row["province"].'</td>
  <td>'.$row["district"].'</td>
  <td>'.$row["district_eng"].'</td>
  <td><button type="button" name="edit" data-row="row'.$count.'" class="btn btn-danger btn-xs remove">Edit</button></td>
 </tr>
 ';
}
$output .= '</table>';
echo $output;
?>
