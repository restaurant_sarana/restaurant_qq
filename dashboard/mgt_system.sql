-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 17, 2018 at 10:12 AM
-- Server version: 5.7.21
-- PHP Version: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mgt_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `stock_invoice`
--

DROP TABLE IF EXISTS `stock_invoice`;
CREATE TABLE IF NOT EXISTS `stock_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `total` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stock_invoice_details`
--

DROP TABLE IF EXISTS `stock_invoice_details`;
CREATE TABLE IF NOT EXISTS `stock_invoice_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_invoice_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

DROP TABLE IF EXISTS `tbl_category`;
CREATE TABLE IF NOT EXISTS `tbl_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` text COLLATE utf8_unicode_ci NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `category`, `img`, `status`) VALUES
(1, 'Phone', 'Phone-4595974-clouds-1535277065.jpg', 1),
(2, 'Touch Pad', 'Touch Pad-137c6fa0b45f860c6f35e63487240a7d-1535273034.png', 1),
(3, 'ážáž¶áž…áŸ‹', 'ážáž¶áž…áŸ‹-1-1536399687.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_commune`
--

DROP TABLE IF EXISTS `tbl_commune`;
CREATE TABLE IF NOT EXISTS `tbl_commune` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `district_id` int(11) NOT NULL,
  `commune` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

DROP TABLE IF EXISTS `tbl_customer`;
CREATE TABLE IF NOT EXISTS `tbl_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop` text COLLATE utf8_unicode_ci NOT NULL,
  `first_name` text COLLATE utf8_unicode_ci NOT NULL,
  `last_name` text COLLATE utf8_unicode_ci NOT NULL,
  `province_id` int(10) NOT NULL,
  `district_id` int(11) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `photo` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`id`, `shop`, `first_name`, `last_name`, `province_id`, `district_id`, `address`, `phone`, `photo`, `status`) VALUES
(1, 'ážŸáž»ážŽáž¶', 'áž¡áŸáž„', 'ážŸáž»ážŽáž¶', 1, 1, 'áž•áŸ’ážŸáž¶ážšážŸáŸ’áž‘áž¹áž„', '012223445', 'shop-28661337_1336039326542313_8852441772738945590_n-1537093508.jpg', 1),
(2, 'áž¢áž¼áž¡áž¶', 'áž¢áž¼', 'áž¡áž¶', 2, 12, 'áž”áž¶ážáŸ‹ážŠáŸ†áž”áž„', '011522335', 'shop-img002-1537097683.jpg', 1),
(3, 'áž˜áŸ‰áž¼áž›áž¸áž€áž¶', 'áž›áž€áŸ’áž', 'áž˜áŸ‰áž¼áž›áž¸áž€áž¶', 5, 45, '', '015663355', 'shop-2-1537097824.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_district`
--

DROP TABLE IF EXISTS `tbl_district`;
CREATE TABLE IF NOT EXISTS `tbl_district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(11) NOT NULL,
  `district` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `district_eng` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=197 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_district`
--

INSERT INTO `tbl_district` (`id`, `province_id`, `district`, `district_eng`) VALUES
(1, 1, 'ážŸáŸ’ážšáž»áž€áž˜áž„áŸ’áž‚áž›áž”áž¼ážšáž¸', 'Mongkol Borey'),
(2, 1, 'ážŸáŸ’ážšáž»áž€áž—áŸ’áž“áŸ†ážŸáŸ’ážšáž»áž€', 'Phnom Srok'),
(3, 1, 'ážŸáŸ’ážšáž»áž€áž–áŸ’ážšáŸ‡áž“áŸážáŸ’ážšáž–áŸ’ážšáŸ‡', 'Preah Netr Preah'),
(4, 1, 'ážŸáŸ’ážšáž»áž€áž¢áž¼ážšáž‡áŸ’ážšáŸ…', 'Ou Chrov'),
(5, 1, 'ážŸáŸ’ážšáž»áž€ážŸáž·ážšáž¸ážŸáŸ„áž—áŸážŽ', 'Serei Saophoan'),
(6, 1, 'ážŸáŸ’ážšáž»áž€ážáŸ’áž˜áž–áž½áž€', 'Thma Puok'),
(7, 1, 'ážŸáŸ’ážšáž»áž€ážŸáŸ’ážœáž¶áž™áž…áŸáž€', 'Svay Chek'),
(8, 1, 'ážŸáŸ’ážšáž»áž€áž˜áŸ‰áž¶áž¡áŸƒ', 'Mala '),
(9, 1, 'áž”áŸ‰áŸ„áž™áž”áŸ‰áŸ‚áž', 'Krong Paoy Paet'),
(10, 2, 'ážŸáŸ’ážšáž»áž€áž”áž¶ážŽáž“áŸ‹', 'Banan'),
(11, 2, 'ážŸáŸ’ážšáž»áž€ážáŸ’áž˜áž‚áŸ„áž›', 'Thmar Kaul'),
(12, 2, 'ážŸáŸ’ážšáž»áž€áž”áž¶ážáŸ‹ážŠáŸ†áž”áž„', 'Battambang'),
(13, 2, 'ážŸáŸ’ážšáž»áž€áž”ážœáŸáž›', 'Bavel'),
(14, 2, 'ážŸáŸ’ážšáž»áž€áž¯áž€áž—áŸ’áž“áŸ†', 'Ek Phnom'),
(15, 2, 'ážŸáŸ’ážšáž»áž€áž˜áŸ„áž„áž«ážŸáŸ’ážŸáž¸', 'Moung Ruessei'),
(16, 2, 'ážŸáŸ’ážšáž»áž€ážšážáž“áž˜ážŽáŸ’ážŒáž›', 'Rotanak Mondol'),
(17, 2, 'ážŸáŸ’ážšáž»áž€ážŸáž„áŸ’áž€áŸ‚', 'Sangkae'),
(18, 2, 'ážŸáŸ’ážšáž»áž€ážŸáŸ†áž¡áž¼áž', 'Samlout'),
(19, 2, 'ážŸáŸ†áž–áŸ…áž›áž¼áž“', 'Sampov Loun'),
(20, 2, 'áž—áŸ’áž“áŸ†áž–áŸ’ážšáž¹áž€', 'Phnum Proek'),
(21, 2, 'áž€áŸ†ážšáŸ€áž„', 'Kamrieng'),
(22, 2, 'áž‚áž¶ážŸáŸ‹áž€áŸ’ážšáž¡', 'Koas Krala'),
(23, 2, 'ážšáž»áž€áŸ’ážáž‚áž·ážšáž¸', 'Rukh Kiri'),
(24, 3, 'ážŸáŸ’ážšáž»áž€áž”áž¶áž’áž¶áž™', 'Batheay'),
(25, 3, 'ážŸáŸ’ážšáž»áž€áž…áŸ†áž€áž¶ážšáž›áž¾', 'Chamkar Leu'),
(26, 3, 'ážŸáŸ’ážšáž»áž€áž‡áž¾áž„áž–áŸ’ážšáŸƒ', 'Cheung Prey'),
(27, 3, 'ážŸáŸ’ážšáž»áž€áž€áŸ†áž–áž„áŸ‹áž…áž¶áž˜', 'Kampong Cham'),
(28, 3, 'ážŸáŸ’ážšáž»áž€áž€áŸ†áž–áž„áŸ‹ážŸáŸ€áž˜', 'Kampong Siem'),
(29, 3, 'ážŸáŸ’ážšáž»áž€áž‚áž„áž˜áž¶ážŸ', 'Kang Meas'),
(30, 3, 'ážŸáŸ’ážšáž»áž€áž€áŸ„áŸ‡ážŸáž»áž‘áž·áž“', 'Koh Sotin'),
(31, 3, 'ážŸáŸ’ážšáž»áž€áž–áŸ’ážšáŸƒážˆážš', 'Prey Chhor'),
(32, 3, 'ážŸáŸ’ážšáž»áž€ážŸáŸ’ážšáž¸ážŸáž“áŸ’áž’ážš', 'Srey Santhor'),
(33, 3, 'ážŸáŸ’ážšáž»áž€ážŸáŸ’áž‘áž¹áž„ážáŸ’ážšáž„áŸ‹', 'Steung Trang'),
(34, 4, 'ážŸáŸ’ážšáž»áž€áž”ážšáž·áž”áž¼ážŽáŸŒ', 'Baribour'),
(35, 4, 'ážŸáŸ’ážšáž»áž€áž‡áž›áž‚áž¸ážšáž¸', 'Chol Kiri'),
(36, 4, 'ážŸáŸ’ážšáž»áž€áž€áŸ†áž–áž„áŸ‹áž†áŸ’áž“áž¶áŸ†áž„', 'Kampong Chhnang'),
(37, 4, 'ážŸáŸ’ážšáž»áž€áž€áŸ†áž–áž„áž›áŸ‚áž„', 'Kampong Leaeng'),
(38, 4, 'ážŸáŸ’ážšáž»áž€áž€áŸ†áž–áž„áŸ‹ážáŸ’ážšáž¡áž¶áž…', 'Kampong Tralach'),
(39, 4, 'ážŸáŸ’ážšáž»áž€ážšáž›áž¶áž”áŸ’áž¢áŸ€ážš', 'Rolea B\'ier'),
(40, 4, 'ážŸáŸ’ážšáž»áž€ážŸáž¶áž˜áž‚áŸ’áž‚áž¸áž˜áž¶áž“áž‡áŸáž™', 'Sameakki Mean Chey'),
(41, 4, 'ážŸáŸ’ážšáž»áž€áž‘áž¹áž€áž•áž»ážŸ', 'Tuek Phos'),
(42, 5, 'ážŸáŸ’ážšáž»áž€áž”ážšážŸáŸážŠáŸ’áž‹', 'Borsedth'),
(43, 5, 'áž€áŸ’ážšáž»áž„áž…áŸ’áž”áž¶ážšáž˜áž“', 'Chbar Mon'),
(44, 5, 'ážŸáŸ’ážšáž»áž€áž‚áž„áž–áž·ážŸáž¸', 'Kong Pisei'),
(45, 5, 'ážŸáŸ’ážšáž»áž€ážŸáŸ’ážšáž»áž€áž±ážšáŸ‰áž¶áž›áŸ‹', 'Aoral'),
(46, 5, 'áž§ážŠáž»áž„áŸ’áž‚', 'Oudong'),
(47, 5, 'ážŸáŸ’ážšáž»áž€áž—áŸ’áž“áŸ†ážŸáŸ’ážšáž½áž…', 'Phnom Sruoch'),
(48, 5, 'ážŸáŸ’ážšáž»áž€ážŸáŸ†ážšáŸ„áž„áž‘áž„', 'Samraong Tong'),
(49, 5, 'ážŸáŸ’ážšáž»áž€ážáŸ’áž–áž„', 'Thpong'),
(50, 6, 'ážŸáŸ’ážšáž»áž€áž”áž¶ážšáž¶áž™ážŽáŸ', 'Baray'),
(51, 6, 'ážŸáŸ’ážšáž»áž€áž€áŸ†áž–áž„áŸ‹ážŸáŸ’ážœáž¶áž™', 'Kampong Svay'),
(52, 6, 'áž€áŸ’ážšáž»áž„ážŸáŸ’áž‘áž¹áž„ážŸáŸ‚áž“', 'Stueng Saen'),
(53, 6, 'ážŸáŸ’ážšáž»áž€áž”áŸ’ážšáž¶ážŸáž¶áž‘áž”áž›áŸ’áž›áŸáž„áŸ’áž€', 'Prasat Balangk'),
(54, 6, 'ážŸáŸ’ážšáž»áž€áž”áŸ’ážšáž¶ážŸáž¶áž‘ážŸáž˜áŸ’áž”áž¼ážš', 'Prasat Sambour'),
(55, 6, 'ážŸáŸ’ážšáž»áž€ážŸážŽáŸ’ážŠáž¶áž“áŸ‹', 'Sandaan'),
(56, 6, 'ážŸáŸ’ážšáž»áž€ážŸáž“áŸ’áž‘áž»áž€', 'Santuk'),
(57, 6, 'ážŸáŸ’ážšáž»áž€ážŸáŸ’áž‘áŸ„áž„', 'Stoung'),
(58, 7, 'ážŸáŸ’ážšáž»áž€áž¢áž„áŸ’áž‚ážšáž‡áŸáž™', 'Angkor Chey'),
(59, 7, 'ážŸáŸ’ážšáž»áž€áž”áž“áŸ’áž‘áž¶áž™áž˜áž¶ážŸ', 'Banteay Meas'),
(60, 7, 'ážŸáŸ’ážšáž»áž€ážˆáž¼áž€', 'Chhouk'),
(61, 7, 'ážŸáŸ’ážšáž»áž€áž‡áž»áŸ†áž‚áž¸ážšáž¸', 'Chum Kiri'),
(62, 7, 'ážŸáŸ’ážšáž»áž€ážŠáž„áž‘áž„', 'Dang Tong'),
(63, 7, 'ážŸáŸ’ážšáž»áž€áž€áŸ†áž–áž„áŸ‹ážáŸ’ážšáž¶áž…', 'Kampong Trach'),
(64, 7, 'ážŸáŸ’ážšáž»áž€áž‘áž¹áž€ážˆáž¼', 'Tuek Chhou'),
(65, 7, 'áž€áŸ’ážšáž»áž„áž€áŸ†áž–áž', 'Krong Kampot'),
(66, 8, 'áž€ážŽáŸ’ážáž¶áž›ážŸáŸ’áž‘áž¹áž„', 'Kandal Steung'),
(67, 8, 'ážŸáŸ’ážšáž»áž€áž‚áŸ€áž“ážŸáŸ’ážœáž¶áž™', 'Kien Svay'),
(68, 8, 'ážŸáŸ’ážšáž»áž€ážáŸ’ážŸáž¶áž…áŸ‹áž€ážŽáŸ’ážáž¶áž›', 'Khsach Kandal'),
(69, 8, 'ážŸáŸ’ážšáž»áž€áž€áŸ„áŸ‡áž’áŸ†', 'Kaoh Thum'),
(70, 8, 'ážŸáŸ’ážšáž»áž€áž›áž¾áž€ážŠáŸ‚áž€', 'Leuk Daek'),
(71, 8, 'ážŸáŸ’ážšáž»áž€áž›áŸ’ážœáž¶áž¯áž˜', 'Lvea Aem'),
(72, 8, 'ážŸáŸ’ážšáž»áž€áž˜áž»ážáž€áŸ†áž–áž¼áž›', 'Mukh Kampul'),
(73, 8, 'ážŸáŸ’ážšáž»áž€áž¢áž„áŸ’áž‚ážŸáŸ’áž“áž½áž›', 'Angk Snuol'),
(74, 8, 'ážŸáŸ’ážšáž»áž€áž–áž‰áŸ’áž‰áž¶áž®', 'Ponhea Lueu'),
(75, 8, 'ážŸáŸ’ážšáž»áž€ážŸáŸ’áž¢áž¶áž„', 'S\'ang'),
(76, 8, 'áž€áŸ’ážšáž»áž„ážáž¶ážáŸ’áž˜áŸ…', 'Krong Ta Khmau'),
(77, 9, 'ážŸáŸ’ážšáž»áž€áž”áž¼áž‘áž»áž˜ážŸáž¶áž‚ážš', 'Botum Sakor'),
(78, 9, 'ážŸáŸ’ážšáž»áž€áž‚áž¸ážšáž¸ážŸáž¶áž‚ážš', 'Kiri Sakor'),
(79, 9, 'áž€áŸ’ážšáž»áž„ážáŸáž˜ážšáŸˆáž—áž¼áž˜áž·áž˜áž·áž“', 'Khemara Phoumin'),
(80, 9, 'ážŸáŸ’ážšáž»áž€ážŸáŸ’áž˜áž¶áž…áŸ‹áž˜áž¶áž“áž‡áŸáž™', 'Smach Mean Chey'),
(81, 9, 'ážŸáŸ’ážšáž»áž€áž˜ážŽáŸ’ážŒáž›ážŸáž¸áž˜áŸ‰áž¶', 'Mondol Seima'),
(82, 9, 'ážŸáŸ’ážšáž»áž€ážŸáŸ’ážšáŸ‚áž¢áŸ†áž”áž·áž›', 'Srae Ambel'),
(83, 9, 'ážŸáŸ’ážšáž»áž€ážáŸ’áž˜áž”áž¶áŸ†áž„', 'Thma Bang'),
(84, 10, 'ážŸáŸ’ážšáž»áž€áž†áŸ’áž›áž¼áž„', 'Chhloung'),
(85, 10, 'áž€áŸ’ážšáž»áž„áž€áŸ’ážšáž…áŸáŸ‡', 'Krong Kracheh'),
(86, 10, 'áž–áŸ’ážšáŸ‚áž€áž”áŸ’ážšážŸáž–áŸ’ážœ', 'Preaek Prasab'),
(87, 10, 'ážŸáŸ†áž”áž¼ážš', 'Sambour'),
(88, 10, 'ážŸáŸ’áž“áž½áž›', 'Snuol '),
(89, 10, 'ážˆáž¹ážáž”áž»ážšáž¸', 'Chitr Borie'),
(90, 11, 'áž€áŸ‚ážœážŸáž¸áž˜áŸ‰áž¶', 'Kaev Seima'),
(91, 11, 'áž€áŸ„áŸ‡áž‰áŸ‚áž€', 'Kaoh Nheak'),
(92, 11, 'áž¢áž¼ážšážšáž¶áŸ†áž„', 'Ou Reang'),
(93, 11, 'áž–áŸáž‡áŸ’ážšáž…áž·áž“áŸ’ážŠáž¶', 'Pechr Chenda'),
(94, 11, 'ážŸáŸ‚áž“áž˜áž“áŸ„ážšáž˜áŸ’áž™', 'Senmonorom'),
(95, 12, 'áž¢áž“áŸ’áž›áž„áŸ‹ážœáŸ‚áž„', 'Anlong Veng'),
(96, 12, 'áž”áž“áŸ’áž‘áž¶áž™áž¢áŸ†áž–áž·áž›', 'Banteay Ampil'),
(97, 12, 'áž…áž»áž„áž€áž¶áž›', 'Chong Kal'),
(98, 12, 'ážŸáŸ†ážšáŸ„áž„', 'Samraong'),
(99, 12, 'ážáŸ’ážšáž–áž¶áŸ†áž„áž”áŸ’ážšáž¶ážŸáž¶áž‘', 'Trapeang Prasat'),
(100, 13, 'áž‡áŸáž™ážŸáŸ‚áž“', 'Chey Saen'),
(101, 13, 'áž†áŸ‚áž”', 'Chhaeb'),
(102, 13, 'áž‡áž¶áŸ†áž€áŸ’ážŸáž¶áž“áŸ’áž', 'Choam Khsant'),
(103, 13, 'áž‚áž¼áž›áŸ‚áž“', 'Kuleaen'),
(104, 13, 'ážšážœáŸ€áž„', 'Rovieng'),
(105, 13, 'ážŸáž„áŸ’áž‚áž˜ážáŸ’áž˜áž¸', 'Sangkum Thmei'),
(106, 13, 'ážáŸ’áž”áŸ‚áž„áž˜áž¶áž“áž‡áŸáž™', 'Tbaeng Meanchey'),
(107, 14, 'ážŸáŸ’ážšáž»áž€áž”áž¶áž€áž¶áž“', 'Bakan'),
(108, 14, 'ážŸáŸ’ážšáž»áž€áž€ážŽáŸ’ážŠáŸ€áž„', 'Kandieng'),
(109, 14, 'ážŸáŸ’ážšáž»áž€áž€áŸ’ážšáž‚ážš', 'Krakor'),
(110, 14, 'áž‡áž½ážšáž—áŸ’áž“áŸ†áž€áŸ’ážšážœáž¶áž‰', 'Phnum Kravanh'),
(111, 14, 'áž€áŸ’ážšáž»áž„áž–áŸ„áž’áž·áŸážŸáž¶ážáŸ‹', 'Krong Pursat'),
(112, 14, 'ážŸáŸ’ážšáž»áž€ážœáž¶áž›ážœáŸ‚áž„', 'Veal Veang'),
(113, 15, 'ážŸáŸ’ážšáž»áž€áž”áž¶áž—áŸ’áž“áŸ†', 'Ba Phnom'),
(114, 15, 'ážŸáŸ’ážšáž»áž€áž€áŸ†áž…áž¶áž™áž˜áž¶ážš', 'Kamchay Mear'),
(115, 15, 'ážŸáŸ’ážšáž»áž€áž€áŸ†áž–áž„áŸ‹ážáŸ’ážšáž”áŸ‚áž€', 'Kampong Trabaek'),
(116, 15, 'ážŸáŸ’ážšáž»áž€áž€áž‰áŸ’áž…áŸ’ážšáŸ€áž…', 'Kanhchriech'),
(117, 15, 'ážŸáŸ’ážšáž»áž€áž˜áŸážŸáž¶áž„', 'Me Sang'),
(118, 15, 'ážŸáŸ’ážšáž»áž€áž–áž¶áž˜áž‡ážš', 'Peam Chor'),
(119, 15, 'ážŸáŸ’ážšáž»áž€áž–áž¶áž˜ážšáž€áŸ', 'Peam Ro'),
(120, 15, 'ážŸáŸ’ážšáž»áž€áž–áž¶ážšáž¶áŸ†áž„', 'Pea Reang'),
(121, 15, 'ážŸáŸ’ážšáž»áž€áž–áŸ’ážšáŸ‡ážŸáŸ’ážŠáŸáž…', 'Preah Sdach'),
(122, 15, 'ážŸáŸ’ážšáž»áž€áž–áŸ’ážšáŸƒážœáŸ‚áž„', 'Prey Veaeng'),
(123, 15, 'ážŸáŸ’ážšáž»áž€áž€áŸ†áž–áž„áŸ‹áž›áž¶ážœ', 'Kampong Leav'),
(124, 15, 'ážŸáŸ’ážšáž»áž€ážŸáŸŠáž¸áž’ážšáž€ážŽáŸ’ážŠáž¶áž›', 'Sithor Kandal'),
(125, 15, 'ážŸáŸ’ážšáž»áž€áž–áž¶ážšáž¶áŸ†áž„', 'Pea Reang'),
(126, 16, 'áž¢ážŽáŸ’ážáž¼áž„áž˜áž¶ážŸ', 'Andoung Meas'),
(127, 16, 'ážŸáŸ’ážšáž»áž€áž”áž¶áž“áž›áž»áž„', 'Banlun '),
(128, 16, 'áž”áž¶áž‚áž¶ážœ', 'Bar Kaev'),
(129, 16, 'ážŸáŸ’ážšáž»áž€áž€áž¼áž“áž˜áž»áŸ†', 'Koun Mom'),
(130, 16, 'áž›áŸ†áž•áž¶ážáŸ‹', 'Lumphat'),
(131, 16, 'áž¢áž¼ážšáž‡áž»áŸ†', 'Ou Chum'),
(132, 16, 'áž¢áž¼ážšáž™áŸ‰áž¶ážŠáž¶ážœ', 'Ou Ya Dav'),
(133, 16, 'ážáž¶ážœáŸ‚áž„', 'Ta Veaeng'),
(134, 16, 'ážœáž¾áž“ážŸáŸƒ', 'Veun Sai'),
(135, 17, 'ážŸáŸ’ážšáž»áž€áž¢áž„áŸ’áž‚ážšáž‡áž»áŸ†', 'Angkor Chum'),
(136, 17, 'ážŸáŸ’ážšáž»áž€áž¢áž„áŸ’áž‚ážšáž’áŸ†', 'Angkor Thom'),
(137, 17, 'ážŸáŸ’ážšáž»áž€áž¢áž„áŸ’áž‚ážšáž’áŸ†', 'Banteay Srei'),
(138, 17, 'áž‡áž¸áž€áŸ’ážšáŸ‚áž„', 'Chi Kraeng'),
(139, 17, 'áž€áŸ’ážšáž›áž¶áž‰áŸ‹', 'Kralanh'),
(140, 17, 'áž–áž½áž€', 'Puok'),
(141, 17, 'ážŸáŸ’ážšáž»áž€áž”áŸ’ážšáž¶ážŸáž¶áž‘áž”áž¶áž‚áž„', 'Prasat Bakong'),
(142, 17, 'ážŸáŸ’ážšáž»áž€ážŸáŸ€áž˜ážšáž¶áž”', 'Siem Reap'),
(143, 17, 'ážŸáŸ’ážšáž»áž€ážŸáž¼áž‘áŸ’ážšáž“áž·áž‚áž˜', 'Sout Nikom'),
(144, 17, 'ážŸáŸ’ážšáž»áž€ážŸáŸ’ážšáž¸ážŸáŸ’áž“áŸ†', 'Srei Snam'),
(145, 17, 'ážŸáŸ’ážšáž»áž€ážŸáŸ’ážœáž¶áž™áž›áž¾', 'Svay Leu'),
(146, 17, 'ážŸáŸ’ážšáž»áž€ážœáŸ‰áž¶ážšáž·áž“', 'Varin'),
(147, 18, 'ážŸáŸážŸáž¶áž“', 'Sesan'),
(148, 18, 'ážŸáŸ€áž˜áž”áž¼áž€', 'Siem Bouk'),
(149, 18, 'ážŸáŸ€áž˜áž”áŸ‰áž¶áž„', 'Siem Pang'),
(150, 18, 'ážŸáŸ’áž‘áž¹áž„ážáŸ’ážšáŸ‚áž„', 'Stung Treng'),
(151, 18, 'ážáž¶áž¡áž¶áž”áž¶ážšáž¸ážœáŸ‰áž¶ážáŸ‹', 'Thala Barivat'),
(152, 19, 'áž…áž“áŸ’ážšáŸ’áž‘áž¶', 'Chanthrea'),
(153, 19, 'áž€áŸ†áž–áž„áŸ‹ážšáŸ„áž‘áŸ', 'Kampong Rou'),
(154, 19, 'ážšáŸ†ážŠáž½áž›', 'rumduol '),
(155, 19, 'ážšáž˜áž¶ážŸáž áŸ‚áž€', 'Romeas Haek'),
(156, 19, 'ážŸáŸ’ážœáž¶áž™áž‡áŸ’ážšáž»áŸ†', 'Svay Chrom'),
(157, 19, 'ážŸáŸ’ážœáž¶áž™ážšáŸ€áž„', 'Svay Rieng'),
(158, 19, 'ážŸáŸ’ážœáž¶áž™áž‘áŸ€áž”', 'Svay Theab'),
(159, 19, 'áž€áŸ’ážšáž»áž„áž”áž¶ážœáž·áž', 'Bavet'),
(160, 20, 'ážŸáŸ’ážšáž»áž€áž¢áž„áŸ’áž‚ážšáž”áž»ážšáž¸', 'Angkor Borei'),
(161, 20, 'ážŸáŸ’ážšáž»áž€áž”áž¶áž‘áž¸', 'Bathi'),
(162, 20, 'ážŸáŸ’ážšáž»áž€áž”áž¼ážšáž¸áž‡áž›ážŸáž¶ážš', 'Bourei Cholsar'),
(163, 20, 'áž‚áž¸ážšáž¸ážœáž„áŸ’ážŸ', 'Kiri Vong'),
(164, 20, 'ážŸáŸ’ážšáž»áž€áž€áŸ„áŸ‡áž¢ážŽáŸ’ážáŸ‚áž', 'Kaoh Andaet'),
(165, 20, 'áž–áŸ’ážšáŸƒáž€áž”áŸ’áž”áž¶ážŸ', 'Prey Kabbas'),
(166, 20, 'ážŸáŸ’ážšáž»áž€ážŸáŸ†ážšáŸ„áž„', 'SamraÅng'),
(167, 20, 'ážŸáŸ’ážšáž»áž€ážŠáž¼áž“áž€áŸ‚ážœ', 'Doun Kaev'),
(168, 20, 'ážáŸ’ážšáž¶áŸ†áž€áž€áŸ‹', 'Tram Kak'),
(169, 20, 'ážŸáŸ’ážšáž»áž€áž‘áŸ’ážšáž¶áŸ†áž„', 'Treang'),
(170, 21, 'ážŸáŸ’ážšáž»áž€ážŠáŸ†ážŽáž¶áž€áŸ‹áž…áž„áŸ’áž¢áž¾ážš', 'Damnak Chang\'aeur'),
(171, 21, 'áž€áŸ’ážšáž»áž„áž€áŸ‚áž”', 'Kep'),
(172, 22, 'áž”áŸ‰áŸƒáž›áž·áž“', 'Pailin '),
(173, 22, 'ážŸáž¶áž›áž¶áž€áŸ’ážšáŸ…', 'Sala Krau'),
(174, 23, 'ážážŽáŸ’ážŒáž…áŸ†áž€áž¶ážšáž˜áž“', 'Chamkar Mon'),
(175, 23, 'ážážŽáŸ’ážŒážŠáž¼áž“áž–áŸáž‰', 'Daun Penh'),
(176, 23, 'ážážŽáŸ’ážŒáŸ§áž˜áž€ážšáž¶', 'Prampir Meakkakra'),
(177, 23, 'áž‘áž½áž›áž‚áŸ„áž€', 'Tuol Kouk'),
(178, 23, 'ážážŽáŸ’ážŒážŠáž„áŸ’áž€áŸ„', 'Dangkao '),
(179, 23, 'ážážŽáŸ’ážŒáž˜áž¶áž“áž‡áŸáž™', 'Mean Chey'),
(180, 23, 'ážážŽáŸ’ážŒáž¬ážŸáŸ’ážŸáž¸áž€áŸ‚ážœ', 'Russey Keo'),
(181, 23, 'ážážŽáŸ’ážŒážŸáŸ‚áž“ážŸáž»áž', 'Sen Sok'),
(182, 23, 'ážážŽáŸ’ážŒáž–áŸ„áž’áž·áŸážŸáŸ‚áž“áž‡áŸáž™', 'Pou Senchey'),
(183, 23, 'ážážŽáŸ’ážŒáž‡áŸ’ážšáŸ„áž™áž…áž„áŸ’ážœáž¶ážš', 'Chroy Changvar'),
(184, 23, 'ážážŽáŸ’ážŒáž–áŸ’ážšáŸ‚áž€áž–áŸ’áž“áŸ…', 'Preaek Pnov'),
(185, 23, 'ážážŽáŸ’ážŒáž…áŸ’áž”áž¶ážšáž¢áŸ†áž–áŸ…', 'Chbar Ampov'),
(186, 24, 'áž€áŸ’ážšáž»áž„áž–áŸ’ážšáŸ‡ážŸáž¸áž áž“áž»', 'Krong Preah Sihanouk'),
(187, 24, 'áž–áŸ’ážšáŸƒáž“áž”áŸ‹', 'Prey Nob'),
(188, 24, 'ážŸáŸ’áž‘áž¹áž„áž áž¶ážœ', 'Stueng Hav'),
(189, 24, 'ážŸáŸ’ážšáž»áž€áž€áŸ†áž–áž„áŸ‹ážŸáž¸áž›áž¶', 'Kampong Seila'),
(190, 25, 'ážŸáŸ’ážšáž»áž€ážŠáŸ†áž”áŸ‚', 'Dambae'),
(191, 25, 'ážŸáŸ’ážšáž»áž€áž€áŸ’ážšáž¼áž…áž†áŸ’áž˜áž¶ážš', 'Krouch Chhmar'),
(192, 25, 'ážŸáŸ’ážšáž»áž€áž˜áŸáž˜ážáŸ‹', 'Memot'),
(193, 25, 'ážŸáŸ’ážšáž»áž€áž¢áž¼ážšáž¶áŸ†áž„ážª', 'Ou Reang Ov'),
(194, 25, 'ážŸáŸ’ážšáž»áž€áž–áž‰áŸ’áž‰áž¶áž€áŸ’ážšáŸ‚áž€', 'Ponhea Kraek'),
(195, 25, 'ážŸáŸ’ážšáž»áž€ážáŸ’áž”áž¼áž„ážƒáŸ’áž˜áž»áŸ†', 'Tbuong Kmoum'),
(196, 25, 'áž€áŸ’ážšáž»áž„ážŸáž½áž„', 'Krong Suong');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee`
--

DROP TABLE IF EXISTS `tbl_employee`;
CREATE TABLE IF NOT EXISTS `tbl_employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` text COLLATE utf8_unicode_ci NOT NULL,
  `last_name` text COLLATE utf8_unicode_ci NOT NULL,
  `sex` int(2) NOT NULL,
  `date_of_birth` date NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `positions` int(11) NOT NULL,
  `salary` int(11) NOT NULL,
  `data_in` date NOT NULL,
  `car_assign` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_positions`
--

DROP TABLE IF EXISTS `tbl_positions`;
CREATE TABLE IF NOT EXISTS `tbl_positions` (
  `id` int(11) NOT NULL,
  `positions` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

DROP TABLE IF EXISTS `tbl_product`;
CREATE TABLE IF NOT EXISTS `tbl_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) NOT NULL,
  `product` text COLLATE utf8_unicode_ci NOT NULL,
  `cost` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `img` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `category_id`, `product`, `cost`, `price`, `img`, `status`) VALUES
(1, 1, 'Beat S2', 43, 55, '1-551103-1TOqFD1502285018-1535275405.jpg', 1),
(2, 1, 'Maga Pro', 43, 55, '1-19201_en_1-1535275479.jpg', 1),
(3, 1, 'Cool Max', 41, 55, '2-131918-games-news-buyer-s-guide-20-best-game-characters-that-defined-20-years-of-playstation-image1-6TDtn8U0Wf-1535275246.jpg', 2),
(4, 1, 'G4', 41, 55, '1-black-line-icon-logo-png-4-1535275486.png', 2),
(5, 2, 'Cool Pro', 55, 60, 'Cool Pro-come-to-fight-emoji-1535277572.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_province`
--

DROP TABLE IF EXISTS `tbl_province`;
CREATE TABLE IF NOT EXISTS `tbl_province` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `province_eng` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_province`
--

INSERT INTO `tbl_province` (`id`, `province`, `province_eng`) VALUES
(1, 'áž”áž“áŸ’áž‘áž¶áž™áž˜áž¶áž“áž‡áŸáž™', 'Banteay Meanchey'),
(2, 'áž”áž¶ážáŸ‹ážŠáŸ†áž”áž„', 'Battambong'),
(3, 'áž€áŸ†áž–áž„áŸ‹áž…áž¶áž˜', 'Kampong Cham'),
(4, 'áž€áŸ†áž–áž„áŸ‹áž†áŸ’áž“áž¶áŸ†áž„', 'Kampong Chhnang'),
(5, 'áž€áŸ†áž–áž„áŸ‹ážŸáŸ’áž–ážº', 'Kampong Speu'),
(6, 'áž€áŸ†áž–áž„áŸ‹áž’áŸ†', 'Kampong Thom'),
(7, 'áž€áŸ†áž–áž', 'Kampot'),
(8, 'áž€áŸ†ážŽáŸ’ážŠáž¶áž›', 'Kandal '),
(9, 'áž€áŸ„áŸ‡áž€áž»áž„', 'Koh Kong'),
(10, 'áž€áŸ’ážšáž…áŸáŸ‡', 'Kraches '),
(11, 'áž˜ážŽáŸ’ážŒáž›áž‚áž·ážšáž¸', 'Mondulkiri'),
(12, 'áž§ážáŸ’ážŠážšáž˜áž¶áž“áž‡áŸáž™', 'Oddar Meanchey'),
(13, 'áž–áŸ’ážšáŸ‡ážœáž·áž áž¶ážš', 'Preah Vihear'),
(14, 'áž–áŸ„áž’áž·áŸážŸáž¶ážáŸ‹', 'Pursat '),
(15, 'áž–áŸ’ážšáŸƒážœáŸ‚áž„', 'Prey Veng'),
(16, 'ážšážáž“áŸˆáž‚áž·ážšáž¸', 'Ratanakiri '),
(17, 'ážŸáŸ€áž˜ážšáž¶áž”', 'Siem Reap'),
(18, 'ážŸáŸ’áž‘áž¹áž„ážáŸ’ážšáŸ‚áž„', 'Steung Treng'),
(19, 'ážŸáŸ’ážœáž¶áž™ážšáŸ€áž„', 'Svay Rieng'),
(20, 'ážáž¶áž€áŸ‚ážœ', 'TakÃ©o '),
(21, 'áž€áŸ‚áž”', 'Kep '),
(22, 'áž”áŸ‰áŸƒáž›áž·áž“', 'Pailin '),
(23, 'áž—áŸ’áž“áŸ†áž–áŸáž‰', 'Phnom Penh'),
(24, 'áž–áŸ’ážšáŸ‡ážŸáž¸áž áž“áž»', 'Preah Sihanouk'),
(25, 'ážáŸ’áž”áž¼áž„ážƒáŸ’áž˜áž»áŸ†', 'Tboung Khmum');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `user_name` text COLLATE utf8_unicode_ci NOT NULL,
  `password` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_village`
--

DROP TABLE IF EXISTS `tbl_village`;
CREATE TABLE IF NOT EXISTS `tbl_village` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commune_id` int(11) NOT NULL,
  `village` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
