<!doctype html>
<html lang="en">


<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Mar 2017 21:29:18 GMT -->
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Material Dashboard PRO by Creative Tim | Premium Bootstrap Admin Template</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Canonical SEO -->
    <link rel="canonical" href="https://www.creative-tim.com/product/material-dashboard-pro" />
    <!--  Social tags      -->
    <meta name="keywords" content="material dashboard, bootstrap material admin, bootstrap material dashboard, material design admin, material design, creative tim, html dashboard, html css dashboard, web dashboard, freebie, free bootstrap dashboard, css3 dashboard, bootstrap admin, bootstrap dashboard, frontend, responsive bootstrap dashboard, premiu material design admin">
    <meta name="description" content="Material Dashboard PRO is a Premium Material Bootstrap Admin with a fresh, new design inspired by Google's Material Design.">
    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Material Dashboard PRO by Creative Tim | Premium Bootstrap Admin Template">
    <meta itemprop="description" content="Material Dashboard PRO is a Premium Material Bootstrap Admin with a fresh, new design inspired by Google's Material Design.">
    <meta itemprop="image" content="../../../s3.amazonaws.com/creativetim_bucket/products/51/opt_mdp_thumbnail.jpg">
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@creativetim">
    <meta name="twitter:title" content="Material Dashboard PRO by Creative Tim | Premium Bootstrap Admin Template">
    <meta name="twitter:description" content="Material Dashboard PRO is a Premium Material Bootstrap Admin with a fresh, new design inspired by Google's Material Design.">
    <meta name="twitter:creator" content="@creativetim">
    <meta name="twitter:image" content="../../../s3.amazonaws.com/creativetim_bucket/products/51/opt_mdp_thumbnail.jpg">
    <!-- Open Graph data -->
    <meta property="fb:app_id" content="655968634437471">
    <meta property="og:title" content="Material Dashboard PRO by Creative Tim | Premium Bootstrap Admin Template" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://www.creative-tim.com/product/material-dashboard-pro" />
    <meta property="og:image" content="../../../s3.amazonaws.com/creativetim_bucket/products/51/opt_mdp_thumbnail.jpg" />
    <meta property="og:description" content="Material Dashboard PRO is a Premium Material Bootstrap Admin with a fresh, new design inspired by Google's Material Design." />
    <meta property="og:site_name" content="Creative Tim" />
    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="assets/css/material-dashboard.css" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/google-roboto-300-700.css" rel="stylesheet" />
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-active-color="rose" data-background-color="black" data-image="assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
        Tip 2: you can also add an image using data-image tag
        Tip 3: you can change the color of the sidebar with data-background-color="white | black"
    -->
            <div class="logo">
                <a href="" class="simple-text">
                    MGT
                </a>
            </div>
            <div class="logo logo-mini">
                <a href="" class="simple-text">
                    
                </a>
            </div>
            <div class="sidebar-wrapper">
                <div class="user">
                    <div class="photo">
                        <img src="assets/img/faces/avatar.jpg" />
                    </div>
                    <div class="info">
                        <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                            <p>Tania Andrew
                            	<b class="caret"></b>
							</p>
                        </a>
                        <div class="collapse" id="collapseExample">
                            <ul class="nav">
                                <li>
                                    <a href="#">My Profile</a>
                                </li>
                                <li>
                                    <a href="#">Edit Profile</a>
                                </li>
                                <li>
                                    <a href="#">Settings</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <ul class="nav">
                    <li class="active">
                        <a href="#">
                            <i class="material-icons">dashboard</i>
                            <p>Sale</p>
                        </a>
                    </li>
					<li>
                        <a href="#" data-opt="fetch_customer">
                            <i class="material-icons">account_box</i>
                            <p>Customer</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="material-icons">assignment_ind</i>
                            <p>Employee</p>
                        </a>
                        
                    </li>
                    <li>
                        <a href="#" data-opt="fetch_product">
                            <i class="material-icons">category</i>
                            <p>Product</p>
                        </a>
                    </li>
					<li>
                        <a data-toggle="collapse" href="#prepareExamples">
                            <i class="material-icons">cached</i>
                            <p>Repair Product
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="prepareExamples">
                            <ul class="nav">
                                <li>
                                    <a href="#">Recive Product</a>
                                </li>
                                <li>
                                    <a href="#">Return Product</a>
                                </li>
                                <li>
                                    <a href="#">Remaining In Hand</a>
                                </li>
                                
                            </ul>
                        </div>
                    </li>
					<li>
                        <a href="#">
                            <i class="material-icons">receipt</i>
                            <p>Invioces</p>
                        </a>
                    </li>
             		
                    <li>
                        <a href="#">
                            <i class="material-icons">timeline</i>
                            <p>Charts</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="material-icons">date_range</i>
                            <p>Calendar</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                            <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                            <i class="material-icons visible-on-sidebar-mini">view_list</i>
                        </button>
                    </div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" id="title" href="#"> Dashboard </a>
						<a class="navbar-brand" id="subTitle" href="#"></a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="hidden-lg hidden-md">
                                        Notifications
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">Mike John responded to your email</a>
                                    </li>
                                    <li>
                                        <a href="#">You have 5 new tasks</a>
                                    </li>
                                    <li>
                                        <a href="#">You're now friend with Andrew</a>
                                    </li>
                                    <li>
                                        <a href="#">Another Notification</a>
                                    </li>
                                    <li>
                                        <a href="#">Another One</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                            <li class="separator hidden-lg hidden-md"></li>
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group form-search is-empty">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form>
                    </div>
                </div>
            </nav>
            <div class="content">
                
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Blog
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p class="copyright pull-right">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="http://www.creative-tim.com/">Creative Tim</a>, made with love for a better web
                    </p>
                </div>
            </footer>
        </div>
    </div>
    <div class="fixed-plugin">
        <div class="dropdown show-dropdown">
            <a href="#" data-toggle="dropdown">
                <i class="fa fa-cog fa-2x"> </i>
            </a>
            <ul class="dropdown-menu">
                <li class="header-title"> Sidebar Filters</li>
                <li class="adjustments-line">
                    <a href="javascript:void(0)" class="switch-trigger active-color">
                        <div class="badge-colors text-center">
                            <span class="badge filter badge-purple" data-color="purple"></span>
                            <span class="badge filter badge-blue" data-color="blue"></span>
                            <span class="badge filter badge-green" data-color="green"></span>
                            <span class="badge filter badge-orange" data-color="orange"></span>
                            <span class="badge filter badge-red" data-color="red"></span>
                            <span class="badge filter badge-rose active" data-color="rose"></span>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li class="header-title">Sidebar Background</li>
                <li class="adjustments-line">
                    <a href="javascript:void(0)" class="switch-trigger background-color">
                        <div class="text-center">
                            <span class="badge filter badge-white" data-color="white"></span>
                            <span class="badge filter badge-black active" data-color="black"></span>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li class="adjustments-line">
                    <a href="javascript:void(0)" class="switch-trigger">
                        <p>Sidebar Mini</p>
                        <div class="togglebutton switch-sidebar-mini">
                            <label>
                                <input type="checkbox" unchecked="">
                            </label>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li class="adjustments-line">
                    <a href="javascript:void(0)" class="switch-trigger">
                        <p>Sidebar Image</p>
                        <div class="togglebutton switch-sidebar-image">
                            <label>
                                <input type="checkbox" checked="">
                            </label>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li class="header-title">Images</li>
                <li class="active">
                    <a class="img-holder switch-trigger" href="javascript:void(0)">
                        <img src="assets/img/sidebar-1.jpg" alt="" />
                    </a>
                </li>
                <li>
                    <a class="img-holder switch-trigger" href="javascript:void(0)">
                        <img src="assets/img/sidebar-2.jpg" alt="" />
                    </a>
                </li>
                <li>
                    <a class="img-holder switch-trigger" href="javascript:void(0)">
                        <img src="assets/img/sidebar-3.jpg" alt=""/>
                    </a>
                </li>
                <li>
                    <a class="img-holder switch-trigger" href="javascript:void(0)">
                        <img src="assets/img/sidebar-4.jpg" alt="" />
                    </a>
                </li>
                
            </ul>
        </div>
    </div>

<div class="block-modal">
	
</div>
</body>
<!-- Modal -->

<!--   Core JS Files   -->
<script src="assets/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/material.min.js" type="text/javascript"></script>
<script src="assets/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="assets/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="assets/js/moment.min.js"></script>
<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="assets/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>
<!--   Sharrre Library    -->
<script src="assets/js/jquery.sharrre.js"></script>
<!-- DateTimePicker Plugin -->
<script src="assets/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="assets/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="assets/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<!--<script src="../assets/js/jquery.select-bootstrap.js"></script>-->
<!-- Select Plugin -->
<script src="assets/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="assets/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="assets/js/sweetalert2.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="assets/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="assets/js/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="assets/js/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="assets/js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

<script src="assets/js/category.js"></script>
	
<script src="assets/js/fetchdata.js"></script>

<script src="assets/js/savedata.js"></script>
	
<script src="assets/js/modaldata.js"></script>
	
<script src="assets/js/get_district.js"></script>
	
<script src="assets/js/get_model.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
		var opt;
		var count_row=1;
		//Function Click Style
		var titile='';
		var subTitle='';
		$('body').on('click','.sidebar-wrapper ul li a',function(){
			titile=$(this).find('p').text();
			$(this).parent().addClass('active').siblings().removeClass('active');
			$(this).parents('ul').find('.collapse ul li').removeClass('active');
			$('#title').text(titile);
			$('#subTitle').text('');
			
		});
		$('body').on('click','.sidebar-wrapper .collapse ul li a',function(){
			subTitle=$(this).text();
			$(this).parents('.sidebar-wrapper').find('ul li').removeClass('active');
			$(this).parent().addClass('active').siblings().removeClass('active');
			$(this).parents('li').addClass('active');
			titile=$(this).parents('li').find('p').text();
			$('#title').text(titile);
			$('#subTitle').text('/ '+subTitle);
			
		});
		
		//Add Category
		$('body').on('click','#add-category',function(){
			category_modal_add();
		});
		//Add Product
		$('body').on('click','#add-product',function(){
			product_modal_add();
		});
		
		//Add Customer
		$('body').on('click','#add-customer',function(){
			customer_modal_add();
		});
		//Add Position
		$('body').on('click','#add-position',function(){
			position_modal_add();
		});
		//Add Car Branch
		$('body').on('click','#add-carBranch',function(){
			carBranch_modal_add();
			
		});
		$('body').on('click','#add-employee',function(){
			employee_modal_add();
			
		});
		$('body').on('click','#add-stock',function(){
			inStock_modal_add();
		});
		//Edit Category
		$('body').on("click", ".edit", function() {
			
			$tr = $(this).closest("tr");
			var data = $('#datatables').DataTable().row($tr).data();
			
			if(opt=='fetch_category'){
				
				var src=$(this).closest("tr").find('img').attr('src');
				var img='<img src="'+src+'" class="img-rounded">';
				var replace=src.replace('img/','');
				category_modal_edit(data,img,replace);
			}
			if(opt=='fetch_product'){
				var category_id=$tr.find('input[type="hidden"]').val();
				var src=$(this).closest("tr").find('img').attr('src');
				var img='<img src="'+src+'" class="img-rounded">';
				var replace=src.replace('img/','');
				product_modal_edit(data,img,replace,category_id);
			}
			if(opt=='fetch_customer'){
				var province=$tr.find('input[name="t_province"]').val();
				var district=$tr.find('input[name="t_district"]').val();
				var src=$(this).closest("tr").find('img').attr('src');
				var img='<img src="'+src+'" class="img-rounded">';
				var replace=src.replace('img/','');
				customer_modal_edit(data,img,replace,province,district);
			}
			if(opt=='fetch_position'){
				position_modal_edit(data);
			}
			if(opt=='fetch_carBranch'){
				carBranch_modal_edit(data);
			}
			if(opt=='fetch_employee'){
				employee_modal_edit(data[0]);
			}
			if(opt=='fetch_inStock'){
				inStock_modal_edit(data[0]);
			}
		});
		// Delete a record
		$('body').on("click", ".remove", function(e) {
			$tr = $(this).closest("tr");
			$('#datatables').DataTable().row($tr).remove().draw();
			e.preventDefault();
		});
		//Fetch District
		$('body').on('change','#province',function(){
			var province_id=$(this).val();
			get_district(province_id);
			
		});
		//Fetch Product
		$('body').on('change','#category',function(){
			var cate_id=$(this).val();
			var tr=$(this).parents('tr').attr('id');
			get_product(cate_id,tr);
		});
		//Fetch Cost Price
		$('body').on('change','#product',function(){
			var product_id=$(this).val();
			var tr=$(this).parents('tr').attr('id');
			get_product_detail(product_id,tr);
		});
		//Add row stock
		$('body').on('click','.add-row',function(){
			var tr='row'+count_row;
			get_row_stock(tr)
			count_row++;
		});
		//Delete Row Stock
		$('body').on('click','.delete',function(){
			var tr=$(this).parents('tr').attr('id');
			$('#'+tr+'').remove();
		});
		//Change QTY Stock
		$('body').on('change','.txt_qty',function(){
			var tr=$(this).parents('tr').attr('id');
			qty_changed(tr);
		});
		//Remove Amount
		$('body').on('click','.remove-amount',function(){
			var qty=$(this).parents('tr').find('.txt_qty');
			var tr=$(this).parents('tr').attr('id');
			if(qty.val()>1){
				qty.val(qty.val()-1);
			}
			qty_changed(tr);
		});
		//Add Amount
		$('body').on('click','.add-amount',function(){
			var qty=$(this).parents('tr').find('.txt_qty');
			var tr=$(this).parents('tr').attr('id');
			qty.val(parseInt(qty.val())+1);
			qty_changed(tr);
		});
		//Change Menu
		$('body').on('click','.sidebar-wrapper ul li a',function(){
			var eThis=$(this);
			opt=eThis.data('opt');
			
			if(opt=='fetch_category'){
				fetch_category();
			}
			if(opt=='fetch_product'){
				fetch_product();
			}
			if(opt=='fetch_customer'){
				fetch_customer();
				
			}
			if(opt=='fetch_position'){
				fetch_position();
			}
			if(opt=='fetch_carBranch'){
				fetch_carBranch();
			}
			if(opt=='fetch_employee'){
				fetch_employee();
			}
			if(opt=='fetch_inStock'){
				fetch_inStock();
			}
		});
		
		
        // Javascript method's body can be found in assets/js/demos.js
       //demo.initVectorMap();
    });
</script>


<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Mar 2017 21:32:16 GMT -->
</html>