<?php
session_start();
if(!isset($_SESSION['user'])){
	header('Location: ../login.php');
}
else{

?>
<!doctype html>
<html lang="en">


<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Mar 2017 21:29:18 GMT -->
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Dashboard</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="assets/css/material-dashboard.css" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/google-roboto-300-700.css" rel="stylesheet" />
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-active-color="rose" data-background-color="black" data-image="assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
        Tip 2: you can also add an image using data-image tag
        Tip 3: you can change the color of the sidebar with data-background-color="white | black"
    -->
            <div class="logo">
                <a href="" class="simple-text">
                Restaurants
                </a>
            </div>
            <div class="logo logo-mini">
                <a href="" class="simple-text">
                    R
                </a>
            </div>
            <div class="sidebar-wrapper">
                <div class="user">
                    <div class="photo">
                        <img src="assets/img/default-avatar.png" />
                    </div>
                    <div class="info">
                        <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                            <p><?php echo $_SESSION['user'];  ?>
                            	<b class="caret"></b>
							</p>
                        </a>
                        <!--<div class="collapse" id="collapseExample">
                            <ul class="nav">
                                <li>
                                    <a href="#">My Profile</a>
                                </li>
                                <li>
                                    <a href="#">Edit Profile</a>
                                </li>
                                <li>
                                    <a href="#">Settings</a>
                                </li>
                            </ul>
                        </div>-->
                    </div>
                </div>
                <ul class="nav">
                    <li class="active">
                        <a href="#" data-opt="fetch_dashboard">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
					<li>
                        <a href="#" data-opt="fetch_customer">
                            <i class="material-icons">account_box</i>
                            <p>Customer</p>
                        </a>
                    </li>
					<li>
                        <a href="#" data-opt="fetch_desk">
                            <i class="material-icons">receipt</i>
                            <p>Table</p>
                        </a>
                    </li>
             		
                    <li>
                        <a data-toggle="collapse" href="#pagesExamples">
                            <i class="material-icons">assignment_ind</i>
                            <p>Employee
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="pagesExamples">
                            <ul class="nav">
                                
                                <li>
                                    <a href="#" data-opt="fetch_employee">Employee</a>
                                </li>
								<li>
                                    <a href="#" data-opt="fetch_position">Positon</a>
                                </li>
								
                                <li>
                                    <a href="#" data-opt="fetch_user">User</a>
                                </li>
                                
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#categoryList">
                            <i class="material-icons">category</i>
                            <p>Product
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="categoryList">
                            <ul class="nav">
                                
                                <li>
                                    <a href="#" data-opt="fetch_foodCategory">Category</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link collapsed" data-toggle="collapse" href="#FoodCollapse" aria-expanded="false">
                                        <span class="sidebar-normal"> Food
                                        <b class="caret"></b>
                                        </span>
                                    </a>
                                    <div class="collapse" id="FoodCollapse" style="">
                                        <ul class="nav">
                                            <li class="nav-item ">
                                                <a href="#" data-opt="fetch_food">Food</a>
                                            </li>
                                            <li class="nav-item ">
                                                <a href="#" data-opt="fetch_foodIngredient">Ingredient</a>
                                            </li>
                                            <li class="nav-item ">
                                                <a href="#" data-opt="fetch_ingredientBuy">Ingredient Buy</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link collapsed" data-toggle="collapse" href="#DrinkCollapse" aria-expanded="false">
                                        <span class="sidebar-normal"> Drink
                                        <b class="caret"></b>
                                        </span>
                                    </a>
                                    <div class="collapse" id="DrinkCollapse" style="">
                                        <ul class="nav">
                                            <li class="nav-item ">
                                                <a href="#" data-opt="fetch_product">Product</a>
                                            </li>
                                            <li class="nav-item ">
                                                <a href="#" data-opt="fetch_inStock">In Stock Products</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </li>
					
					
                    <!--<li>
                        <a data-toggle="collapse" href="#prepareInventory">
                            <i class="material-icons">cached</i>
                            <p>Inventory
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="prepareInventory">
                            <ul class="nav">
                                <li>
                                    <a href="#" data-opt="fetch_inventory">Inventory</a>
                                </li>
                                <li>
                                    <a href="#" data-opt="fetch_inventoryIn">Invenory In</a>
                                </li>
                                <li>
                                    <a href="#" data-opt="fetch_inventoryDamage">Invenory Damage</a>
                                </li>
                                
                            </ul>
                        </div>
                    </li>-->
					<li>
                        <a data-toggle="collapse" href="#prepareSupply">
                            <i class="material-icons">cached</i>
                            <p>Supply
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="prepareSupply">
                            <ul class="nav">
                                <li>
                                    <a href="#" data-opt="fetch_supplier">Supplier</a>
                                </li>
                                <li>
                                    <a href="#" data-opt="fetch_supplierCompany">Company</a>
                                </li>
                                
                                
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#">
                            <i class="material-icons">date_range</i>
                            <p>Calendar</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                            <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                            <i class="material-icons visible-on-sidebar-mini">view_list</i>
                        </button>
                    </div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" id="title" href="#"> Dashboard </a>
						<a class="navbar-brand" id="subTitle" href="#"></a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="../home.php">
                                    <i class="material-icons">shopping_cart</i> Home
                                </a>
                            </li>
                            <li class="dropdown" id="notification-icon">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <p class="hidden-lg hidden-md">
                                        Notifications
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <ul class="dropdown-menu notification-details">
                                    
                                </ul>
                            </li>
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
										<a class="dropdown-item" href="#" id="logout">Log Out</a>
									</div>
                                </a>
                            </li>
                            <li class="separator hidden-lg hidden-md"></li>
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group form-search is-empty">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form>
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="orange">
                                    <i class="material-icons">store</i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Order</p>
                                    <h3 class="card-title" id="order_count"></h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">update</i> Just Updated 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="green">
                                    <i class="material-icons">account_balance</i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Revenue</p>
                                    <h3 class="card-title" id="sum_income"></h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">update</i> Just Updated
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="blue">
                                    <i class="material-icons">swap_horiz</i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Expense</p>
                                    <h3 class="card-title" id="expense"><?php echo "0"; ?></h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">update</i> Just Updated
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <a href="http://localhost:8080/QQ/dashboard/report.php">
<!--                                    <a href="http://localhost/dashboard/report.php">-->
                                    <button class="btn btn-social btn-fill btn-reddit">
                                        <i class="fa fa-tasks"></i> Report
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Order List</h4>
                            <!-- <div class="toolbar">
                                <button class="btn btn-info" id="add-desk">
                                    <span class="btn-label">
                                    <i class="material-icons">shopping_cart</i>
                                    </span>
                                Add Position
                                </button> 
                            </div> -->
                            <div class="responsive material-datatables">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="5%">Order ID</th>
                                            <th width="10%">Order Date</th>
                                            <th width="10%">Payment Date</th>
                                            <th width="10%">Table</th>
                                            <th width="10%">Status</th>
                                            <th width="10%" class="disabled-sorting text-right">Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Order ID</th>
                                            <th>Order Date</th>
                                            <th>Payment Date</th>
                                            <th>Table</th>
                                            <th>Status</th>
                                            <th class="text-right">Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
        <?php
        include('action/connect.php');
        $order="select defacno,date,paid_date,(select name from desk where id=order_food.table_id) as desk,
        case when type='P' then 'Paid' when type='C' then 'Cancel' else 'Not Paid' end status
        from order_food where DATE_FORMAT(order_food.date,'%Y-%m-%d')=DATE_FORMAT(sysdate(),'%Y-%m-%d') order by id desc;";
        $result_order = $connect->prepare($order);
        $result_order->execute(); 
        while($row = $result_order->fetch()){
            echo '
                <tr>
                    <td>'.$row[0].'</td>
                    <td>'.$row[1].'</td>
                    <td>'.$row[2].'</td>
                    <td>'.$row[3].'</td>
                    <td>'.$row[4].'</td>
                    <td class="text-right">
                    <a href="#" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>
                    </td>
                </tr>
            ';
        }
        ?>

                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                                <!-- end content-->
                    </div>
                            <!--  end card  -->
                    
                </div><!--  end container-fluid -->
            </div>
            
        </div>
    </div>
    <div class="fixed-plugin">
        <div class="dropdown show-dropdown">
            <a href="#" data-toggle="dropdown">
                <i class="fa fa-cog fa-2x"> </i>
            </a>
            <ul class="dropdown-menu">
                <li class="header-title"> Sidebar Filters</li>
                <li class="adjustments-line">
                    <a href="javascript:void(0)" class="switch-trigger active-color">
                        <div class="badge-colors text-center">
                            <span class="badge filter badge-purple" data-color="purple"></span>
                            <span class="badge filter badge-blue" data-color="blue"></span>
                            <span class="badge filter badge-green" data-color="green"></span>
                            <span class="badge filter badge-orange" data-color="orange"></span>
                            <span class="badge filter badge-red" data-color="red"></span>
                            <span class="badge filter badge-rose active" data-color="rose"></span>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li class="header-title">Sidebar Background</li>
                <li class="adjustments-line">
                    <a href="javascript:void(0)" class="switch-trigger background-color">
                        <div class="text-center">
                            <span class="badge filter badge-white" data-color="white"></span>
                            <span class="badge filter badge-black active" data-color="black"></span>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li class="adjustments-line">
                    <a href="javascript:void(0)" class="switch-trigger">
                        <p>Sidebar Mini</p>
                        <div class="togglebutton switch-sidebar-mini">
                            <label>
                                <input type="checkbox" unchecked="">
                            </label>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li class="adjustments-line">
                    <a href="javascript:void(0)" class="switch-trigger">
                        <p>Sidebar Image</p>
                        <div class="togglebutton switch-sidebar-image">
                            <label>
                                <input type="checkbox" checked="">
                            </label>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </li>
                <li class="header-title">Images</li>
                <li class="active">
                    <a class="img-holder switch-trigger" href="javascript:void(0)">
                        <img src="assets/img/sidebar-1.jpg" alt="" />
                    </a>
                </li>
                <li>
                    <a class="img-holder switch-trigger" href="javascript:void(0)">
                        <img src="assets/img/sidebar-2.jpg" alt="" />
                    </a>
                </li>
                <li>
                    <a class="img-holder switch-trigger" href="javascript:void(0)">
                        <img src="assets/img/sidebar-3.jpg" alt=""/>
                    </a>
                </li>
                <li>
                    <a class="img-holder switch-trigger" href="javascript:void(0)">
                        <img src="assets/img/sidebar-4.jpg" alt="" />
                    </a>
                </li>
                
            </ul>
        </div>
    </div>

<div class="block-modal">
	
</div>
</body>
<!-- Modal -->

<!--   Core JS Files   -->
<script src="assets/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/material.min.js" type="text/javascript"></script>
<script src="assets/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="assets/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="assets/js/moment.min.js"></script>
<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="assets/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>
<!--   Sharrre Library    -->
<script src="assets/js/jquery.sharrre.js"></script>
<!-- DateTimePicker Plugin -->
<script src="assets/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="assets/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="assets/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<!--<script src="../assets/js/jquery.select-bootstrap.js"></script>-->
<!-- Select Plugin -->
<script src="assets/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="assets/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="assets/js/sweetalert2.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="assets/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="assets/js/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="assets/js/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="assets/js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

<script src="assets/js/category.js"></script>
	
<script src="assets/js/fetchdata.js"></script>

<script src="assets/js/savedata.js"></script>
	
<script src="assets/js/modaldata.js"></script>
	
<script src="assets/js/get_district.js"></script>
	
<script src="assets/js/get_model.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
		var opt;
		var count_row=1;
		//Function Click Style
		var titile='';
        var subTitle='';
        
        function order_count(){
            $('#order_count').load('action/order_count.php');
            setInterval(function() {
                $('#order_count').load('action/order_count.php');
            }, 5000);
        }
        function sum_income(){
            $('#sum_income').load('action/sum_income.php');
            setInterval(function() {
                $('#sum_income').load('action/sum_income.php');
            }, 5000);
        }

        order_count();
        sum_income();

		$('body').on('click','.sidebar-wrapper ul li a',function(){
			titile=$(this).find('p').text();
			$(this).parent().addClass('active').siblings().removeClass('active');
			$(this).parents('ul').find('.collapse ul li').removeClass('active');
			$('#title').text(titile);
			$('#subTitle').text('');
			
		});
		$('body').on('click','.sidebar-wrapper .collapse ul li a',function(){
			subTitle=$(this).text();
			$(this).parents('.sidebar-wrapper').find('ul li').removeClass('active');
			$(this).parent().addClass('active').siblings().removeClass('active');
			$(this).parents('li').addClass('active');
			titile=$(this).parents('li').find('p').text();
			$('#title').text(titile);
			$('#subTitle').text('/ '+subTitle);
			
		});
		
		//Add Category
		$('body').on('click','#add-category',function(){
			category_modal_add();
		});
		//Add Product
		$('body').on('click','#add-product',function(){
			product_modal_add();
		});
		
		//Add Customer
		$('body').on('click','#add-customer',function(){
			customer_modal_add();
		});
		//Add Position
		$('body').on('click','#add-position',function(){
			position_modal_add();
		});
		//Add Car Branch
		$('body').on('click','#add-carBranch',function(){
			carBranch_modal_add();
			
		});
		$('body').on('click','#add-employee',function(){
			employee_modal_add();
			
		});
		$('body').on('click','#add-stock',function(){
			inStock_modal_add();
		});
		$('body').on('click','#transfer-stock',function(){
			transfer_modal_add();
		});
		$('body').on('click','#add-company',function(){
			supplierCompany_modal_add();
		});
		$('body').on('click','#add-supplier',function(){
			supplier_modal_add();
		});
		$('body').on('click','#add-foodCategory',function(){
			foodCategory_modal_add();
		});
		$('body').on('click','#add-food',function(){
			food_modal_add();
		});
		$('body').on('click','#add-ingredient',function(){
			ingredient_modal_add();
		});
		$('body').on('click','#add-ingredientBuy',function(){
			ingredientBuy_modal_add();
		});
		$('body').on('click','#add-desk',function(){
			desk_modal_add();
        });
        $('body').on('click','#add-user',function(){
			user_modal_add();
		});
		//Edit Category
		$('body').on("click", ".edit", function() {
			
			$tr = $(this).closest("tr");
			var data = $('#datatables').DataTable().row($tr).data();
			
			if(opt=='fetch_category'){
				
				var src=$(this).closest("tr").find('img').attr('src');
				var img='<img src="'+src+'" class="img-rounded">';
				var replace=src.replace('img/','');
				category_modal_edit(data,img,replace);
			}
			if(opt=='fetch_product'){
				var category_id=$tr.find('input[type="hidden"]').val();
				var src=$(this).closest("tr").find('img').attr('src');
				var img='<img src="'+src+'" class="img-rounded">';
				var replace=src.replace('img/','');
				product_modal_edit(data,img,replace,category_id);
			}
			if(opt=='fetch_customer'){
				var province=$tr.find('input[name="t_province"]').val();
				var district=$tr.find('input[name="t_district"]').val();
				var src=$(this).closest("tr").find('img').attr('src');
				var img='<img src="'+src+'" class="img-rounded">';
				var replace=src.replace('img/','');
				customer_modal_edit(data,img,replace,province,district);
			}
			if(opt=='fetch_position'){
				position_modal_edit(data);
			}
			
			if(opt=='fetch_employee'){
				employee_modal_edit(data[0]);
			}
			if(opt=='fetch_inStock'){

				inStock_modal_edit(data[0]);
			}
			if(opt=='fetch_supplierCompany'){
				
				var province=$tr.find('input[name="t_province"]').val();
				var district=$tr.find('input[name="t_district"]').val();
				var src=$(this).closest("tr").find('img').attr('src');
				var img='<img src="'+src+'" class="img-rounded">';
				var replace=src.replace('img/','');
				supplierCompany_modal_edit(data,img,replace,province,district);
				
			}
			if(opt=='fetch_supplier'){
				
				var company=$tr.find('input[name="t_company"]').val();
				var src=$(this).closest("tr").find('img').attr('src');
				var img='<img src="'+src+'" class="img-rounded">';
				var replace=src.replace('img/','');
				supplier_modal_edit(data,img,replace,company);
				
			}
			if(opt=='fetch_foodCategory'){
				
				var src=$(this).closest("tr").find('img').attr('src');
				var img='<img src="'+src+'" class="img-rounded">';
				var replace=src.replace('img/','');
				foodCategory_modal_edit(data,img,replace);
			}
			if(opt=='fetch_food'){
				var category_id=$tr.find('input[type="hidden"]').val();
				var src=$(this).closest("tr").find('img').attr('src');
				var img='<img src="'+src+'" class="img-rounded">';
				var replace=src.replace('img/','');
				food_modal_edit(data,img,replace,category_id);
			}
			if(opt=='fetch_foodIngredient'){
				var scale_id=$tr.find('input[type="hidden"]').val();
				var src=$(this).closest("tr").find('img').attr('src');
				var img='<img src="'+src+'" class="img-rounded">';
				var replace=src.replace('img/','');
				ingredient_modal_edit(data,img,replace,scale_id);
			}
			if(opt=='fetch_desk'){
				desk_modal_edit(data);
            }
            if(opt=='fetch_user'){
                user_modal_edit(data[0]);
            }
            if(opt=='fetch_dashboard'){
                order_dashboard(data[0]);
            }
		});
		// Delete a record
		$('body').on("click", ".remove", function(e) {
			$tr = $(this).closest("tr");
			$('#datatables').DataTable().row($tr).remove().draw();
			e.preventDefault();
		});
		//Fetch District
		$('body').on('change','#province',function(){
			var province_id=$(this).val();
			get_district(province_id);
			
		});
		//Fetch Product
		$('body').on('change','#category',function(){
			var cate_id=$(this).val();
			var tr=$(this).parents('tr').attr('id');
			get_product(cate_id,tr);
		});
		//Fetch Cost Price
		$('body').on('change','#product',function(){
			var product_id=$(this).val();
			var tr=$(this).parents('tr').attr('id');
			get_product_detail(product_id,tr);
		});
		//Add row stock
		$('body').on('click','.add-row',function(){
			var tr='row'+count_row;

            if(opt=='fetch_ingredientBuy'){
                get_row_ingre(tr);
            }
            else {
                get_row_stock(tr);
            }
			count_row++;
		});
		//Delete Row Stock
		$('body').on('click','.delete',function(){
			var tr=$(this).parents('tr').attr('id');
			$('#'+tr+'').remove();
		});
		//Change QTY Stock
		$('body').on('change','.txt_qty',function(){
			var tr=$(this).parents('tr').attr('id');
			qty_changed(tr);
		});
		//Remove Amount
		$('body').on('click','.remove-amount',function(){
			var qty=$(this).parents('tr').find('.txt_qty');
			var tr=$(this).parents('tr').attr('id');
			if(qty.val()>1){
				qty.val(qty.val()-1);
			}
			qty_changed(tr);
		});
		//Add Amount
		$('body').on('click','.add-amount',function(){
			var qty=$(this).parents('tr').find('.txt_qty');
			var tr=$(this).parents('tr').attr('id');
			qty.val(parseInt(qty.val())+1);
			qty_changed(tr);
		});
		$('body').on('change','#select_branch',function(){
			var branch=$(this).val();
			if(branch!="0"){
				$.ajax({
					url:'action/assign_product_branch.php',
					type: "POST",             // Type of request to be send, called as method
					data: {branch:branch}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					//contentType: false,       // The content type used when sending data to the server.
					cache: false,             // To unable request pages to be cached
					//processData:false,        // To send DOMDocument or non processed data file it is set to false
					success: function(data){
						$('.content').html(data);
						$("#select_branch").val(branch);
					}
				});
			}
			else{
				assign_product();
			}
			
		});
        $('body').on('change','#ingredients',function () {
            var eThis=$(this);
            var ingrediantsid=eThis.parents('tr').find('#ingredients').val();
            $.ajax({
                url:'action/getPrice.php',
                type: "POST",             // Type of request to be send, called as method
                data: {ingredientsid:ingrediantsid}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                //contentType: false,       // The content type used when sending data to the server.
                //cache: false,             // To unable request pages to be cached
                //processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(data){
                    eThis.parents('tr').find('#ingredientsCost').val(data);
                    var total=parseFloat(data)*1;

                    eThis.parents('tr').find('.txt_total').val(total);
                }
            });
        });
        $('body').on('click','#addingredientModal',function () {
            alert();
        });
		//Change Menu
		$('body').on('click','.sidebar-wrapper ul li a',function(){
			var eThis=$(this);
			opt=eThis.data('opt');
			
			if(opt=='fetch_category'){
				fetch_category();
			}
			if(opt=='fetch_product'){
				fetch_product();
			}
			if(opt=='fetch_customer'){
				fetch_customer();
			}
			if(opt=='fetch_position'){
				fetch_position();
			}
			if(opt=='fetch_carBranch'){
				fetch_carBranch();
			}
			if(opt=='fetch_employee'){
				fetch_employee();
			}
			if(opt=='fetch_inStock'){
				fetch_inStock();
			}
			if(opt=='assign_qtyproduct'){
				assign_qtyproduct();
			}
			if(opt=='assign_product'){
				assign_product();
			}
			if(opt=='fetch_supplier'){
				fetch_supplier();
			}
			if(opt=='fetch_supplierCompany'){
				fetch_supplierCompany();
			}
			if(opt=='fetch_foodCategory'){
				fetch_foodCategory();
			}
			if(opt=='fetch_food'){
				fetch_food();
			}
			if(opt=='fetch_foodIngredient'){
				fetch_foodIngredient();
			}
			if(opt=='fetch_ingredientBuy'){
				fetch_ingredientBuy();
			}
			if(opt=='fetch_desk'){
				fetch_desk();
			}
			if(opt=='fetch_inventory'){
				fetch_inventory();
			}
			if(opt=='fetch_inventoryIn'){
				fetch_inventoryIn();
			}
			if(opt=='fetch_inventoryDamage'){
				fetch_inventoryDamage();
            }
            if(opt=='fetch_user'){
                fetch_user();
            }
            if(opt=='fetch_dashboard'){
                fetch_dashboard();
            }
		});
		
		$(document).on("click","#logout",function(){
            $.ajax({
                url:"../logout.php",
                method:"post",
                success:function(data){
                    location.reload();
                }
            });
        });
        // Javascript method's body can be found in assets/js/demos.js
       //demo.initVectorMap();
    });
</script>


</html>

<?php
}
?>