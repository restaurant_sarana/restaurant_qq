// JavaScript Document

//category Modal
function category_modal_add(){
	$.ajax({
		url:'action/category-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('#txt_category').closest('div').addClass('is-empty').removeClass('is-focused');
			$('#categoryModal').modal('show');
			$('#add_edit_cate').val('0');
			$('#save-category').val('ADD');
			$('#txt_category').val('');
			$('select[name=txt_status]').val('1');
			$('.selectpicker').selectpicker('refresh');
			$('.fileinput').fileinput('clear');

			
		}
	});
}
function category_modal_edit(data1,img,replace){
	$.ajax({
		url:'action/category-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.fileinput.fileinput-new.text-center').addClass('fileinput-exists').removeClass('fileinput-new');
			$('#txt_category').closest('div').removeClass('is-empty').addClass('is-focused');
			
			$('#categoryModal').modal('show');
			$('#add_edit_cate').val('1');
			$('#save-category').val('Edit');
			$('#txt_category').val(data1[1]);
			
			$('.fileinput-preview.fileinput-exists.thumbnail').html(img);
			$('.fileinput-filename').val(replace);
			$('#txt_id').val(data1[0]);
			$('select[name=txt_status]').val(data1[3]);
			$('.selectpicker').selectpicker('refresh');
		}
	});
}
//Product Modal
function product_modal_add(){
	$.ajax({
		url:'action/product-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#productModal').modal('show');
			$('#add_edit').val('0');
			$('#save-product').val('ADD');
			$('#txt_product').val('');
			$('.fileinput').fileinput('clear');
			
		}
	});
}

function product_modal_edit(data1,img,replace,category_id){
	$.ajax({
		url:'action/product-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.fileinput.fileinput-new.text-center').addClass('fileinput-exists').removeClass('fileinput-new');
			$('#txt_category').closest('div').removeClass('is-empty').addClass('is-focused');
			
			$('#productModal').modal('show');
			$('#add_edit_prod').val('1');
			$('#save-product').val('Edit');
			$('#category').val(category_id);
			$('#txt_product').val(data1[2]);
			$('#txt_cost').val(data1[3]);
			$('#txt_price').val(data1[4]);
			$('.fileinput-preview.fileinput-exists.thumbnail').html(img);
			$('.fileinput-filename').val(replace);
			$('#txt_id').val(data1[0]);
			$('select[name=txt_status]').val(data1[6]);
			$('.selectpicker').selectpicker('refresh');
		}
	});
}
function customer_modal_add(){
	$.ajax({
		url:'action/customer-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#customerModal').modal('show');
			$('#add_edit_prod').val('0');
			$('#save-customer').val('ADD');
			$('.fileinput').fileinput('clear');
			
		}
	});
}
function customer_modal_edit(data1,img,replace,province,district){
	$.ajax({
		url:'action/customer-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.fileinput.fileinput-new.text-center').addClass('fileinput-exists').removeClass('fileinput-new');
			//$('#txt_category').closest('div').removeClass('is-empty').addClass('is-focused');
			
			$('#customerModal').modal('show');
			$('#add_edit_prod').val('1');
			$('#save-customer').val('Edit');
			$('#province').val(province);
			get_district_edit(province,district);
			$('#txt_firstName').val(data1[1]);
			$('#txt_lastName').val(data1[2]);
			$('#txt_address').val(data1[5]);
			$('#txt_phoneNumber').val(data1[6]);
			$('.fileinput-preview.fileinput-exists.thumbnail').html(img);
			$('.fileinput-filename').val(replace);
			$('#txt_id').val(data1[0]);
			//$('select[name=txt_status]').val(data1[6]);
			$(".selectpicker").selectpicker("refresh");
		}
	});
	
}
function position_modal_add(){
	$.ajax({
		url:'action/position-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#positionModal').modal('show');
			$('#add_edit').val('0');
			$('#save-position').val('ADD');
			//$('.fileinput').fileinput('clear');
			
		}
	});
}
function position_modal_edit(data1){
	$.ajax({
		url:'action/position-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			//$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#positionModal').modal('show');
			$('#add_edit_prod').val('1');
			$('#save-position').val('Edit');
			$('#txt_id').val(data1[0]);
			$('#txt_position').val(data1[1]);
			$('#txt_position_eng').val(data1[2]);
			//$('.fileinput').fileinput('clear');
			
		}
	});
}

function employee_modal_add(){
	$.ajax({
		url:'action/employee-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#employeeModal').modal('show');
			$('#add_edit_prod').val('0');
			$('#save-employee').val('ADD');
			$('.fileinput').fileinput('clear');
			
		}
	});
}
function employee_modal_edit(id){
	$.ajax({
		url:'action/fetch_data.php',
		type: "POST",             // Type of request to be send, called as method
		data: {id:id}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		//contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		//processData:false,        // To send DOMDocument or non processed data file it is set to false
		dataType: 'json',
		success: function(data){
			$.ajax({
			url:'action/employee-modal.php',
			type: "POST",             // Type of request to be send, called as method
			data: {dob:data.dateofbirth}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			//contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			//processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data1){
				$('.block-modal').html(data1);
				$('.fileinput.fileinput-new.text-center').addClass('fileinput-exists').removeClass('fileinput-new');
				//$('#txt_category').closest('div').removeClass('is-empty').addClass('is-focused');
				var img='<img src="img/'+data.photo+'" class="img-rounded">';
				$('#employeeModal').modal('show');
				$('#add_edit_prod').val('1');
				$('#save-employee').val('Edit');
				$('#txt_firstName').val(data.firstName);
				$('#txt_lastName').val(data.lastName);
				$('#txt_sex').val(data.sex);
				$('#txt_address').val(data.address);
				$('#province').val(data.province);
				$('#txt_phoneNumber').val(data.phoneNumber);
				$('#txt_position').val(data.position);
				$('#txt_salary').val(data.salary);
				$('.fileinput-preview.fileinput-exists.thumbnail').html(img);
				$('.fileinput-filename').val(data.photo);
				$('#txt_id').val(data.id);
				////$('select[name=txt_status]').val(data1[6]);
				$(".selectpicker").selectpicker("refresh");
				
				}
			});
		}
	});
}
function inStock_modal_add(){
	$.ajax({
		url:'action/inStock-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#inStockModal').modal('show');
			$('#add_edit').val('0');
			$('#save-stock').val('ADD');
			$('.fileinput').fileinput('clear');
			
		}
	});
}
function inStock_modal_edit(id){
	$.ajax({
		url:'action/fetch_data_stock.php',
		type: "POST",             // Type of request to be send, called as method
		data: {id:id}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		//contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		//processData:false,        // To send DOMDocument or non processed data file it is set to false
		dataType: 'json',
		success: function(data){
			var len=data.length;
			var id=[];
			var category_id=[];
			var product_id=[];
			var qty=[];
			var cost=[];
			var subtotal=[];
			var tr=[];
			for(var i=0;i<len;i++){
				id.push(data[i].id);
				category_id.push(data[i].category_id);
				product_id.push(data[i].product_id);
				qty.push(data[i].qty);
				cost.push(data[i].cost);
				subtotal.push(data[i].subtotal);
				tr.push("row"+i+"");

			}
			$.ajax({
				url:'action/inStock-modal-edit.php',
				type: "POST",             // Type of request to be send, called as method
				data: {id:id,category:category_id,product:product_id,qty:qty,cost:cost,subtotal:subtotal}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				//contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				//processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(resonse){
					$('.block-modal').html(resonse);
					$('.label-floating').addClass('is-empty').removeClass('is-focused');
					$('#inStockModal').modal('show');
					$('#add_edit_prod').val('1');
					$('#save-stock').val('Delete');
					qty_changed();
				}
			});

		}
	});
}
function transfer_modal_add(){
	$.ajax({
		url:'action/transfer-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#transferModal').modal('show');
			$('#add_edit').val('0');
			$('#save-transfer').val('ADD');
			$('.fileinput').fileinput('clear');
			
		}
	});
}
function supplierCompany_modal_add(){
	$.ajax({
		url:'action/supplierCompany-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#CompanyModal').modal('show');
			$('#add_edit').val('0');
			$('#save-product').val('ADD');
			$('.fileinput').fileinput('clear');
			
		}
	});
}
function supplierCompany_modal_edit(data1,img,replace,province,district){
	$.ajax({
		url:'action/supplierCompany-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.fileinput.fileinput-new.text-center').addClass('fileinput-exists').removeClass('fileinput-new');
			//$('#txt_category').closest('div').removeClass('is-empty').addClass('is-focused');
			
			$('#CompanyModal').modal('show');
			$('#add_edit_prod').val('1');
			$('#save-company').val('Edit');
			$('#province').val(province);
			get_district_edit(province,district);
			$('#txt_company').val(data1[1]);
			$('#txt_address').val(data1[4]);
			$('#txt_phoneNumber').val(data1[5]);
			$('#txt_email').val(data1[6])
			$('.fileinput-preview.fileinput-exists.thumbnail').html(img);
			$('.fileinput-filename').val(replace);
			$('#txt_id').val(data1[0]);
			//$('select[name=txt_status]').val(data1[6]);
			$(".selectpicker").selectpicker("refresh");
		}
	});
	
}
function supplier_modal_add(){
	$.ajax({
		url:'action/supplier-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#supplierModal').modal('show');
			$('#add_edit').val('0');
			$('#save-supplier').val('ADD');
			$('.fileinput').fileinput('clear');
			
		}
	});
}
function supplier_modal_edit(data1,img,replace,company){
	$.ajax({
		url:'action/supplier-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.fileinput.fileinput-new.text-center').addClass('fileinput-exists').removeClass('fileinput-new');
			//$('#txt_category').closest('div').removeClass('is-empty').addClass('is-focused');
			
			$('#supplierModal').modal('show');
			$('#add_edit_prod').val('1');
			$('#save-supplier').val('Edit');
			$('#company').val(company);
			$('#txt_firstName').val(data1[2]);
			$('#txt_lastName').val(data1[3]);
			$('#txt_sex').val(data1[4]);
			$('#txt_address').val(data1[5]);
			$('#txt_phoneNumber').val(data1[6]);
			$('#txt_email').val(data1[7]);
			$('.fileinput-preview.fileinput-exists.thumbnail').html(img);
			$('.fileinput-filename').val(replace);
			$('#txt_id').val(data1[0]);
			//$('select[name=txt_status]').val(data1[6]);
			$(".selectpicker").selectpicker("refresh");
		}
	});
	
}
function foodCategory_modal_add(){
	$.ajax({
		url:'action/foodCategory-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('#foodCategoryModal').modal('show');
			$('#add_edit_cate').val('0');
			$('#save-foodCategory').val('ADD');
			$('#txt_category').val('');
			$('select[name=txt_status]').val('1');
			$('.selectpicker').selectpicker('refresh');
			$('.fileinput').fileinput('clear');

			
		}
	});
}
function foodCategory_modal_edit(data1,img,replace){
	$.ajax({
		url:'action/foodCategory-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.fileinput.fileinput-new.text-center').addClass('fileinput-exists').removeClass('fileinput-new');
			$('#txt_category').closest('div').removeClass('is-empty').addClass('is-focused');
			
			$('#foodCategoryModal').modal('show');
			$('#add_edit_cate').val('1');
			$('#save-foodCategory').val('Edit');
			var i_type = "";
			switch(data1[1]) {
				case "Food":
				  i_type="F";
				  break;
				case "Drink":
				  i_type="D";
				  break;
				default:
				  // code block
			  }
			$('select[name=txt_type]').val(i_type);
			$('#txt_category').val(data1[2]);
			
			$('.fileinput-preview.fileinput-exists.thumbnail').html(img);
			$('.fileinput-filename').val(replace);
			$('#txt_id').val(data1[0]);
			$('select[name=txt_status]').val(data1[4]);
			$('.selectpicker').selectpicker('refresh');
		}
	});
}
function food_modal_add(){
	$.ajax({
		url:'action/food-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#foodModal').modal('show');
			$('#add_edit').val('0');
			$('#save-food').val('ADD');
			$('#txt_product').val('');
			$('.fileinput').fileinput('clear');
			
		}
	});
}
function food_modal_edit(data1,img,replace,category_id){
	$.ajax({
		url:'action/food-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.fileinput.fileinput-new.text-center').addClass('fileinput-exists').removeClass('fileinput-new');
			$('#txt_category').closest('div').removeClass('is-empty').addClass('is-focused');
			
			$('#foodModal').modal('show');
			$('#add_edit_prod').val('1');
			$('#save-food').val('Edit');
			$('#category').val(category_id);
			$('#txt_food').val(data1[2]);
			$('#txt_price').val(data1[3]);
			$('.fileinput-preview.fileinput-exists.thumbnail').html(img);
			$('.fileinput-filename').val(replace);
			$('#txt_id').val(data1[0]);
			$('select[name=txt_status]').val(data1[5]);
			$('.selectpicker').selectpicker('refresh');
		}
	});
}
function ingredient_modal_add(){
	$.ajax({
		url:'action/ingredient-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#ingredientModal').modal('show');
			$('#add_edit').val('0');
			$('#save-ingredient').val('ADD');
			$('#txt_product').val('');
			$('.fileinput').fileinput('clear');
			
		}
	});
}
function ingredient_modal_edit(data1,img,replace,scale_id){
	$.ajax({
		url:'action/ingredient-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.fileinput.fileinput-new.text-center').addClass('fileinput-exists').removeClass('fileinput-new');
			
			$('#ingredientModal').modal('show');
			$('#add_edit_prod').val('1');
			$('#save-ingredient').val('Edit');
			$('#txt_scale').val(scale_id);
			$('#txt_ingredient').val(data1[1]);
			$('#txt_cost').val(data1[2]);
			$('.fileinput-preview.fileinput-exists.thumbnail').html(img);
			$('.fileinput-filename').val(replace);
			$('#txt_id').val(data1[0]);
			$('select[name=txt_status]').val(data1[5]);
			$('.selectpicker').selectpicker('refresh');
		}
	});
}
function ingredientBuy_modal_add(){
	$.ajax({
		url:'action/ingredientBuy-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#inStockModal').modal('show');
			$('#add_edit').val('0');
			$('#save-stock').val('ADD');
			$('.fileinput').fileinput('clear');
			
		}
	});
}
function desk_modal_add(){
	$.ajax({
		url:'action/desk-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#deskModal').modal('show');
			$('#add_edit_prod').val('0');
			$('#save-desk').val('ADD');
			//$('.fileinput').fileinput('clear');
			
		}
	});
}
function desk_modal_edit(data1){
	$.ajax({
		url:'action/desk-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			//$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#deskModal').modal('show');
			$('#add_edit_prod').val('1');
			$('#save-desk').val('Edit');
			$('#txt_id').val(data1[0]);
			$('#txt_deskName').val(data1[1]);
			$('select[name=txt_status]').val(data1[2]);
			$('.selectpicker').selectpicker('refresh');
			//$('.fileinput').fileinput('clear');
			
		}
	});
}
function user_modal_add(){
	$.ajax({
		url:'action/user-modal.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.block-modal').html(data);
			$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#userModal').modal('show');
			$('#add_edit_prod').val('0');
			$('#save-user').val('ADD');
			//$('.fileinput').fileinput('clear');
			
		}
	});
}
function user_modal_edit(id){

	$.ajax({
		url:'action/user_info.php',
		type: "POST",             // Type of request to be send, called as method
		data: {userid:id}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		//contentType: false,       // The content type used when sending data to the server.
		//cache: false,             // To unable request pages to be cached
		//processData:false,        // To send DOMDocument or non processed data file it is set to false
		dataType: 'json',
		success: function(data){
			var user_id=id;
			var employee_id=data[1].employee_id;
			var role=data[1].role;
			var user_name=data[1].user_name;
			var user_create=data[1].user_create;
			var status=data[1].status;
			$.ajax({
				url:'action/user-modal.php',
				type: "POST",             // Type of request to be send, called as method
				data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data2){
					$('.block-modal').html(data2);
					//$('.label-floating').addClass('is-empty').removeClass('is-focused');
					$('#userModal').modal('show');
					$('#add_edit_prod').val('1');
					$('#save-user').val('Edit');
					$('#txt_id').val(user_id);
					$('select[name=employee]').val(employee_id);
					$('select[name=role]').val(role);
					$('select[name=user_create]').val(user_create);
					$('#txt_username').val(user_name);
					$('select[name=txt_status]').val(data.status);
					$('.selectpicker').selectpicker('refresh');
					//$('.fileinput').fileinput('clear');
					
				}
			});
		}
	});
	
}
function order_dashboard(id){
	$.ajax({
		url:'action/order_info.php',
		type: "POST",             // Type of request to be send, called as method
		data: {order_id:id}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		//contentType: false,       // The content type used when sending data to the server.
		//cache: false,             // To unable request pages to be cached
		//processData:false,        // To send DOMDocument or non processed data file it is set to false
		//dataType: 'json',
		success: function(data){
			$('.block-modal').html(data);
			$('.label-floating').addClass('is-empty').removeClass('is-focused');
			$('#orderModal').modal('show');
			$('#add_edit_prod').val('1');
		}
	});
	
}