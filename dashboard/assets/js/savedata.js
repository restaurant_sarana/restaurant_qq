// JavaScript Document
function save_category(frm_data){
	$.ajax({
		url:'action/save-category.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('#txt_category').val('');
			$('.fileinput').fileinput('clear');
			fetch_category();
			$('#categoryModal').modal('hide');
		}
	});
}
function save_product(frm_data){
	$.ajax({
		url:'action/save-product.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('#txt_product').val('');
			$('txt_cost').val('');
			$('txt_price').val('');
			$('.fileinput').fileinput('clear');
			fetch_product();
			$('#productModal').modal('hide');
		}
	});
}
function save_customer(frm_data){
	$.ajax({
		url:'action/save-customer.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			alert(data);
			$('.fileinput').fileinput('clear');
			fetch_customer();
			$('#customerModal').modal('hide');
		}
	});
}
function save_position(frm_data){
	$.ajax({
		url:'action/save-position.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.fileinput').fileinput('clear');
			fetch_position();
			$('#positionModal').modal('hide');
		}
	});
}
function save_carBranch(frm_data){
	$.ajax({
		url:'action/save-carBranch.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.fileinput').fileinput('clear');
			fetch_carBranch();
			$('#carBranchModal').modal('hide');
		}
	});
}
function save_employee(frm_data){
	$.ajax({
		url:'action/save-employee.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.fileinput').fileinput('clear');
			fetch_employee();
			$('#employeeModal').modal('hide');
		}
	});
}
function save_stock(frm_data){
	$.ajax({
		url:'action/save-stock.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			alert(data);
			$('.fileinput').fileinput('clear');
			fetch_inStock();
			$('#inStockModal').modal('hide');
		}
	});
}
function save_transfer(frm_data){
	$.ajax({
		url:'action/save-transfer.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			alert(data);
			$('.fileinput').fileinput('clear');
			assign_qtyproduct();
			$('#transferModal').modal('hide');
		}
	});
}
function save_supplierCompany(frm_data){
	$.ajax({
		url:'action/save-supplierCompany.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			alert(data);
			$('.fileinput').fileinput('clear');
			fetch_supplierCompany();
			$('#CompanyModal').modal('hide');
		}
	});
}
function save_supplier(frm_data){
	$.ajax({
		url:'action/save-supplier.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			alert(data);
			$('.fileinput').fileinput('clear');
			fetch_supplier();
			$('#supplierModal').modal('hide');
		}
	});
}
function save_foodCategory(frm_data){
	$.ajax({
		url:'action/save-foodCategory.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('#txt_category').val('');
			$('.fileinput').fileinput('clear');
			fetch_foodCategory();
			$('#foodCategoryModal').modal('hide');
		}
	});
}
function save_food(frm_data){
	$.ajax({
		url:'action/save-food.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.fileinput').fileinput('clear');
			fetch_food();
			$('#foodModal').modal('hide');
		}
	});
}
function save_ingredient(frm_data){
	$.ajax({
		url:'action/save-ingredients.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.fileinput').fileinput('clear');
			fetch_foodIngredient();
			$('#ingredientModal').modal('hide');
		}
	});
}
function save_ingrdientBuy(frm_data){
	$.ajax({
		url:'action/buy-ingrediants.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			alert(data);
			fetch_ingredientBuy();
			$('#inStockModal').modal('hide');
		}
	});
}
function save_desk(frm_data){
	$.ajax({
		url:'action/save-desk.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.fileinput').fileinput('clear');
			fetch_desk();
			$('#deskModal').modal('hide');
		}
	});
}
function save_user(frm_data){
	$.ajax({
		url:'action/save-user.php',
		type: "POST",             // Type of request to be send, called as method
		data: frm_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.fileinput').fileinput('clear');
			fetch_user();
			$('#userModal').modal('hide');
		}
	});
}