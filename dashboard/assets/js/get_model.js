// JavaScript Document

function get_product(cate_id,tr){
	$.ajax({
		url:'action/get_product.php',
		type: "POST",             // Type of request to be send, called as method
		data: {category:cate_id}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		//contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		//processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('#'+tr+'').find('#product').html(data);
			$(".selectpicker").selectpicker("refresh");	
		}
	});
}
function get_product_edit(cate_id,product_id,tr){
	$.ajax({
		url:'action/get_product.php',
		type: "POST",             // Type of request to be send, called as method
		data: {category:cate_id}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		//contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		//processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('#'+tr+'').find('#product').html(data);
			$('#'+tr+'').find('#product').val(product_id);
			$(".selectpicker").selectpicker("refresh");	
		}
	});
}
function get_product_detail(product_id,tr){
	$.ajax({
		url:'action/get_product_detail.php',
		type: "POST",             // Type of request to be send, called as method
		data: {product:product_id}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		//contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		//processData:false,        // To send DOMDocument or non processed data file it is set to false
		dataType: 'json',
		success: function(data){
			$('#'+tr+'').find('.txt_cost').val(data.cost);
			var qty=$('#'+tr+'').find('.txt_qty').val();
			var total=qty*data.cost;
			$('#'+tr+'').find('.txt_total').val(total);
			var grandCostTotal=0;
			$('.txt_total').each(function(){
				var stval = parseFloat($(this).val());
        		grandCostTotal += isNaN(stval) ? 0 : stval;
			});
			$('.txt_cost_total').val(grandCostTotal.toFixed(2));
			var grandQtyTotal=0;
			$('.txt_qty').each(function(){
				var stval = parseFloat($(this).val());
        		grandQtyTotal += isNaN(stval) ? 0 : stval;
			});
			$('.txt_qty_total').val(grandQtyTotal.toFixed(0));
			
		}
	});
}
function get_row_stock(tr){
	$.ajax({
		url:'action/get_row_stock.php',
		type: "POST",             // Type of request to be send, called as method
		data: {tr:tr}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		//contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		//processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('#table_body').append(data);
			$(".selectpicker").selectpicker("refresh");	
		}
	});
}
function get_row_ingre(tr){
	$.ajax({
		url:'action/get_row_ingre.php',
		type: "POST",             // Type of request to be send, called as method
		data: {tr:tr}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		//contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		//processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('#table_body').append(data);
			$(".selectpicker").selectpicker("refresh");
		}
	});
}
function qty_changed(tr){
	var cost=$('#'+tr+'').find('.txt_cost').val();
	var qty=$('#'+tr+'').find('.txt_qty').val();
	var total=cost*qty;
	$('#'+tr+'').find('.txt_total').val(total);
	var grandCostTotal=0;
	$('.txt_total').each(function(){
		var stval = parseFloat($(this).val());
       	grandCostTotal += isNaN(stval) ? 0 : stval;
	});
	$('.txt_cost_total').val(grandCostTotal.toFixed(2));
	var grandQtyTotal=0;
	$('.txt_qty').each(function(){
		var stval = parseFloat($(this).val());
       	grandQtyTotal += isNaN(stval) ? 0 : stval;
	});
	$('.txt_qty_total').val(grandQtyTotal.toFixed(0));
}