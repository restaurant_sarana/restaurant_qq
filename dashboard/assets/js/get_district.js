// JavaScript Document

function get_district(province_id){
	$.ajax({
		url:'action/get_district.php',
		type: "POST",             // Type of request to be send, called as method
		data: {province:province_id}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		//contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		//processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('body').find('#district').html(data);
			$(".selectpicker").selectpicker("refresh");	
		}
	});
}
function get_district_edit(province_id,district){
	$.ajax({
		url:'action/get_district.php',
		type: "POST",             // Type of request to be send, called as method
		data: {province:province_id}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		//contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		//processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('body').find('#district').html(data);
			$('body').find('#district').val(district);
			$(".selectpicker").selectpicker("refresh");	
		}
	});
}