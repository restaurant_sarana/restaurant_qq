// JavaScript Document

function fetch_category(){
	$.ajax({
		url:'action/fetch_category.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_product(){
	$.ajax({
		url:'action/fetch_product.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_customer(){
	$.ajax({
		url:'action/fetch_customer.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_position(){
	$.ajax({
		url:'action/fetch_position.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_carBranch(){
	$.ajax({
		url:'action/fetch_carBranch.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_employee(){
	$.ajax({
		url:'action/fetch_employee.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_inStock(){
	$.ajax({
		url:'action/fetch_inStock.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function assign_qtyproduct(){
	$.ajax({
		url:'action/assign_qtyproduct.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function assign_product(){
	$.ajax({
		url:'action/assign_product.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_supplier(){
	$.ajax({
		url:'action/fetch_supplier.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_supplierCompany(){
	$.ajax({
		url:'action/fetch_supplierCompany.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_foodCategory(){
	$.ajax({
		url:'action/fetch_foodCategory.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_food(){
	$.ajax({
		url:'action/fetch_food.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_foodIngredient(){
	$.ajax({
		url:'action/fetch_foodIngredient.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_ingredientBuy(){
	$.ajax({
		url:'action/fetch_ingredientBuy.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_desk(){
	$.ajax({
		url:'action/fetch_desk.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_inventory(){
	$.ajax({
		url:'action/fetch_product.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_inventoryIn(){
	$.ajax({
		url:'action/fetch_inStock.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_inventoryDamage(){
	$.ajax({
		url:'action/fetch_inStock.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_user(){
	$.ajax({
		url:'action/fetch_user.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}
function fetch_dashboard(){
	$.ajax({
		url:'action/fetch_dashboard.php',
		type: "POST",             // Type of request to be send, called as method
		data: {name:1}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData:false,        // To send DOMDocument or non processed data file it is set to false
		success: function(data){
			$('.content').html(data);
			
		}
	});
}