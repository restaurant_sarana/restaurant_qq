<?php
include("dashboard/action/connect.php");
session_start();
if(!isset($_SESSION['user'])){
	header('Location: login.php');
}
else{

$sql="SELECT * FROM desk WHERE status=1";
$result=$conn->query($sql);

?>

<!doctype html>
<html lang="en">


<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Mar 2017 21:29:18 GMT -->
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Resturant Manangerment</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="assets/css/material-dashboard.css" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
	<link href="assets/css/normalize.css" rel="stylesheet" />
    <!-- <link href="assets/css/skeleton.css" rel="stylesheet" /> -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/google-roboto-300-700.css" rel="stylesheet" />
	<link href="assets/css/home.css" rel="stylesheet"/>
	<link href="assets/css/simplePagination.css" rel="stylesheet"/>
	<link href="assets/css/jquery.bootstrap-touchspin.css" rel="stylesheet"/>
	<link href="assets/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet"/>
    
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-active-color="rose" data-background-color="black" data-image="../assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
        Tip 2: you can also add an image using data-image tag
        Tip 3: you can change the color of the sidebar with data-background-color="white | black"
    -->		<button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon btn-slider">
				<i class="material-icons visible-on-sidebar-regular">view_module</i>
            </button>
            <div class="logo">
                <a href="#" class="simple-text">
                    Restaurants
                </a>
            </div>
            <div class="logo logo-mini">
                <a href="#" class="simple-text">
					R
                </a>
            </div>
            <div class="sidebar-wrapper" id="slider-desk">
				<div class="row">
					
				<?php
				while($row=$result->fetch_array()){
					if($row[2]==="B")
					{
					$type='active-Book';
					}
					elseif ($row[2]==="U") {
					$type='active-Using';
					}
					else {
					$type="";
					}
					echo '<div class="col-md-12 user" id="'.$row[0].'">
						<div class="photo reserve-border '.$type.'" data-tablestatus="'.$row[2].'">
							<p class="reserve-table">'.$row[1].'</p>
						</div>
					</div>';
				}

				?>
				</div>
			</div>
			
		</div>
		<div class="full-background"></div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                            <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                            <i class="material-icons visible-on-sidebar-mini">view_list</i>
                        </button>
                    </div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Order Food </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
							<li>
								<a href="dashboard/dashboard.php">
									<i class="material-icons">dashboard</i> Dashboard
								</a>
							</li>
                            <li class="dropdown" id="notification-icon">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <p class="hidden-lg hidden-md">
                                        Notifications
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <ul class="dropdown-menu notification-details">
                                    
                                </ul>
                            </li>

                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
										<a class="dropdown-item" href="#" id="logout">Log Out</a>
									</div>
                                </a>
                            </li>
                            <li class="separator hidden-lg hidden-md"></li>
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group form-search is-empty">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form>
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
						<div class="col-md-8 select_order_panel">

								<div class="card">

									<div class="card-content">
										<div class="row  tags-menu">
											<div class="col-md-12 menuList">
												<ul>
													<li class="active"><a href="#" data-category="0" data-realcategory="Food">ទាំងអស់</a></li>
													<?php
													$sql_category="SELECT * FROM food_category WHERE status=1 AND type='F'";
													$result_category=$conn->query($sql_category);
													while($row=$result_category->fetch_array()){
														echo '<li><a href="#" data-category="'.$row[0].'" data-realcategory="Food">'.$row[2].'</a></li>';
													}

													?>

												</ul>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="row"  id="pageList">

													<?php
													$sql_food="SELECT * FROM food_items WHERE status=1 LIMIT 0,6";
													$result_food=$conn->query($sql_food);
													while($row=$result_food->fetch_array()){
														echo '<div class="col-lg-4 col-md-4">
														<div class="card" id="'.$row[0].'" data-realcategory="Food">
															<img src="dashboard/img/'.$row[4].'" class="img-fluid" style="height: 10vh;">
															<div class="card-body">
																<p class="card-text-title">'.$row[2].'</p>
																<p class="card-text">'.$row[3].' ៛</p>
																<div style="position:relative;" class="add-product">
																	<button type="button" disabled class="btn btn-primary" style="margin: auto;position: absolute;top:0;left:50%;transform : translate(-50%,0%)">Add to Cart</button>
																</div>
															</div>
														</div>
													</div>';
													}
													?>

												</div>

												<div class="col-md-12">
													<div id="pagination-container" style="float: right"></div>
												</div>
											</div>
										</div>

									</div>
								</div>
								<div class="card">
									<div class="card-content">
										<div class="row tags-menu">
											<div class="col-md-12 menuList-drink">
												<ul>
													<li class="active"><a href="#" data-category="0" data-realcategory="Drink">ទាំងអស់</a></li>
													<?php

													$sql_drink_category="SELECT * FROM food_category WHERE status=1 AND type='D'";
													$result_drink_category=$conn->query($sql_drink_category);
													while($row=$result_drink_category->fetch_array()){
														echo '<li><a href="#" data-category="'.$row[0].'" data-realcategory="Drink">'.$row[2].'</a></li>';
													}
													?>

												</ul>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="row"  id="pageList-drink">

													<?php
													$sql_drink="SELECT * FROM soft_drink WHERE status=1 LIMIT 0,6";
													$result_drink=$conn->query($sql_drink);
													while($row=$result_drink->fetch_array()){
														echo '<div class="col-md-4">
														<div class="card" id="'.$row[0].'" data-realcategory="Drink">
															<img src="dashboard/img/'.$row[6].'" class="img-fluid" style="height: 10vh;">
															<div class="card-body">
																<p class="card-text-title">'.$row[2].'</p>
																<p class="card-text">'.$row[4].' ៛</p>
																<div style="position:relative;" class="add-product">
																	<button type="button" disabled class="btn btn-primary" style="margin: auto;position: absolute;top:0;left:50%;transform : translate(-50%,0%)">Add to Cart</button>
																</div>

															</div>
														</div>
													</div>';
													}
													?>

												</div>

												<div class="col-md-12">
													<div id="pagination-container-drink" style="float: right"></div>
												</div>
											</div>
										</div>

									</div>
								</div>
                        </div>
						<div class="col-md-4 order_panel">
							<div class="row">
								<div class="col-sm-12">
									<div class="card">

										<div class="card-content" style="padding-top: 0px;">

											<div class="row">
												<div class="col-sm-12">
													<div class="row">
														<div class="col-sm-4" style="padding-top:30px;text-align:right;">
															<b class="Small Tag">Stand Table :</b>
														</div>
														<div class="col-sm-8">
															<select class="form-control showtable_index">

																<option value="Home">Please Select Table!</option>
																<?php
																$sql="SELECT * FROM desk WHERE status=1";
																$result=$conn->query($sql);
																while($row1=$result->fetch_array()){
																	
																	echo '<option value="'.$row1[0].'">'.$row1[1].'</option>';
																}

																?>
															</select>
														</div>
													</div>
													<div class="table-responsive table-sales" data-order_id="0" style="padding:0;margin:0;">
														<table class="table table-food" style="font-size:12px;table-layout: fixed; width: 100%;">
															<tbody>
																<tr>
																	<th>Food</th>
																	<th>Size</th>
																	<th class="text-right">Price</th>
																	<th colspan="2">Qty</th>
																	<th align="left"></th>
																</tr>

															</tbody>

														</table>

													</div>
													<div class="table-responsive table-sales" style="padding:0;margin:0;">
														<table class="table table-drink" style="font-size:12px;table-layout: fixed; width: 100%;">
															<tbody>
																<tr>
																	<th>Drink</th>
																	<th>Size</th>
																	<th class="text-right">Price</th>
																	<th colspan="2">Qty</th>
																	<th align="left"></th>
																</tr>

															</tbody>

														</table>

													</div>
													<div class="table-responsive table-sales">
														<table class="table"  width="90%" style="font-size:12px">
															<tbody>

																<tr>
																	<th>
																		Sub Total:
																	</th>
																	<th  id="subTotal">0</th>
																	<th>
																		Discount%:
																	</th>
																	<th contenteditable="false" class="number_key" id="discount">0</th>
																</tr>
																<!-- <tr>
																	<th>
																		Grand Total(៛):
																	</th>
																	<th id="grandTotalR">100000</th>
																	<th>
																		Grand Total($):
																	</th>
																	<th id="grandTotalD">25</th>

																</tr> -->


															</tbody>

														</table>
													</div>
												</div>

											</div>
										</div>
                            		</div>
								</div>
								<div class="col-md-12" >
									<div class="col-lg-3 col-sm-6">
										<button class="btn btn-primary btn-round btn-sm" id="cancel_order">Cancel Order</button>
									</div>
									<div class="col-lg-3 col-sm-6">
										<button class="btn btn-primary btn-round btn-sm" id="modify_btn">Modify</button>
									</div>
									<div class="col-lg-3 col-sm-6">
										<button class="btn btn-primary btn-round btn-sm" id="modify_save" disabled="disabled">Save</button>
									</div>
									<div class="col-lg-3 col-sm-6">
										<button class="btn btn-primary btn-round btn-sm" id="modify_cancel" disabled="disabled">Cancel</button>
									</div>
									
								</div>
								<div class="col-md-12">

											<div class="row">
												<div class="col-lg-4 col-sm-6">
													<div class="card" style="background-color: gray;">
                            <a href="#" id="order" class="action_click">
  														<div class="card-content" style="text-align: center; font-size: 20px; color: ghostwhite; font-weight: 500;">
  															<i class="material-icons">send</i>
  															<p>Order</p>
  														</div>
                            </a>
													</div>
												</div>
												<div class="col-lg-4 col-sm-6">
													<div class="card" style="background-color: dodgerblue;">
                            <a href="#" id="invoice" class="action_click" disabled>
  														<div class="card-content" style="text-align: center; font-size: 20px; color: ghostwhite; font-weight: 500;">
  															<i class="material-icons">receipt</i>
  															<p>Invoice</p>
  														</div>
                            </a>
													</div>
												</div>
												<div class="col-lg-4 col-sm-6">
													<div class="card" style="background-color: green;">
                            <a href="#" id="paid_cash" class="action_click" disabled>
  														<div class="card-content" style="text-align: center; font-size: 20px; color: ghostwhite; font-weight: 500;">
  															<i class="material-icons">attach_money</i>
  															<p>Paid Case</p>
  														</div>
                            </a>
													</div>
												</div>
												<div class="col-lg-4 col-sm-6">
                          <a href="#" id="paid_credit" class="action_click">
  													<div class="card" style="background-color: dodgerblue;">
  														<div class="card-content" style="text-align: center; font-size: 20px; color: ghostwhite; font-weight: 500;">
  															<i class="material-icons">credit_card</i>
  															<p>Paid Credit</p>
  														</div>
                            </a>
													</div>
												</div>
											</div>

								</div>
							</div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
	<div class="ModalBox">
		
	</div>
</body>
<!--   Core JS Files   -->
<script src="assets/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/material.min.js" type="text/javascript"></script>
<script src="assets/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="assets/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="assets/js/moment.min.js"></script>
<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="assets/js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>
<!--   Sharrre Library    -->
<script src="assets/js/jquery.sharrre.js"></script>
<!-- DateTimePicker Plugin -->
<script src="assets/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="assets/js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="assets/js/nouislider.min.js"></script>
<!--  Google Maps Plugin    -->
<!--<script src="../assets/js/jquery.select-bootstrap.js"></script>-->
<!-- Select Plugin -->
<script src="assets/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="assets/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="assets/js/sweetalert2.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="assets/js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="assets/js/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="assets/js/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="assets/js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

<script src="assets/js/jquery.simplePagination.js"></script>

<script src="assets/js/jquery.bootstrap-touchspin.js"></script>

<script src="assets/js/jquery.bootstrap-touchspin.min.js"></script>

<script src="assets/js/printThis.js"></script>

<script src="assets/scripts/JSPrintManager.js"></script>
<script src="assets/scripts/zip.js"></script>
<script src="assets/scripts/zip-ext.js"></script>
<script src="assets/scripts/deflate.js"></script>

<script type="text/javascript">

    $(document).ready(function() {
		var foodCount;
		var prevoisNumber = 1;
		var rowTable=1;
		var blog="";
    	var desk="";
		var tempdesk="";
		var order_id=$('.table-sales').data('order_id');

		function load_unseen_notification(view = '')
		{
			$.ajax({
				url:"fetch_notification.php",
				method:"POST",
				data:{view:view},
				dataType:"json",
				success:function(data)
				{
					$('.notification-details').html(data.notification);
					if(data.unseen_notification > 0)
					{
						if($('.notification').length>0){

						}
						else{
							$('#notification-icon .dropdown-toggle').append('<span class="notification"></span>');
						}
						$('.notification').html(data.unseen_notification);
					}
				}
			});
		}
		load_unseen_notification();
		$(document).on('click', '#notification-icon .dropdown-toggle', function(){
			$('.notification').html('');
			$('.notification').remove();
			load_unseen_notification('yes');
		});
		setInterval(function(){ 
  			load_unseen_notification();
		}, 1000);
		function getOrdered(desk){
				$(".table-sales .table-food tbody").html('<tr><th>Food</th><th>Size</th><th class="text-right">Price</th><th colspan="2">Qty</th><th align="left"></th></tr>');
				$(".table-sales .table-drink tbody").html('<tr><th>Drink</th><th>Size</th><th class="text-right">Price</th><th colspan="2">Qty</th><th align="left"></th></tr>');
				

				$('#cancel_order').prop("disabled", false);
				$('#modify_btn').prop("disabled", false);
				$('.add-product button').prop("disabled", true);
				$('#paid_case').prop("disabled", true);
				$.ajax({
				url:'fetch_ordered.php',
				method:'post',
				data:{desk:desk},
				dataType:'json',
				success:function(data){
					var indexfood=1;
					var indexdrink=1;
					var total=0;
					var discount=data.discount[0];
					order_id=data.order;
					for(i=0;i<data.i;i++){

						if(data.itemtype[i]=='F'){
							var foodid=data.itemsid[i];
							var food=data.items[i];
							var foodsize=data.itemssize[i];
							var foodqty=data.itemsqty[i];
							var foodprice=data.itemsprice[i];
							var total_food=data.total[i];
							total+=parseInt(total_food);
							

							var row="<tr id="+foodid+" class='itemsfoodid'>";
							row +="<td>";
							row +=food;
							row +="</td>";
							row +="<td>";
							row +="<select class='itemsfoodsize' id='option_selected_f"+foodid+"' value='"+foodsize+"'>";
							row +="<option value='M'>M</option>";
							row +="<option value='L'>L</option>";
							row +="</select>";
							row +="</td>";

							row +="<td class='text-right itemsfoodprice'>";
							row +=foodprice;
							row +="</td>";
							row +="<td class='text-right itemsfoodqty'>";
							row +=foodqty;
							row +="</td>";
							row +="<td>";
							row +='<div style="float: right;">';
							// row +='<input type="button" value="+" name="plus" class="plus" style="width: 10px; height: 10px; margin: 0;padding: 0;"><br>';
							// row +='<input type="button" value="-" name="minus" class="minus" style="width: 10px; height: 10px; margin: 0;padding: 0;">';
							row +='<button name="plus" style="margin:0;padding:0;" class="btn btn-primary btn btn-just-icon btn-simple plus"><i class="material-icons">add</i></button><br>';
							row +='<button name="minus" style="margin:0;padding:0;" class="btn btn-primary btn btn-just-icon btn-simple minus"><i class="material-icons">remove</i></button>';
							row +='</div>';
							row +="</td>";
							row +="<td class='text-left'>";
							row +='<button class="btn btn-primary btn btn-just-icon btn-simple clearrow">';
							row +='<i class="material-icons">delete</i>';
							row +='</button>';
							row +="</td>";
							row +="</tr>";
							$(".table-sales .table-food tbody:last").append(row);
							indexfood=parseInt(indexfood)+1;
							$('#option_selected_f'+foodid+'').val(foodsize);
						}
						else{
							var drinkid=data.itemsid[i];
							var drink=data.items[i];
							var drinksize=data.itemssize[i];
							var drinkqty=data.itemsqty[i];
							var drinkprice=data.itemsprice[i];
							var total_drink=data.total[i];
							total+=parseInt(total_drink);

							var row="<tr id="+drinkid+" class='itemsdrinkid'>";
							row +="<td>";
							row +=drink;
							row +="</td>";
							row +="<td>";
							row +="<select class='itemsdrinksize' id='option_selected_d"+drinkid+"' value='"+drinksize+"'>";
							row +="<option value='M'>M</option>";
							row +="<option value='L'>L</option>";
							row +="</select>";
							row +="</td>";

							row +="<td class='text-right itemsdrinkprice'>";
							row +=drinkprice;
							row +="</td>";
							row +="<td class='text-right itemsdrinkqty'>";
							row +=drinkqty;
							row +="</td>";
							row +="<td>";
							row +='<div style="float: right;">';
							// row +='<input type="button" value="+" name="plus" class="plus" style="width: 10px; height: 10px; margin: 0;padding: 0;"><br>';
							// row +='<input type="button" value="-" name="minus" class="minus" style="width: 10px; height: 10px; margin: 0;padding: 0;">';
							row +='<button name="plus" style="margin:0;padding:0;" class="btn btn-primary btn btn-just-icon btn-simple plus"><i class="material-icons">add</i></button><br>';
							row +='<button name="minus" style="margin:0;padding:0;" class="btn btn-primary btn btn-just-icon btn-simple minus"><i class="material-icons">remove</i></button>';
							row +='</div>';
							row +="</td>";
							row +="<td class='text-left'>";
							row +='<button class="btn btn-primary btn btn-just-icon btn-simple clearrow">';
							row +='<i class="material-icons">delete</i>';
							row +='</button>';
							row +="</td>";
							row +="</tr>";
							$(".table-sales .table-drink tbody:last").append(row);
							indexdrink=parseInt(indexdrink)+1;
							$('#option_selected_d'+drinkid+'').val(drinksize);

							}
						}
						$('#discount').text(discount);
						var subTotal=$('#subTotal');
						subTotal.text(total);
					}
					
				});
		}
		function deskload(){
			$.ajax({
				url:'fetch_desk.php',
				method:'post',
				data:{desk:desk},
				success: function(data){
					$('#slider-desk').html(data);
				}
			});
		}
		function indexPage(prevoisNumber,category,blogs){
			$.ajax({
				url:'pageList.php',
				type: "POST",             // Type of request to be send, called as method
				data: { pageNumber : prevoisNumber,category : category,blogs: blogs}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				//contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				//processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data){

					if(blogs=="Food"){
						$('#pageList').html(data);

					}
					else if(blogs=="Drink"){
						$('#pageList-drink').html(data);
					}


				}
			});
		}
		function getItems(category,blogs){
			if(blogs=="Food"){
				$.ajax({
					url:'getItems.php',
					type: "POST",             // Type of request to be send, called as method
					data: {category:category}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					//contentType: false,       // The content type used when sending data to the server.
					cache: false,             // To unable request pages to be cached
					//processData:false,        // To send DOMDocument or non processed data file it is set to false
					dataType: 'json',
					success: function(data){

						$("#pagination-container").pagination({
							items: data.food,
							itemsOnPage: 6,
							cssStyle: 'compact-theme',
							onPageClick: function(pages,event){
								blog = $('.menuList ul .active a').data('realcategory');
								var activeRole = $('.menuList ul .active a').data('category');
								indexPage(pages,activeRole,blog);
							}
						});

					}

				});
			}
			else if(blogs=="Drink"){
				$.ajax({
					url:'getItems_drink.php',
					type: "POST",             // Type of request to be send, called as method
					data: {category:category}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					//contentType: false,       // The content type used when sending data to the server.
					cache: false,             // To unable request pages to be cached
					//processData:false,        // To send DOMDocument or non processed data file it is set to false
					dataType: 'json',
					success: function(data){

						$("#pagination-container-drink").pagination({
							items: data.drink,
							itemsOnPage: 6,
							cssStyle: 'compact-theme',
							onPageClick: function(pages,event){
								blog = $('.menuList-drink ul .active a').data('realcategory');
								var activeRole = $('.menuList-drink ul .active a').data('category');
								indexPage(pages,activeRole,blog);
							}
						});

					}

				});
			}

		}
		// THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
		function isNumber(evt, element) {

		var charCode = (evt.which) ? evt.which : event.keyCode

		if (
			(charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
			(charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
			(charCode < 48 || charCode > 57))
			return false;

		return true;
		}

		getItems(0,"Food");
		getItems(0,"Drink");
		$(document).on('keypress','.number_key',function(event) {
		//$(".number_key").keypress(function (event) {
			return isNumber(event, this);
   		});
		
		$(document).on("click","#invoice_prt",function(){
			print(desk);
		});
		$(document).on("click","#receipt_prt",function(){
			var orderid=$('#orderNo').val();
			// if (jspmWSStatus()) {
            //Create a ClientPrintJob
            var cpj = new JSPM.ClientPrintJob();
            //Set Printer type (Refer to the help, there many of them!)
            cpj.clientPrinter = new JSPM.DefaultPrinter();
            //Set content to print...
            //Create ESP/POS commands for sample label
            var esc = '\x1B'; //ESC byte in hex notation
            var newLine = '\x0A'; //LF byte in hex notation

			$.ajax({
				url:"fetch_payment.php",
				method:"POST",
				data:{order_id:orderid},
				dataType:"json",
				success:function(data){
					var itemsorder=data.order[0];
					var cmds = 'Receipt'; //text to print
					cmds += newLine + newLine;
					cmds += 'Restaurant'; //text to print
            		cmds += newLine + newLine;
					cmds += 'Order:'+itemsorder; //text to print
					cmds += newLine + newLine;
					cmds += 'Date:'+data.payment_date[0];
					cmds += newLine + newLine;
					var total_price=[];
					for(var i=0;i<data.items.length;i++){
						
						total_price.push(data.itemsqty[i] * data.itemsprice[i]);
						var itemsLength=data.items[i].length;
						var total=parseInt(data.itemsqty[i])*parseInt(data.itemsprice[i]);
						var priceLength=data.itemsprice[i].length;
						var replaceDot=40-parseInt(itemsLength)-parseInt(priceLength);
						var dotJ='';
						for(var j=0;j<replaceDot;j++){
							dotJ+=".";
						}
						cmds += data.items[i]+dotJ+total+"៛";
						cmds += newLine;
					}
					var grandTotal = 0;
					for (var i = 0; i < total_price.length; i++) {
						grandTotal += total_price[i] << 0;
					}
					cmds += newLine + newLine;
					cmds += 'SUBTOTAL.................'+grandTotal+'៛';
					cmds += newLine;
					cmds += 'Discount%................'+data.discount[0]+'៛';
					cmds += newLine;
					cmds += 'TOTAL....................'+grandTotal+'៛';
					cmds += newLine;
					cmds += 'Receive..................'+data.payment_receive[0]+'៛';
					cmds += newLine;
					cmds += 'Changed..................'+data.payment_change[0]+'៛';
					cmds += newLine + newLine;
					
					cmds += 'Thank you!';
					cmds += newLine + newLine;

                    var my_file1 = new JSPM.PrintFileTXT(cmds,'MyFile.txt',1);
                    my_file1.fontBold=1;
                    my_file1.fontSize=13;
                    cpj.files.push(my_file1);
                    var cpjg = new JSPM.ClientPrintJobGroup();
                    cpjg.jobs.push(cpj);
                    cpjg.sendToClient();
				}
			});
            
        // }
		});
		$(document).on("click",".tags-menu ul li",function(){
			var eThis = $(this);
			eThis.addClass("active").siblings().removeClass("active");
			var category = eThis.children().data("category");
			blog = eThis.children().data("realcategory");
			//alert(blog);
			prevoisNumber = 1;
			if(blog=="Food"){
				getItems(category,"Food");
				$.ajax({
					url:'foodCategory.php',
					type: "POST",             // Type of request to be send, called as method
					data: { category : category}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					//contentType: false,       // The content type used when sending data to the server.
					cache: false,             // To unable request pages to be cached
					//processData:false,        // To send DOMDocument or non processed data file it is set to false
					success: function(data){
						$('#pageList').html(data);

					}
				});
			}
			else if(blog=="Drink"){
				getItems(category,"Drink");
				$.ajax({
					url:'drinkCategory.php',
					type: "POST",             // Type of request to be send, called as method
					data: { category : category}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					//contentType: false,       // The content type used when sending data to the server.
					cache: false,             // To unable request pages to be cached
					//processData:false,        // To send DOMDocument or non processed data file it is set to false
					success: function(data){
						$('#pageList-drink').html(data);

					}
				});
			}

		});
		$(document).on("click",".add-product",function(){
			var eThis = $(this);
			var subTotal=$('#subTotal');
			var grandTotalR=$('#grandTotalR');
			var grandTotalD=$('#grandTotalD');

			var realcategory=eThis.closest('.col-md-4').find('.card').data('realcategory');
			var foodId= eThis.parents("#pageList .card").attr("id");
			var drinkId= eThis.parents("#pageList-drink .card").attr("id");
			if(realcategory=="Food"){
				var doubleDataFood= $('.table-food tbody').find('#'+foodId+'').attr("id");
				if (doubleDataFood==foodId){
					var unit_price = $('.table-food tbody #'+foodId+' td').eq(2).text();
					var editRow = $('.table-food tbody #'+foodId+' td').eq(3);
					var getQty = parseInt(editRow.text()) + 1;
					var subStore = subTotal.text();
					editRow.text(getQty);

					var total = parseFloat(subStore) + (parseFloat(unit_price));
					subTotal.text(total);
				}
				else{
					$.ajax({
					url:'FoodDetails.php',
					type: "POST",             // Type of request to be send, called as method
					data: {foodId:foodId,realcategory: realcategory}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					//contentType: false,       // The content type used when sending data to the server.
					cache: false,             // To unable request pages to be cached
					//processData:false,        // To send DOMDocument or non processed data file it is set to false
					dataType: 'json',
					success: function(data){

						var row="<tr id="+data.id+" class='itemsfoodid'>";
						row +="<td>";
						row +=data.food;
						row +="</td>";
						row +="<td>";
						row +="<select class='itemsfoodsize'>";
						row +="<option value='M'>M</option>";
						row +="<option value='L'>L</option>";
						row +="</select>";
						row +="</td>";

						row +="<td class='text-right itemsfoodprice'>";
						row +=data.unit_price;
						row +="</td>";
						row +="<td class='text-right itemsfoodqty'>";
						row +="1";
						row +="</td>";
						row +="<td>";
						row +='<div style="float: right;">';
						// row +='<input type="button" value="+" name="plus" class="plus" style="width: 10px; height: 10px; margin: 0;padding: 0;"><br>';
						// row +='<input type="button" value="-" name="minus" class="minus" style="width: 10px; height: 10px; margin: 0;padding: 0;">';
						row +='<button name="plus" style="margin:0;padding:0;" class="btn btn-primary btn btn-just-icon btn-simple plus"><i class="material-icons">add</i></button><br>';
						row +='<button name="minus" style="margin:0;padding:0;" class="btn btn-primary btn btn-just-icon btn-simple minus"><i class="material-icons">remove</i></button>';
						row +='</div>';
						row +="</td>";
						row +="<td>";
						row +='<button class="btn btn-primary btn btn-just-icon btn-simple clearrow">';
						row +='<i class="material-icons">delete</i>';
						row +='</button>';
						row +="</td>";
						row +="</tr>";
						$(".table-sales .table-food tbody:last").append(row);
						rowTable = parseInt(rowTable) + 1;


						var subStore = subTotal.text();
						subTotal.text(parseFloat(subStore) + parseFloat(data.unit_price));
					}

				});
			}
			}
			else{
				var doubleDataDrink= $('.table-drink tbody').find('#'+drinkId+'').attr("id");
				if (doubleDataDrink==drinkId){
					var editRow = $('.table-drink tbody #'+drinkId+' td').eq(4);
					var getQty = parseInt(editRow.text()) + 1;
					editRow.text(getQty);

				}
				else{
					$.ajax({
					url:'FoodDetails.php',
					type: "POST",             // Type of request to be send, called as method
					data: {foodId:drinkId,realcategory: realcategory}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					//contentType: false,       // The content type used when sending data to the server.
					cache: false,             // To unable request pages to be cached
					//processData:false,        // To send DOMDocument or non processed data file it is set to false
					dataType: 'json',
					success: function(data){

						var row="<tr id="+data.id+" class='itemsdrinkid'>";
						row +="<td>";
						row +=data.food;
						row +="</td>";
						row +="<td>";
						row +="<select class='itemsdrinksize'>";
						row +="<option value='M'>M</option>";
						row +="<option value='L'>L</option>";
						row +="</select>";
						row +="</td>";

						row +="<td class='text-right itemsdrinkprice'>";
						row +=data.unit_price;
						row +="</td>";
						row +="<td class='text-right itemsdrinkqty'>";
						row +="1";
						row +="</td>";
						row +="<td>";
						row +='<div style="float: right;">';
						// row +='<input type="button" value="+" name="plus" class="plus" style="width: 10px; height: 10px; margin: 0;padding: 0;"><br>';
						// row +='<input type="button" value="-" name="minus" class="minus" style="width: 10px; height: 10px; margin: 0;padding: 0;">';
						row +='<button name="plus" style="margin:0;padding:0;" class="btn btn-primary btn btn-just-icon btn-simple plus"><i class="material-icons">add</i></button><br>';
						row +='<button name="minus" style="margin:0;padding:0;" class="btn btn-primary btn btn-just-icon btn-simple minus"><i class="material-icons">remove</i></button>';
						row +='</div>';
						row +="</td>";
						row +="<td class='text-right'>";
						row +='<button class="btn btn-primary btn btn-just-icon btn-simple clearrow">';
						row +='<i class="material-icons">delete</i>';
						row +='</button>';
						row +="</td>";
						row +="</tr>";
						$(".table-sales .table-drink tbody:last").append(row);
						rowTable = parseInt(rowTable) + 1;

            			var subStore = subTotal.text();
						subTotal.text(parseFloat(subStore) + parseFloat(data.unit_price));
					}

				});
			}
			}
		});

    $(document).on('click','.plus',function(){
      var eThis=$(this);
      var qtycol = eThis.parents("tr").find('td:eq(3)');
      var qty = qtycol.text();
      var plus = parseInt(qty)+1;
      qtycol.text(plus);
      var price = eThis.parents("tr").find('td:eq(2)').text();
      var total= parseFloat(price);

      var subTotal=$('#subTotal');
      var subTotalvalue= parseFloat(subTotal.text());
      var plusTotal=parseFloat(subTotalvalue+total);

      subTotal.text(plusTotal);

    });
    $(document).on('click','.minus',function(){
      var eThis=$(this);
      var qtycol = eThis.parents("tr").find('td:eq(3)');
      var qty = qtycol.text();
      if(qty>1){
        var minus = parseInt(qty)-1;
        qtycol.text(minus);
        var price = eThis.parents("tr").find('td:eq(2)').text();
        var total= parseFloat(price);

        var subTotal=$('#subTotal');
        var subTotalvalue= parseFloat(subTotal.text());
        var minusTotal=parseFloat(subTotalvalue-total);

        subTotal.text(minusTotal);

      }

	});
	function order(desk){

		var foodid = [];
				var foodsize = [];
				var foodqty = [];
				var foodprice = [];
				var discount = $("#discount").text();
				var type=[];
				$('.table-food .itemsfoodid').each(function(){
					foodid.push($(this).attr('id'));
					type.push('F');
				});
				$('.table-food .itemsfoodsize').each(function(){
					foodsize.push($(this).val());
				});
				$('.table-food .itemsfoodqty').each(function(){
					foodqty.push($(this).text());
				});
				$('.table-food .itemsfoodprice').each(function(){
					foodprice.push($(this).text());
				});
				$('.table-drink .itemsdrinkid').each(function(){
					foodid.push($(this).attr('id'));
					type.push('D');
				});
				$('.table-drink .itemsdrinksize').each(function(){
					foodsize.push($(this).val());
				});
				$('.table-drink .itemsdrinkqty').each(function(){
					foodqty.push($(this).text());
				});
				$('.table-drink .itemsdrinkprice').each(function(){
					foodprice.push($(this).text());
				});
				$.ajax({
					url:"order.php",
					method:"post",
					data:{foodid:foodid,foodsize:foodsize,foodqty:foodqty,foodprice:foodprice,discount:discount,type:type,desk:desk},
					success:function(data1){
						deskload();
						type = ['','info','success','warning','danger','rose','primary'];

						color = Math.floor((Math.random() * 6) + 1);

						$.notify({
							icon: "notifications",
							message: "<b style='font-size:20px;'>"+data1.toUpperCase()+"</b>"

						},{
							type: type[color],
							timer: 5000,
							placement: {
								from: 'bottom',
								align: 'right'
							}
						});
					}
				});
	}
    $(document).on("click",".action_click",function(){
      var eThis = $(this);
      var values = eThis.attr('id');
			if(values=='order'){
				swal({
                    title: 'Are You Want to Order this?',
                    //text: 'You will not be able to recover this imaginary file!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Order',
                    cancelButtonText: 'Cancel',
                    confirmButtonClass: "btn btn-success",
                    cancelButtonClass: "btn btn-danger",
                    buttonsStyling: false
                }).then(function() {
                  swal({
                    title: 'Ordering Please wait',
                    text: 'Your been ordered.',
                    type: 'success',
                    confirmButtonClass: "btn btn-success",
                    buttonsStyling: false
                    }).then(function(){
						order(desk);
                    });
                }, function(dismiss) {
                  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                  if (dismiss === 'cancel') {
                    swal({
                      title: 'Cancelled',
                      text: 'You Have cancel the order.',
                      type: 'error',
                      confirmButtonClass: "btn btn-info",
                      buttonsStyling: false
                    })
                  }
                });

			}
			if(values=='invoice'){
				$.ajax({
					url:'fetch_ordered.php',
					method:'post',
					data:{desk:desk},
					dataType:'json',
					success:function(data){
						var itemsorder=data.order;
						var itemsdate=data.date_time;
						var itemstype=[];
						var itemsid=[];
						var itemsname=[];
						var itemssize=[];
						var itemsqty=[];
						var itemsprice=[];
						var discount=[];
						for(i=0;i<data.i;i++){
							itemstype.push(data.itemtype[i]);
							discount.push(data.discount[i]);
    						itemsid.push(data.itemsid[i]);
							itemsname.push(data.items[i]);
							itemssize.push(data.itemssize[i]);
							itemsqty.push(data.itemsqty[i]);
							itemsprice.push(data.itemsprice[i]);

						}
						$.ajax({
							url:'invoice.php',
							method:'post',
							data:{itemsorder:itemsorder,itemsdate:itemsdate,itemstype:itemstype,itemsid:itemsid,itemsname:itemsname,itemssize:itemssize,itemsqty:itemsqty,itemsprice:itemsprice,discount:discount},
							success:function(data1){
								$('.ModalBox').html(data1);
								$('#invoiceModal').modal('show');
								
							}
						});
					}
				});

				
				
			}
			if(values=='paid_cash'){
				swal({
                    title: 'Cash Payment',
					//text: 'You will not be able to recover this imaginary file!',
					html:
    				'<label>Input Currency in KHR (Riel)</label><input id="payment_khr" class="swal2-input" placeholder="Input Currency in KHR (Riel)">' +
    				'<label>Input Currency in USD (Dollar)</label><input id="payment_usd" class="swal2-input" placeholder="Input Currency in USD (Dollar)">'+
    				'<label>Input Discount Rate(%)</label><input id="discount_rate" class="swal2-input" placeholder="Input Discount Rate(%)">',
					inputAttributes: {
						autocapitalize: 'off'
					},
                    showCancelButton: true,
                    confirmButtonText: 'Order',
                    cancelButtonText: 'Cancel',
                    confirmButtonClass: "btn btn-success",
                    cancelButtonClass: "btn btn-danger",
					buttonsStyling: false
					
                }).then(function() {
					var payment_khr=0;
					var payment_usd=0;
					var discount_rate=0;
					if($('#payment_khr').val()!=''){
						payment_khr=$('#payment_khr').val();
					}
					if($('#payment_usd').val()!=''){
						payment_usd=$('#payment_usd').val();
					}
					if($('#discount_rate').val()!=''){
						discount_rate=$('#discount_rate').val();
					}
					$.ajax({
						url:'fetch_ordered.php',
						method:'post',
						data:{desk:desk},
						dataType:'json',
						success:function(data){
							var itemsorder=data.order;
							var itemsdate=data.date_time;
							var itemstype=[];
							var itemsid=[];
							var itemsname=[];
							var itemssize=[];
							var itemsqty=[];
							var itemsprice=[];
							var discount=[];
							var total_price=[];
							for(i=0;i<data.i;i++){
								itemstype.push(data.itemtype[i]);
								discount.push(data.discount[i]);
								itemsid.push(data.itemsid[i]);
								itemsname.push(data.items[i]);
								itemssize.push(data.itemssize[i]);
								itemsqty.push(data.itemsqty[i]);
								itemsprice.push(data.itemsprice[i]);
								
								total_price.push(data.itemsqty[i] * data.itemsprice[i]);
							}
							var grandTotal = 0;
							for (var i = 0; i < total_price.length; i++) {
								grandTotal += total_price[i] << 0;
							}
							// var totalAmount=grandTotal;
							// var totalDiscount=discount_rate;
							// var recive_usd=payment_usd;
							// var recive_khr=payment_khr;
							$.ajax({
								url:'paid.php',
								method:'post',
								data:{desk:desk,order_id:order_id,totalAmount:grandTotal,totalDiscount:discount_rate,recive_khr:payment_khr,recive_usd:payment_usd},
								dataType:'json',
								success:function(data1){
									deskload();
									var change_khr=0;
									if(data1.change.length!=0){
										change_khr=data1.change;
									}
									$.ajax({
										url:'receipt.php',
										method:'post',
										data:{change_khr:change_khr,discount_rate:discount_rate,recive_khr:payment_khr,recive_usd:payment_usd,itemsorder:itemsorder,itemsdate:itemsdate,itemstype:itemstype,itemsid:itemsid,itemsname:itemsname,itemssize:itemssize,itemsqty:itemsqty,itemsprice:itemsprice,discount:discount},
										success:function(data2){
											$('.ModalBox').html(data2);
											$('#invoiceModal').modal('show');
											
										}
									});
										
									}

							});
							
						}
					});
                }, function(dismiss) {
                  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                  if (dismiss === 'cancel') {
                    swal({
                      title: 'Cancelled',
                      text: 'You Have cancel the order.',
                      type: 'error',
                      confirmButtonClass: "btn btn-info",
                      buttonsStyling: false
                    })
                  }
                });
				

				
				
			}
			if(values=='paid_credit'){
				
			}
    });

    $(document).on("click",".user ",function(){
			$(".table-sales .table-food tbody").html('<tr><th>Food</th><th>Size</th><th class="text-right">Price</th><th colspan="2">Qty</th><th align="left"></th></tr>');
			$(".table-sales .table-drink tbody").html('<tr><th>Drink</th><th>Size</th><th class="text-right">Price</th><th colspan="2">Qty</th><th align="left"></th></tr>');
			$('#subTotal').text('0');
			$('#discount').text('0');
			notmodify();
			var eThis = $(this);
			var id = eThis.attr('id');
			$('.user .reserve-border').removeClass('active');
			$('.user#'+id+' .reserve-border').addClass('active');
			desk = id;
			$('.showtable_index').val(desk);
			
			$('.add-product button').prop("disabled", false);
			var tablestatus= eThis.find('.active').data('tablestatus');
			
			if(tablestatus=='U'){
				
				getOrdered(desk);
				
			}
			
    });
	// $(document).on("click","#paid",function(){
	// 	var totalAmount=$('#grand_total').text();
	// 	var totalDiscount=$('#grand_discount').text();
	// 	var recive_usd=$('#recive_usd').text();
	// 	var recive_khr=$('#recive_khr').text();
	// 	$.ajax({
	// 			url:'paid.php',
	// 			method:'post',
	// 			data:{desk:desk,order_id:order_id,totalAmount:totalAmount,totalDiscount:totalDiscount,recive_khr:recive_khr,recive_usd:recive_usd},
	// 			dataType:'json',
	// 			success:function(data){
	// 				deskload();
	// 				$('#invoiceModal').modal('hide');
	// 				alert(data.error);
	// 				alert(data.change);
	// 			}

	// 	});
	// });
	function modify(){
		
		$('#cancel_order').prop("disabled", true);
		$('#modify_btn').prop("disabled", true);
		$('#modify_save').prop("disabled", false);
		$('#modify_cancel').prop("disabled", false);

		$('.add-product button').prop("disabled", false);
		$('#paid_case').prop("disabled", true);
		$('#paid_credit').prop("disabled", true);
		$('#order').prop("disabled", true);
	}
	function notmodify(){
		$('#cancel_order').prop("disabled", true);
		$('#modify_btn').prop("disabled", true);
		$('#modify_save').prop("disabled", true);
		$('#modify_cancel').prop("disabled", true);

		$('.add-product button').prop("disabled", true);
		$('#paid_case').prop("disabled", false);
		$('#paid_credit').prop("disabled", false);
		$('#order').prop("disabled", false);
	}
	$(document).on("click","#cancel_order",function(){
		notmodify();
		swal({
                    title: 'Do you want to cancel this order?',
                    //text: 'You will not be able to recover this imaginary file!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Cancel Order',
                    cancelButtonText: 'Close',
                    confirmButtonClass: "btn btn-success",
                    cancelButtonClass: "btn btn-danger",
                    buttonsStyling: false
                }).then(function() {
                  swal({
                    title: 'Canceling Please wait',
                    //text: 'Your been ordered.',
                    type: 'success',
                    confirmButtonClass: "btn btn-success",
                    buttonsStyling: false
                    }).then(function(){
						$.ajax({
							url:'cancel_order.php',
								method:'post',
								data:{desk:desk,order_id:order_id},
								success:function(data){
									deskload();
									getOrdered(desk);
									type = ['','info','success','warning','danger','rose','primary'];

									color = Math.floor((Math.random() * 6) + 1);

									$.notify({
										icon: "notifications",
										message: "<b style='font-size:20px;'>"+data.toUpperCase()+"</b>"

									},{
										type: type[color],
										timer: 5000,
										placement: {
											from: 'bottom',
											align: 'right'
										}
									});
								}
						});
                    });
                }, function(dismiss) {
                  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                  if (dismiss === 'Close') {
                    swal({
                      title: 'Cancelled',
                      //text: 'You Have Keep this this order.',
                      type: 'error',
                      confirmButtonClass: "btn btn-info",
                      buttonsStyling: false
                    })
                  }
                });
		
	});
	$(document).on("click","#modify_btn",function(){
		modify();
	});
	$(document).on("click","#modify_cancel",function(){
		notmodify();
	});
	function modify_save(desk){
		var foodid = [];
				var foodsize = [];
				var foodqty = [];
				var foodprice = [];
				var discount = $("#discount").text();
				var type=[];
				$('.table-food .itemsfoodid').each(function(){
					foodid.push($(this).attr('id'));
					type.push('F');
				});
				$('.table-food .itemsfoodsize').each(function(){
					foodsize.push($(this).val());
				});
				$('.table-food .itemsfoodqty').each(function(){
					foodqty.push($(this).text());
				});
				$('.table-food .itemsfoodprice').each(function(){
					foodprice.push($(this).text());
				});
				$('.table-drink .itemsdrinkid').each(function(){
					foodid.push($(this).attr('id'));
					type.push('D');
				});
				$('.table-drink .itemsdrinksize').each(function(){
					foodsize.push($(this).val());
				});
				$('.table-drink .itemsdrinkqty').each(function(){
					foodqty.push($(this).text());
				});
				$('.table-drink .itemsdrinkprice').each(function(){
					foodprice.push($(this).text());
				});
				$.ajax({
					url:"update_order.php",
					method:"post",
					data:{order_id:order_id,foodid:foodid,foodsize:foodsize,foodqty:foodqty,foodprice:foodprice,discount:discount,type:type,desk:desk},
					success:function(data){
						deskload();
						notmodify();
						getOrdered(desk);
						type = ['','info','success','warning','danger','rose','primary'];

									color = Math.floor((Math.random() * 6) + 1);

									$.notify({
										icon: "notifications",
										message: "<b style='font-size:20px;'>"+data.toUpperCase()+"</b>"

									},{
										type: type[color],
										timer: 5000,
										placement: {
											from: 'bottom',
											align: 'right'
										},
										
									});
					}
				});
	}
	$(document).on('click','#modify_save',function(){
		swal({
                    title: 'Do you want to update this order?',
                    //text: 'You will not be able to recover this imaginary file!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Update Order',
                    cancelButtonText: 'cancel',
                    confirmButtonClass: "btn btn-success",
                    cancelButtonClass: "btn btn-danger",
                    buttonsStyling: false
                }).then(function() {
                  swal({
                    title: 'Updating Please wait',
                    //text: 'Your been ordered.',
                    type: 'success',
                    confirmButtonClass: "btn btn-success",
                    buttonsStyling: false
                    }).then(function(){
						modify_save(desk);
                    });
                }, function(cancel) {
                  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                  if (dismiss === 'Close') {
                    swal({
                      title: 'Cancelled',
                      //text: 'You Have Keep this this order.',
                      type: 'error',
                      confirmButtonClass: "btn btn-info",
                      buttonsStyling: false
                    })
                  }
                });
				
	});
	$(document).on("click","#logout",function(){
		$.ajax({
			url:"logout.php",
			method:"post",
			success:function(data){
				location.reload();
			}
		});
	});
	$(document).on("change",".showtable_index",function(){
		desk=$(this).val();
	});
	// If theres no activity for 5 seconds do something
    var activityTimeout = setTimeout(inActive, 100000);
    
    function resetActive(){
        $(document.body).attr('class', 'active');
        clearTimeout(activityTimeout);
        activityTimeout = setTimeout(inActive, 100000);
    }
    
    function inActive(){
		$(document.body).attr('class', 'inactive');
		setTimeout(function(){

			$('.user').addClass('col-md-2').removeClass('col-md-12');
			$('.sidebar').css('width','50%');
			$('.sidebar-wrapper').css('width','100%');
			$('#minimizeSidebar .visible-on-sidebar-regular').html("more_vert");
			$('.btn-slider').css("left","105%");

			$('.sidebar .sidebar-wrapper').scrollTop(0);

			$('.sidebar').css('z-index','2');
            $('.full-background').css('display','inline-block');

			$('.sidebar .collapse').css('height','auto');
			md.misc.sidebar_mini_active = true;
		},300);
    }
	$(document).bind('mousemove', function(){resetActive()});
	$(document).on("click",".full-background",function(){
		$('.user').addClass('col-md-12').removeClass('col-md-2');
        $('.sidebar').css('width','200px');
        $('.sidebar-wrapper').css('width','100%');
        $('#minimizeSidebar .visible-on-sidebar-regular').html("view_module");
        $('.btn-slider').css("left","230px");

        $('.sidebar').css('z-index','2');
        $('.full-background').css('display','none');

        md.misc.sidebar_mini_active = false;

	});
	$(document).on("click",".clearrow",function(){

		var eThis = $(this);
			//var id = eThis.parents("tr").attr("id");
      var qty = eThis.parents("tr").find('td:eq(3)').text();
      var price = eThis.parents("tr").find('td:eq(2)').text();
      var total= parseFloat(qty*price);

      var subTotal=$('#subTotal');
      var subTotalvalue= parseFloat(subTotal.text());
      var minusTotal=parseFloat(subTotalvalue-total);

      subTotal.text(minusTotal);

			eThis.parents("tr").remove();
		});
        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

        demo.initVectorMap();
    });
	//WebSocket settings
    JSPM.JSPrintManager.auto_reconnect = true;
    JSPM.JSPrintManager.start();
    JSPM.JSPrintManager.WS.onStatusChanged = function () {
        if (jspmWSStatus()) {
            //get client installed printers
            JSPM.JSPrintManager.getPrinters().then(function (myPrinters) {
                var options = '';
                for (var i = 0; i < myPrinters.length; i++) {
                    options += '<option>' + myPrinters[i] + '</option>';
                }
                $('#installedPrinterName').html(options);
            });
        }
    };
 
    //Check JSPM WebSocket status
    function jspmWSStatus() {
        if (JSPM.JSPrintManager.websocket_status == JSPM.WSStatus.Open)
            return true;
        else if (JSPM.JSPrintManager.websocket_status == JSPM.WSStatus.Closed) {
            alert('JSPrintManager (JSPM) is not installed or not running! Download JSPM Client App from https://neodynamic.com/downloads/jspm');
            return false;
        }
        else if (JSPM.JSPrintManager.websocket_status == JSPM.WSStatus.BlackListed) {
            alert('JSPM has blacklisted this website!');
            return false;
        }
    }
 
    //Do printing...
    function print(desk) {
        if (jspmWSStatus()) {
            //Create a ClientPrintJob
            var cpj = new JSPM.ClientPrintJob();
            //Set Printer type (Refer to the help, there many of them!)
            cpj.clientPrinter = new JSPM.DefaultPrinter();
            var cpj1 = new JSPM.ClientPrintJob();
            //Set Printer type (Refer to the help, there many of them!)
            cpj1.clientPrinter = new JSPM.DefaultPrinter();
            var cpj2 = new JSPM.ClientPrintJob();
            //Set Printer type (Refer to the help, there many of them!)
            cpj2.clientPrinter = new JSPM.DefaultPrinter();

            //Set content to print...
            //Create ESP/POS commands for sample label
           // var esc = '\x1B'; //ESC byte in hex notation
            var newLine = '\x0A'; //LF byte in hex notation

			$.ajax({
				url:"fetch_ordered.php",
				method:"POST",
				data:{desk:desk},
				dataType:"json",
				success:function(data){
					var itemsorder=data.order;
					var hearder_1 = 'Invoice';
                    hearder_1 += newLine + newLine;
                    hearder_1 += 'Restaurant'; //text to print
                    hearder_1 += newLine + newLine;
                    hearder_1 += 'Order:'+itemsorder; //text to print
                    hearder_1 += newLine + newLine;
					var total_price=[];
                    var cmds = "";
					for(var i=0;i<data.itemsid.length;i++){

						total_price.push(data.itemsqty[i] * data.itemsprice[i]);
						var itemsLength=data.items[i].length;
						var total=parseInt(data.itemsqty[i])*parseInt(data.itemsprice[i]);
						var priceLength=data.itemsprice[i].length;
						var replaceDot=35-parseInt(itemsLength)-parseInt(priceLength);
						var dotJ='';
						for(var j=0;j<replaceDot;j++){
							dotJ+=".";
						}
						cmds += data.items[i]+dotJ+total+"៛";
						cmds += newLine;
					}
					var grandTotal = 0;
					for (var i = 0; i < total_price.length; i++) {
						grandTotal += total_price[i] << 0;
					}
					var footer_1="";
                    footer_1 += newLine + newLine;
                    footer_1 += 'SUBTOTAL.................'+grandTotal+'៛';
                    footer_1 += newLine;
                    footer_1 += 'TAX0%....................0.00៛';
                    footer_1 += newLine;
                    footer_1 += 'TOTAL....................'+grandTotal+'៛';

                    footer_1 += newLine + newLine;

                    footer_1 += 'Thank you!';
                    footer_1 += newLine + newLine;

                    footer_1 += newLine + newLine;
                    var contentall=hearder_1+cmds+footer_1;
					//cpj.printerCommands = cmds;

                    var my_file1 = new JSPM.PrintFileTXT(contentall,'MyFile.txt',1);
                    my_file1.fontBold=1;
                    my_file1.fontSize=13;

                    var my_file2 = new JSPM.PrintFileTXT(cmds, 'MyFile.txt', 1);
                    my_file2.fontSize=12;
                    var my_file3 = new JSPM.PrintFile('invoice_pri.php',JSPM.FileSourceType.Text,'invoice1.php',1);
                    cpj.files.push(my_file1);
                    var cpjg = new JSPM.ClientPrintJobGroup();
                    cpjg.jobs.push(cpj);
                    cpjg.sendToClient();


				}
			});
            
        }
    }
</script>


<!-- Mirrored from demos.creative-tim.com/material-dashboard-pro/examples/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Mar 2017 21:32:16 GMT -->
</html>
<?php
	
}
?>